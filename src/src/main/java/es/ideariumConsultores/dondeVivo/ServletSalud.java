package es.ideariumConsultores.dondeVivo;

import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;



public class ServletSalud extends HttpServlet {

	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException
	{
		try{
		FileInputStream file = new FileInputStream(getServletContext().getRealPath("/")+"/index.html");
		//response.setContentLength(file.length());
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("text/html");
		IOUtils.copy(file,response.getOutputStream());
		}
		catch(Exception ex){
			try{
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
			}catch(IOException ex2){
				System.err.println("No se pudo enviar respuesta de error (error interno)");
				ex2.printStackTrace();
				return;
			}
		}
		
	}
}