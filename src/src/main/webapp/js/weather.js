var html_weather='<p><b>Cielo:</b> #estado_cielo#<p>'+
'<p><b>Temperatura:</b> #temperatura#minima#&ordm;  #temperatura#maxima#&ordm;<p>'+
'<p><b>Sensación térmica:</b> #sens_termica#minima#&ordm;  #sens_termica#maxima#&ordm;<p>'+
'<p><b>Probabilidad precipitación:</b> #prob_precipitacion#<p>'+
'<p><b>Viento:</b> #viento#velocidad# km/h #viento#direccion#<p>'+
'<p><b>Racha máxima:</b> #racha_max#<p>'+
'<p><b>Humedad relativa:</b> #humedad_relativa#minima#% #humedad_relativa#maxima#% <p>'+
'<p><b>Radiación ultravioleta máxima:</b> #uv_max#<p>';
function showWeather(){
	if ($('#weatherDialog').css("display")!="none"){
		$('#weatherDialog').hide();
	}
	else{

	if ($("#selMuniWeather option").length<=0){
		$("#selMuniWeather").append("<option value='0'>-- Seleccione un municipio</option>");
		var municipios =muniDefault.split("\n");
		for (var i=0; i<municipios.length;i++){
			var datos = municipios[i].split(";");
			$("#selMuniWeather").append("<option value='"+datos[1]+"'>"+datos[0]+"</option>");
		}
		$("#selMuniWeather").chosen( { search_contains: true, no_results_text: "Sin resultados para " , width:"100%"} );
	}

	if(munActual){
    $("#selMuniWeather").val(munActual);
    $('#selMuniWeather').trigger("chosen:updated");
    fillWeather($("#selMuniWeather").val());
  }

	$('#weatherDialog').resizable({"alsoResize":"#weatherDialog .dialogFullContent"});
	$('#weatherDialog').draggable({
        cursor: 'move',
				containment: "#mainPage",
				scroll: false,
				handle: '.ui-header'
    });
	$("#weatherDialog").addClass("ui-page-theme-a ui-corner-all");
	$("#weatherDialog div[data-role='header']").addClass("ui-header ui-bar-inherit");
	$(".dialogTitle").addClass("ui-title");

	closeAllDialogs();
	$("#weatherDialog").show();
	$("#closeButtonWeatherDialog").bind("vclick", function(event, ui) { event.preventDefault();$('#weatherDialog').hide(); });

	setWeather();
	}
}

function setWeather(){
	var coords = $("#coords").text().split(" ");
	if (coords.length>=2){
		 $("#gettingWeatherOverlay").css("display","block");
	$.ajax({
		url: server+'/Visor2D?service=WFS&' +
		'version=1.0.0&request=GetFeature&typename=BusMun&' +
		'outputFormat=application/json&srsname=EPSG:25830&' +
		'CQL_FILTER=INTERSECTS(shape,POINT('+coords[0]+' '+coords[1]+'))',
		type: 'GET',

		success: function(data) {
			var format = new ol.format.GeoJSON();
			var features = format.readFeatures(data);
			if (features.length<=0){
				creaVentanaAviso("La ubicación indicada no se encuentra Aragón");

			}
			else if (features.length>=1) {

				var cmuniine =features[0].getProperties().c_muni_ine;
				$("#selMuniWeather").val(cmuniine);
				//$("#muni").text(features[0].getProperties().d_muni_ine);
				fillWeather(cmuniine);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			creaVentanaAviso("No se ha podido obtener el municipio correspondiente "+textStatus +"\n "+errorThrown);
			if ($("#dialogFtForm").dialog( "isOpen" )==false) {
				$("#dialogFtForm").dialog("open");
			}
			$("#gettingWeatherOverlay").css("display","none");
		}
	});
	}
}

function  changeMuniWeather(){

	fillWeather($("#selMuniWeather").val());
}
	function fillWeather(cmuniine){
		$("#previsiones").empty();


		if(cmuniine!=0){
				$("#gettingWeatherOverlay").css("display","block");
				$.ajax({
    			url: server+'/AEMET/xml/municipios/localidad_'+cmuniine+".xml",
    			type: 'GET',

    			success: function(data) {
    				$("#gettingWeatherOverlay").css("display","none");
    				var dias = data.getElementsByTagName("dia");
    				var hoy = new Date();
    				var fecha = dias[0].getAttribute("fecha");
    				var inicio=0;
    				if (parseInt(fecha.substr(8,2))!= hoy.getDate()){//siguen incluyendo la previsión de ayer -> la saltamos
    					inicio=1;
    				}
    				for (var i=inicio; i<Math.min(3+inicio,dias.length);i++){
    					fillDayWeather(dias[i],i==inicio,(i==inicio? "Hoy":(i==1 +inicio?"Mañana":"Pasado")));
    				}
    				//$(".dailyWeather").collapsible().trigger("create");
    				//$('#previsiones').collapsibleset().trigger('create');


    			},
    			error: function(jqXHR, textStatus, errorThrown) {
    				creaVentanaAviso("No se ha podido obtener la previsión para el municipio con código INE "+cmuniine+" ("+textStatus +"\n "+errorThrown);
    				$("#gettingWeatherOverlay").css("display","none");
    			}
    		});
		}
	}

	function replaceWeatherValues(day_html, children){
		var hijos = children.children;
		if (hijos.length<1){
			if (children.textContent && children.textContent.length>0){
				day_html = day_html.replace("#"+children.nodeName+"#",(children.getAttribute("descripcion")?children.getAttribute("descripcion"):children.textContent));
			}
		}
		else if (hijos.length==1){
			var valor = (children.getAttribute("descripcion")?children.getAttribute("descripcion"):hijos[0].wholeText);

			if (valor && valor.length>0){
				day_html = day_html.replace("#"+children.nodeName+"#",(children.getAttribute("descripcion")?children[i].getAttribute("descripcion"):hijos[0].wholeText));
			}
		}
		else{
			for (var j=0; j<hijos.length;j++){

				if (hijos[j].firstChild && hijos[j].firstChild.wholeText && hijos[j].firstChild.wholeText.length>0){
						day_html = day_html.replace("#"+children.nodeName+"#"+hijos[j].nodeName+"#",hijos[j].firstChild.wholeText);
					}
			}
		}
		return day_html;
	}
	function fillDayWeather(prevision, primerDia,label){
		var children = prevision.children;
		var day_html = html_weather;
		for (var i=0; i<children.length;i++){
			if ((children[i].getAttribute("periodo")==null) ||(children[i].getAttribute("periodo")=="00-24")){
				day_html = replaceWeatherValues(day_html, children[i]);
			}
		}
		if (primerDia){
		// hago otra pasada con los valores de 12-24 y otra 18-24 por si alguno venía vacío en los anteriores periodos
		for (var i=0; i<children.length;i++){
			if ((children[i].getAttribute("periodo")==null) ||(children[i].getAttribute("periodo")=="12-24")){
				day_html = replaceWeatherValues(day_html, children[i]);
			}
		}
		for (var i=0; i<children.length;i++){
			if ((children[i].getAttribute("periodo")==null) ||(children[i].getAttribute("periodo")=="18-24")){
				day_html = replaceWeatherValues(day_html, children[i]);
			}
		}
		}
		day_html=day_html.replace(/#.*#/g,"");
		$("#previsiones").append('<div id="dw_'+label+'" class="dailyWeather" data-role="collapsible" ><h3 >'+label+" ("+ prevision.getAttribute("fecha")+')</h3>'+
				day_html+'</div>');
		$( '#dw_'+label ).collapsible();
		// quito todos los eventos y lo programo "a mano" para que funcione tanto en móvil (chrome) como en pc
		bindCollapse("dw_"+label);
		
	}