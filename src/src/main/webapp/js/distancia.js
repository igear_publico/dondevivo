var distanciaDialogCreated=false;

function openDistanciaDialog() {
	if(distanciaDialogCreated){
		if ($('#distanciaDialog').css("display")!="none"){
			$('#distanciaDialog').hide();
		}
		else{
		closeAllDialogs();
		$('#distanciaDialog').show();
		}
	}
	else{
	$('#distanciaDialog').resizable({"alsoResize":"#distanciaDialog .dialogFullContent"});
	$('#distanciaDialog').draggable({
		cursor: 'move',
		containment: "#mainPage",
		scroll: false,
		handle: '.ui-header'
	});


	$(".distanciaTab").trigger("create");

	$(".dialogTitle").addClass("ui-title");


	$("#distanciaDialog").addClass("ui-page-theme-a ui-corner-all");
	$("#distanciaDialog div[data-role='header']").addClass("ui-header ui-bar-inherit");
	$("#distanciaDialog div[data-role='button']").button();
	$(".dialogTitle").addClass("ui-title");

	$(".ui-tabs-anchor").addClass("ui-link ui-btn");

	closeAllDialogs();
	$('#distanciaDialog').show();
	$("#closeButtonDistanciaDialog").bind("vclick", function(event, ui) { event.preventDefault();$('#distanciaDialog').hide(); });

	distanciaDialogCreated=true;
	}

	updateDistancia()

}

function incDist(slider){
	var value =Math.max($("#"+slider).val() + 1,  0);
	$("#"+slider).val( value);
  $("#"+slider).slider("refresh");
  updateDistancia();
}

function decDist(slider){
	var value =Math.max($("#"+slider).val() - 1,  0);
	$("#"+slider).val( value);
  $("#"+slider).slider("refresh");
  updateDistancia();
}

function updateDistancia(){
	var value = $("#distanciaSlider").val()
	if(value == 0){
		changeDistance(1000);
		$('.ui-slider-handle').text("1KM").css({width:40,'text-align': 'center','margin-left': '-20px'});
	}else if(value == 1){
		changeDistance(5000);
		$('.ui-slider-handle').text("5KM").css({width:40,'text-align': 'center','margin-left': '-20px'});
	}else{
		changeDistance(10000);
		$('.ui-slider-handle').text("10KM").css({width:50,'text-align': 'center','margin-left': '-25px'});
	}
}


function changeDistance(distancia){
	d = distancia;
	refreshResults(distancia);
	changeBufferGeom(distancia);
}
function getSelectedDistance(){
	return d;
}
