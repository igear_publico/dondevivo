
var printWidth = 1200;
var printHeight = 572;
var printMapWidth = 1200;
var printMapHeight = 572;
var printWidthAragon = 325;
var printHeightAragon = 394;
var printWidthOverview = 130;
var printHeightOverview = 140;

var printServer ="//" + document.location.host;
	
var xminOverview = 530000;
var ymaxOverview = 4760000;
var xmaxOverview = 855000;
var yminOverview = 4410000;

var printActionURL = server + "/Mapa/";
var printActionXSL ="/app/jboss/Mapa/templates/informe_v2D.xsl";


var printOverviewURL= server + "/BD_GIS/getOverviewMap.jsp";
var printScaleURL= server + "/BD_GIS/getMapScale.jsp";
var printMarkersURL= server + "/BD_GIS/getMarkersMap.jsp";

var mapOL;
var tamPixelOverviewX = (xmaxOverview - xminOverview) / printWidthOverview;
var tamPixelOverviewY = (ymaxOverview - yminOverview) / printHeightOverview;
function openPrintDialog() {
	mapOL=map;
	if ($('#printDialog').css("display")!="none"){
		$('#printDialog').hide();
	}
	else{

	$('#printDialog').draggable({
        cursor: 'move',
		containment: "#mainPage",
		scroll: false,
		handle: '.ui-header'
    });
	$("#printDiv").trigger("create");
	$("#printDialog").addClass("ui-page-theme-a ui-corner-all");
	$("#printDialog div[data-role='header']").addClass("ui-header ui-bar-inherit");
	$(".dialogTitle").addClass("ui-title");

	$("#printDialog div[data-role='button']").button();
	closeAllDialogs();
	$("#printDialog").show();
	$("#closeButtonPrintDialog").bind("vclick", function(event, ui) { event.preventDefault();$('#printDialog').hide(); });

	}

}

function doPrint(profileImg){
	$("#printDialog").hide();
    document.printForm.xml.value = makeXMLsafeAcentos(getDataToPrint(profileImg).replace(/http:\/\/localhost/g,"https://idearagon.aragon.es"));
    document.printForm.xslt.value = printActionXSL;
    document.printForm.action = printActionURL;
    document.printForm.submit();
    
}

function checkDeformation( mwidth, mheight) {
	//var newBBOX = new boundingBox(_minx, _miny, _maxx, _maxy);
	var bounds = mapOL.getView().calculateExtent(mapOL.getSize());

	var _minx = bounds[0];
	var _miny = bounds[1];
	var _maxx = bounds[2];
	var _maxy = bounds[3];
	var ratioTamano = mwidth / mheight;
	var diffX = _maxx - _minx;
	var diffY = _maxy - _miny;
	var ratioCoord = diffX / diffY;
//	alert("ratioT" + ratioTamano + "," + ratioCoord)
	if (ratioCoord > ratioTamano) {
		// estirar la Y
		//	alert("estirar la Y")

		var newDiffY = diffX / ratioTamano;

		var gapY = newDiffY - diffY;

		if (gapY > 0) {
			_miny = _miny - (gapY/2);
			_maxy = _maxy + (gapY/2);
		} else {
			_maxy = _miny - (gapY/2);
			_miny = _maxy + (gapY/2);
		}
	} else if (ratioCoord < ratioTamano){
		// estirar la X
		//alert("estirar la X")

		var newDiffX = ratioTamano * diffY;

		var gapX = newDiffX - diffX;
		if (gapX > 0) {
			_minx = _minx - (gapX/2);
			_maxx = _maxx + (gapX/2);
		} else {
			_maxx = _minx - (gapX/2);
			_minx = _maxx + (gapX/2);
		}
	}
	return _minx+","+ _miny+","+ _maxx+","+_maxy;
}

function coords2px(printWidth, printHeight,minx, miny, maxx, maxy,xIn, yIn) {
	pixelX = (maxx - minx) / printWidth;
	pixelY = (maxy - miny) / printHeight;

	return Math.round((xIn-minx)/pixelX)+","+Math.round((maxy-yIn)/pixelY);
}

function getPrintSearchResult(printWidth, printHeight,minxPrint,minyPrint,maxxPrint, maxyPrint){

	var fts = searchLayer.getSource().getFeatures();
	var urls="";
		for (var i=0;i<fts.length;i++){
			if (fts[i].get('layer') && (fts[i].get('layer').length>0)){
				urls+='<mapa><laUrl>'+urlAbsoluta(server+'/Visor2D?SRS=EPSG:25830&version=1.1.1&service=WMS&FORMAT=image/png&TRANSPARENT=TRUE&request=GetMap&WIDTH=' + printWidth + '&HEIGHT=' + printHeight+ '&BBOX=' + minxPrint + ',' + minyPrint + ',' + maxxPrint + ',' + maxyPrint+
						"&STYLES=busqueda&Layers="+fts[i].get('layer')+
				'&CQL_FILTER='+fts[i].get('cql_filter'))+'</laUrl></mapa>';
				
			}
			else{// punto
				try{
				var coords = fts[i].getGeometry().getCoordinates();
				
				var pixels=coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,coords[0],coords[1]);
				urls += '<mapa><laUrl>' + urlAbsoluta(printMarkersURL)+'?icon='+urlAbsoluta(fts[i].getStyle().getImage().getSrc())+'&coords='+pixels+'&width='+printWidth+'&height='+printHeight+ '</laUrl></mapa>';
				}
				catch(err){// por si no fuera un punto con icono
					console.log(err);
				}

			}
		
		}
	return urls;
	
}

function getPrintGeoms(features,printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint){
	var numData = features.length;
	var request="";
	if (numData > 0) {
		
		for (var jj = 0; jj < numData; jj++) {
			var type = features[jj].getGeometry().getType();
			if (type=="Point"){
				request += getPrintPoint(features[jj].getGeometry(),printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint);
			}
			else if (type=="LineString"){
				request += getPrintLine(features[jj].getGeometry(),printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint);
			}
			else if (type=="Polygon"){
				request += getPrintPolygon(features[jj].getGeometry(),printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint);
			}
		}
	}	
	return request;
}

function getPrintPoint(geometry,printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint){
	aux = geometry.getCoordinates();
			
			var pixels=coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,aux[0],aux[1]);
			return '<mapa><laUrl>' + urlAbsoluta(printMarkersURL)+'?color=3399CC&coords='+pixels+'&width='+printWidth+'&height='+printHeight+'&drawWidth=5'+ '</laUrl></mapa>';
		
}

function getPrintLine(geometry,printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint){
	var pixels="";		
	aux = geometry.getCoordinates();
			for (var i = 0; i < aux.length;i++) {
				
				if (i>0){
					pixels+=",";
				}
				 pixels+=coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,aux[i][0],aux[i][1]);
			
			}
			return '<mapa><laUrl>' + urlAbsoluta(printMarkersURL)+'?color=3399CC&coords='+pixels+'&width='+printWidth+'&height='+printHeight+'&drawWidth=2'+ '</laUrl></mapa>';
}
function getPrintPolygon(geometry,printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint){
			var pixels="";
			aux = geometry.getCoordinates()[0];
			for (var i = 0; i < aux.length;i++) {
				
				if (i>0){
					pixels+=",";
				}
				 pixels+=coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,aux[i][0],aux[i][1]);
			
			}
			pixels+=","+coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,aux[0][0],aux[0][1]);

			return '<mapa><laUrl>' + urlAbsoluta(printMarkersURL)+'?color=3399CC&coords='+pixels+'&width='+printWidth+'&height='+printHeight+'&drawWidth=2'+ '</laUrl></mapa>';
		
}


function getPrintBuffer(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint){
	if (featureBuffer){
		var center = featureBuffer.getGeometry().getCenter();
		var mXpixel = (maxxPrint - minxPrint) / printWidth;

		var radius = featureBuffer.getGeometry().getRadius()/mXpixel;
	pixels = coords2px(printWidth, printHeight,minxPrint, minyPrint, maxxPrint, maxyPrint,center[0],center[1])
	return '<mapa><laUrl>' + urlAbsoluta(printMarkersURL)+'?color=0099ff&coords='+pixels+'&radio='+parseInt(radius)+'&width='+printWidth+'&height='+printHeight+'&drawWidth=2'+ '</laUrl></mapa>';
	}
	else{
		return "";
	}
}


function getDataToPrint(profileImg){
	var newBboxPrint = checkDeformation(printMapWidth, printMapHeight);
	var extent = newBboxPrint.split(",");
	var wms_layers = getWMSRequests(newBboxPrint,printMapWidth, printMapHeight);
	var capas="";
	for (var i=0; i<wms_layers.length; i++){
		capas += "<mapa><laUrl>"+wms_layers[i]+"</laUrl></mapa>";
	}
	//capas += getPrintBOALayers(printWidth, printHeight,extent[0], extent[1], extent[2] , extent[3]);
	if (searchLayer){
		capas += getPrintSearchResult(printWidth, printHeight,extent[0], extent[1], extent[2] , extent[3]);
		capas += getPrintBuffer(printWidth, printHeight,extent[0], extent[1], extent[2] , extent[3]);
	}


	/**
	if (drawingLayer){
		capas += getPrintGeoms(drawingLayer.getSource().getFeatures(),printWidth, printHeight,extent[0], extent[1], extent[2] , extent[3]);
	}
	
	if (pathLayer) {
		capas += getPrintGeoms(pathLayer.getSource().getFeatures(),printWidth, printHeight,extent[0], extent[1], extent[2] , extent[3]);
	}
	if (areaLayer) {
		capas += getPrintGeoms(areaLayer.getSource().getFeatures(),printWidth, printHeight,extent[0], extent[1], extent[2] , extent[3]);
	}
	if (profileLayer) {
		capas += getPrintGeoms(profileLayer.getSource().getFeatures(),printWidth, printHeight,extent[0], extent[1], extent[2] , extent[3]);
	}
*/
	var titulo = document.getElementById("titleForPrint").value;
	

	// devolver el XML completo con la informaci�n necesaria para el PDF
	return "<imprimir><fecha>"+getFechaActual()+"</fecha><titulo>"+(profileImg?"Perfil topográfico longitudinal":titulo)+"</titulo>"+
	capas+
	"<mapa><laUrl>"+getPrintScale(printMapWidth, printMapHeight,extent[0],extent[2])+
	"</laUrl></mapa>"+getPrintOverview(extent)+getPrintLegends()+(profileImg ? profileImg:"")+
"</imprimir>";
}

function getPrintLegends(){
	var leyendas = "";
	var listacapas = $("#layers li");
	for (var i=0; i<listacapas.length; i++){
		var imagenes = $("#"+listacapas[i].id+" .imgLey").not(".legendError");
		
			
			for (var j=0; j<imagenes.length; j++){
				leyendas+="<leyenda>";
				leyendas+="<laUrl>"+imagenes[j].src.replace("http://localhost","https://idearagon.aragon.es")+"</laUrl>"
				if (j==0){
				leyendas+="<label>"+$("#"+listacapas[i].id+" .titleLayer")[0].innerText+"</label>";
				}
				leyendas+="</leyenda>";
			}
		
		
	}
	return leyendas;
}
function coord2px_overview(_theX, _theY) {
	var result = new Array();

	var offsetX = _theX - xminOverview;
	var offsetY = ymaxOverview - _theY;

	result["x"] = Math.round(offsetX / tamPixelOverviewX);
	result["y"] = Math.round(offsetY / tamPixelOverviewY);

	if (result["x"] <= 0) {
		result["x"] = 0;
	}

	if (result["y"] <= 0) {
		result["y"] = 0;
	}

	if (result["x"] >= 130) {
		result["x"] = 130;
	}

	if (result["y"] >= 140) {
		result["y"] = 140;
	}
	return result;
}

function urlAbsoluta(theUrl){
	var url = theUrl
	if ((theUrl.indexOf("http:/")<0)&&(theUrl.indexOf("https:/")<0)&&(theUrl.indexOf("//")!=0)){
		url = printServer+theUrl;
		
	}
	if ((url.indexOf("http:/")<0)&&(url.indexOf("https:/")<0)){
		url = location.protocol +url;
	}
		return url;
}