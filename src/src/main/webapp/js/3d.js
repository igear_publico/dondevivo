function open3D() {
	var coords = map.getView().getCenter();
	var latLon = ol.proj.transform(coords, "EPSG:25830", "EPSG:4258");
	theLon = latLon[0];
	theLat = latLon[1];
	window.open("http://vuelos3d.aragon.es/?latitud=" + theLat + "&longitud=" + theLon);
}

function add3DButton() {
	$("#btn3D").append('<button type="button" title="Ver en visor 3D" class="ui-icon-3d"></button>');
	 var tool3D = new ol.control.Control({element: document.getElementById("btn3D")});
	 map.addControl(tool3D);
}