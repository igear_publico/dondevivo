var toc_group;

var capas = [];
var capasTabla = [];
var grupos=[];
var grupoActual;
var bloques=[];
var wmts_capabilities = [];
var serviciosWMTS = [];


var toc_nuevo = true;
var default_layers = [];

var idx_class_css = 0;
var list_class_css = [];

var layerList = null;
var tocDialogCreated=false;

var visibleClickTimestamp=0;


/**
 * Elimina los acentos de una cadena
 * @param entry cadena de texto
 * @returns cadena de texto sin acentos
 */
function asciify(entry) {
	entry = entry.toString().toLowerCase();
	entry = entry.replace(/á/g, 'a');
	entry = entry.replace(/é/g, 'e');
	entry = entry.replace(/í/g, 'i');
	entry = entry.replace(/ó/g, 'o');
	entry = entry.replace(/ú/g, 'u');
	entry = entry.replace(/ñ/g, 'n');
	entry = entry.replace(/ü/g, 'u');
	entry = entry.replace(/ç/g, 'z');
	entry = entry.replace(/\'/g, "");

	return entry;
}


function addTema(titulo) {
	if ($("#"+list_class_css[titulo]).length == 0) {
		$("#toc_themes").append('<option id="' + list_class_css[titulo] + '" value="' + list_class_css[titulo] + '">' + titulo + '</option>');
	}
}
function get_class_css(titulo) {
	if (titulo in list_class_css) {
		return list_class_css[titulo];
	} else {
		var value_css = 'toc_theme_' + idx_class_css;
		list_class_css[titulo] = value_css;

		idx_class_css++;
	}
	return list_class_css[titulo];
}

function activarCapas(listaCapas,activar){

		var ids=listaCapas.split(",");
		for (var i=0; i<ids.length; i++){
			var lay = getOLLayerByWMSLayer(ids[i]);
			if(!lay){ 			//añadirla al TOC
				// ver si está en capas disponibles
				for (var j=0; j<=capas.length; j++){
					if (capas[j] && capas[j].layers){
						var cindividual = capas[j].layers.split(",");
						for (var k=0; k<cindividual.length; k++){
							if ((cindividual[k])&& (cindividual[k]==ids[i])){
								lay=addLayer(j);
								break;
							}
						}
					}
				}
			}
			if(!lay){ 			//añadirla al TOC. No está en disponibles
				// TODO: añadir como capa de usuario. Ver como gestionarlas.
				console.log("No existe la capa "+ids[i]+" en el TOC.");
				//return;
			}

			else if (lay.getVisible()!=activar){
				setVisibleLayer(lay,lay.get("idx"),activar);
				saveTocList(false);
			}

		}
}
function includeListaCapas(listaCapas, txt, class_css, type, oneVisLayerGroup, current_theme) {

	if (listaCapas.length > 0) {
		addTema(current_theme);
	}
	var hiddenLayersList =  getInitialParam(document.location.toString(), "HIDDENLAYERS=");
	var hiddenLayers=new Array();
	if (hiddenLayersList){
		hiddenLayers = decodeURI(hiddenLayersList).split(",");
	}
	for (var i = 0; i<listaCapas.length;i++) {
		var pk_current_capa = listaCapas[i].pk;
		var current_class_css = (class_css != '' ? class_css : 'toc_other');

		capas[pk_current_capa] = listaCapas[i];
		capas[pk_current_capa].type = type;
		capas[pk_current_capa].class_css = current_class_css;
		capas[pk_current_capa].temas = txt;
		capas[pk_current_capa].oneVisLayerGroup = oneVisLayerGroup;

	}
}

function parseTOC(data, txt, class_css, oneVisLayerGroup, current_theme) {



	if ('capasWMS' in data) {
		includeListaCapas(data.capasWMS, txt, class_css, 'wms', oneVisLayerGroup, current_theme);
	}

	if ('capasWMTS' in data) {
		includeListaCapas(data.capasWMTS, txt, class_css, 'wmts', oneVisLayerGroup, current_theme);
	}

	for (var i = 0; i<data.grupos.length;i++) {

		var current_class_css = '';

		if (class_css == "") {
			
			current_class_css = get_class_css(data.grupos[i].titulo);
			grupos[data.grupos[i].pk]=new Object();
			grupos[data.grupos[i].pk].class_css=current_class_css;
			grupos[data.grupos[i].pk].titulo=data.grupos[i].titulo;
			current_theme = data.grupos[i].titulo;

		} else {
			current_class_css = class_css;
		}
		var grupo_excluyente = oneVisLayerGroup;
		if ((grupo_excluyente == null) && (data.grupos[i].capas_excluyentes == "t")) {
			grupo_excluyente = data.grupos[i].pk;
		}
		parseTOC(data.grupos[i], txt + ((txt != '')? " > " : '') + data.grupos[i].titulo, current_class_css, grupo_excluyente, current_theme);
	}
}


function addGroupLayers(){
	$("#info_layer").selectmenu();

	for (var grupo in grupos) {
		$("#info_layer").append("<option id='info_layer_opt_"+grupo+"' value='"+grupo+"'>"+grupo+"</option>")
	}
	$("#info_layer").selectmenu("refresh");
}

function changeGroup(){
$("#tocSearchText").val("");
	var grupoAnterior = grupoActual;
	var layers = toc_group.getLayers();
	closePopup();

	//Primero se eliminan las capas del grupo anterior
	for(var pk in capas){

    if(capas[pk].temas.split(' > ')[0] == grupoAnterior){
      var layerGroup = getOLLayerByTabla(capas[pk].tablas);
      for(var i in layerGroup){
        layers.remove(layerGroup[i]);
      }
    }
  }
  $("#layers").empty();//html("<ul id=\"layers\"></ul>");

	grupoActual = $("#info_layer").val();
	//Luego se añaden las capas de nuevo grupo


	//Ordenar las capas para que se añadan en orden
	var keys = []
	for(var pk in capas){
		keys.push(pk);
	}
	keys.sort(function (a, b) {
    if (+a.split('_')[0] > +b.split('_')[0]) {
      return 1;
    }
    if (+a.split('_')[0] < +b.split('_')[0]) {
      return -1;
    }
    return 0;
  });



	for(var pk of keys){
  		if(capas[pk].temas.split(' > ')[0] == grupoActual){
  			addLayerClick(pk,false);
  		}
  	}


		if(resultadosSpSS){
			refreshResults(getSelectedDistance());
		}

		if(mun){
      showResultSITA( 'codmun='+mun);
      showSIUa('cod_ine='+mun);
    }
 for (var i=0; i<resultadosSalud.length;i++){
	 
	 showResultSalud(resultadosSalud[i], 'objectid='+resultadosSaludZBS[i])
 }
 updateTOCList();
}


function toggleZoneToc(id) {
	if ($(id).css('display') == 'block') {
		$(id).hide();
	} else {
		$(id).show();
		if (id.indexOf("#moreDetails_")==0){
			updateLegend(getOLLayer("pk",id.replace("#moreDetails_","")));
		}
	}
}



function changeTocThemes() {
	layerList.search($("#layersContainer input").val());
}

function initTOC(app) {
	toc_group = new ol.layer.Group({ });
	map.addLayer(toc_group);

	$.ajax({
  		url: 'consultaLayers.jsp?SERVICE='+APP_NAME,
  		type: 'GET',
  		success:  function(data) {
				var json = JSON.parse($.trim(data));
				if (APP_NAME!='DVS'){
					$.ajax({
				  		url: 'consultaLayers.jsp?SERVICE=DVS',
				  		type: 'GET',
				  		success:  function(data) {
								var jsonS = JSON.parse($.trim(data));
								var data2 = changeFormat(json.capas.concat(jsonS.capas));
					      parseTOC(data2, '', '');
					      addGroupLayers();
					      changeGroup();
								
					     

				  	},
				  	error: function(jqXHR, textStatus, errorThrown) {

				  		alert("No se cargaron correctamente los indicadores de salud:"+jqXHR.responseText);
				  	}
				  });
				}
				else{
				var data2 = changeFormat(json.capas);
	      parseTOC(data2, '', '');
	      addGroupLayers();
	      changeGroup();
				}
	     

  	},
  	error: function(jqXHR, textStatus, errorThrown) {

  		alert("No se inicio correctamente la aplicación:"+jqXHR.responseText);
  	}
  });
}


function changeFormat(layers){

	var res = []
	var res = {
  						grupos: [],
  						capasWMS: [
  						]
  				}

  var orden = 1;
	var lastGroup;
  for(var i = 0; i<layers.length; i++){
    var visible = true;
    if(layers[i].bloque == "ECONOMIA" || layers[i].bloque == "SOCIEDAD" ||(layers[i].capas=='v_instalaciones_sanitarias_geom_otras')||(layers[i].capas=='v_instalaciones_sanitarias_geom_centro_sanitario')){
        visible = false;
    }
    var grupo = getGrupo(res.grupos,layers[i].bloque);
    
    if(grupo==null){
        res.grupos.push({
           pk: layers[i].bloque,
           titulo: layers[i].bloque,
           orden: orden,
           capas_excluyentes: (layers[i].capas_excluyentes ? "t":"f"),
           grupos: [],
           capasWMS:[]
       });
       orden++;
       
    }
    else  	if (layers[i].capas_excluyentes){
    	visible = false;
    }
    grupo = getGrupo(res.grupos,layers[i].bloque);
    var c = layers[i].capas.split(',');
    var t = layers[i].tablas.toLowerCase().split(',');

    if(c.length > 1){
      grupo.grupos.push({
           pk: i,
           titulo: layers[i].titulo,
          // orden: orden,
           capas_excluyentes: (layers[i].capas_excluyentes ? "t":"f"),
           grupos: [],
           capasWMS:[]
      })

      for(var j = 0; j<c.length;j++){
          var capa = {
             pk: i+"_"+j,
             //pk: i,
             url: "/"+layers[i].servicio,
             titulo: layers[i].titulo,
             tablas: t[j],
             grafico: layers[i].grafico,
             version: "1.1.1",
             format: "image/png",
             layers: c[j],
             opacidad: (layers[i].opacity ? layers[i].opacity :"1"),
             info: layers[i].info,
             glg: "t",
             visible:  visible
           }
           grupo.grupos[grupo.grupos.length-1].capasWMS.push(capa);
      }

    }else{
      var capa = {
         pk: i,
         url: "/"+layers[i].servicio,
         titulo: layers[i].titulo,
         tablas: layers[i].tablas,
         grafico: layers[i].grafico,
         version: "1.1.1",
         format: "image/png",
         layers: layers[i].capas,
         opacidad: (layers[i].opacity ? layers[i].opacity :"1"),
         info: layers[i].info,
         glg: "t",
         visible:  visible
       }
       grupo.capasWMS.push(capa);
    }
  }
  return res;
}


function getGrupo(grupos, tituloGrupo){
	for(var i = 0; i<grupos.length;i++){
		if(grupos[i].titulo == tituloGrupo){
			return grupos[i];
		}
	}
	return null;
}


function openTocDialog() {
	if(tocDialogCreated){
		if ($('#tocDialog').css("display")!="none"){
			$('#tocDialog').hide();
		}
		else{
		//closeAllDialogs();
		$('.dialogClose').hide();
    if(isSmall){
        $('#datosDialog').hide();
    }
		$('#tocDialog').show();
	
		}
	}
	else{
		$('#tocDialog').resizable({"alsoResize":"#tocDialog .dialogFullContent"});
		$('#tocDialog').draggable({
		cursor: 'move',
		containment: "#mainPage",
		scroll: false,
		handle: '.ui-header'
	});

	$("#tocToolbar").navbar();
	$(".tocTab").trigger("create");

	$("#tocDialog").addClass("ui-page-theme-a ui-corner-all");
	$("#tocDialog div[data-role='header']").addClass("ui-header ui-bar-inherit");
	$("#tocDialog div[data-role='button']").button();
	$(".dialogTitle").addClass("ui-title");

	$("#tocAdd").addClass("ui-block-a");
	$("#tocList").addClass("ui-block-b");
	$("#tocFondos").addClass("ui-block-c");
	$(".ui-tabs-anchor").addClass("ui-link ui-btn");
	$("#availableLayers a.addBtn").addClass("ui-link ui-btn ui-icon-plus ui-btn-icon-notext ui-shadow ui-corner-all");
	$("#availableLayers a.infoBtn").addClass("ui-link ui-btn ui-icon-info ui-btn-icon-notext ui-shadow ui-corner-all");

	$('.dialogClose').hide();
  if(isSmall){
      $('#datosDialog').hide();
  }
	$('#tocDialog').show();
	$("#closeButtonTocDialog").bind("vclick", function(event, ui) { event.preventDefault();$('#tocDialog').hide(); });
	
	tocDialogCreated=true;
	}
	
}


function closeTocDialog() {
	$('#tocDialog').hide();
}

function saveTocList(order) {
	if (!order && !tokenId) {
		return;
	}
	var user_toc = new Object();
	user_toc.capas = [];
	var num = $("#layers li").length;
	$("#layers li").each(function(index) {
		var id = $(this)[0].id;
		if (id && !$(this).hasClass('ui-sortable-helper')) {

			id = id.replace("wmslayer_", "wms_").replace("layer_", "");
			if (tokenId) {
				var capa = new Object();
				capa.layer_id = id;
				capa.visible = getOLLayer("pk", id).getVisible();
				user_toc.capas.push(capa);
			}

			if (order) {
				setLayerZIndex(id, num-index);
			}
		}
	});
	if (tokenId) {
		$.ajax({
			url: server + "/BD_GIS/saveTOC.jsp?token=" + tokenId + "&toc=" + JSON.stringify(user_toc) + "&app=" + app,
			type: 'POST',
			success:	function(userTOC) {
				console.log("TOC de usuario guardado");
			},

			error: 		function(data, textStatus, errorThrown) {
				console.log('Error obteniendo el listado de capas del usuario. State: ' + data.readyState + ", Status:" + data.status);
				return false;
			}
		});
	}
}

function loadWMTSCapabilities(wmts, pk) {
	for(var i = 0; i<wmts.length; i++) {
		if (wmts[i].pk == pk) {
			$.ajax({
				url: wmts[i].url+"?REQUEST=GetCapabilities",
				type: "GET",
				async: false,
				success: function(data) {
					wmts_capabilities[pk] = data;
				},
				error: function(jqXHR, textStatus, errorThrown) { console.log("Error al obtener el capabilities de "+wmts.url+" "+textStatus +"\n "+errorThrown);},
				contentType: "text/xml",
				dataType: "text"
			});
			return;
		}
	}
	console.log("No se ha encontrado el wmts con pk=" + pk);
}


function updateStyleLayer(obj, layerId) {
	var id = layerId;
	var new_style = obj.value;

	//TODO: Revisar con las user layer
	if (layerId.indexOf("userlayer") < 0) {
		id = layerId.replace("layer_", "");
	}

	// TODO: Revisar esta parte para app movil
	var layer = getOLLayer("pk", id.replace("userlayer_", "ul_"));

	var foo = layer.getSource().getParams();
	foo.STYLES = obj.value;
	layer.getSource().updateParams(foo);

	updateLegend(layer);
}

function updateLegends() {
	var layers = toc_group.getLayers();
	for (var i = 0; i < layers.getLength(); i++) {
		var pk = layers.item(i).get("pk");
		if ($("#moreDetails_"+pk).css("display")!="none"){
			updateLegend(layers.item(i));
		}
		
	}
}

function onlegenderror(obj, pklayer) {
	// TODO: Revisar si es necesario lo siguiente
	//document.getElementById(id).title=url;
	obj.attr("src", "/lib/IDEAragon/themes/default/images/noLegend.png");
	obj.error(function() {
	});

	obj.removeClass("oculto");	
	if ($("#leyenda_"+pklayer+" .imgLey:not(.oculto)").length>1){
		obj.addClass("oculto");
	}
	obj.addClass("legendError");
}

function getCurrentScale() {
	return map.getView().getResolution()*DOTS_PER_M;
}

function addLegend(layer){

	var url = "";
	var layer_legend = layer.get('legend');

	if (layer_legend) {
		var legend_class = layer_legend.replace(/ /g, '_').replace(/\./g, '_').replace(/\//g, '_').replace(/&/g, '_').replace(/\?/g, '_').replace(/=/g, '_');
		url = "/lib/IDEAragon/legends/" + layer_legend;
		if ($("."+legend_class).length > 0) {
			legend_class += ' undisplayed';
		}

		if (layer_legend.indexOf("http") == 0) {
			legend_class += " legendHttp";
			url = layer_legend;
		}
		$('#legend_panel_' + layer.get('pk')).html("<img class=\"imgLey\" src=\""+url+"\" onerror=\"onlegenderror($(this),'"+layer.get('pk')+"')\">");
	} else if (layer.get('glg') && layer.get('glg')!='f') {


		var theCode = "<div class='leyendaCapa' id=\"leyenda_"+layer.get('pk')+"\">";
		var ows_layer = layer.getSource().getParams().LAYERS;
		var listaLeyendas = ows_layer.split(',');

		for (var layerItem in listaLeyendas) {
				theCode += "<img id='imgLey_"+listaLeyendas[layerItem]+"' class=\"imgLey\" onerror=\"onlegenderror($(this),'"+layer.get('pk')+"')\">";
		}

		theCode += "</div>";
		$('#legend_panel_' + layer.get('pk')).html(theCode);
		updateLegend(layer);
	}
}

function updateLegend(layer) {

	var url = "";
	if (layer.get('glg') && layer.get('glg')!='f') {
		try {
			url = layer.getSource().getUrls()[0];
		}
		catch(err) {
			url = layer.getSource().getUrl();
		}

		if (url.indexOf("?") < 0) {
			url = url+"?";
		}
		var ows_layer = layer.getSource().getParams().LAYERS;
		var ows_style = layer.getSource().getParams().STYLES;
		var currentScale = getCurrentScale();
		var listaLeyendas = ows_layer.split(',');
		var listaStyles = ows_style.split(',');
		for (var layerItem in listaLeyendas) {
			var currentStyle;
			try {
				currentStyle = listaStyles[layerItem];
			} catch(err) {
				console.log("Problemas en la definicion de la capa pk " + layer.get('pk') + ". Hay diferencia entre el numero de styles y de layers");
			}

			urlCurrentLegend = url + "SERVICE=WMS&VERSION=1.0.0&REQUEST=GetLegendGraphic&format=image/png&layer=" + listaLeyendas[layerItem] + "&width=20&height=20&legend_options=forceLabels:on&SCALE="+currentScale+(ows_style? "&STYLE="+currentStyle:"");
			$('#imgLey_' + listaLeyendas[layerItem].replace("/","\\/")).removeClass("oculto");
			$('#imgLey_' + listaLeyendas[layerItem].replace("/","\\/")).attr("src",urlCurrentLegend);
			
		}

		
		
	}
}

function getMoreDetailsFromLayer(capa) {
	var comboStyles = '';

	if (capa.styles != null) {
		var listaStyles = capa.styles.split('#');
		var usarTitle = false;

		var listaStylesTitle = null;
		if (capa.tituloestilo != null) {
			listaStylesTitle = capa.tituloestilo.split('#');
			if (listaStyles.length == listaStylesTitle.length) {
				usarTitle = true;
			}
		}

		if (listaStyles.length > 1) {
			comboStyles = "<select data-mini='true' class='styleCombo' id='styleCombo_" + capa.pk + "' onchange='updateStyleLayer(this, \"layer_" + capa.pk + "\")'>";
			for (var styleItem in listaStyles) {
				var styleName = listaStyles[styleItem];
				comboStyles += "<option value='" + styleName + "'>";
				if (usarTitle) {
					comboStyles += listaStylesTitle[styleItem];
				} else {
					comboStyles += styleName;
				}
				comboStyles += "</option>";
			}
			comboStyles += "</select>";
		}
	}

	var moreDetails = "<div id='moreDetails_" + capa.pk + "' style='display:none'>";
	if (comboStyles != '') {
		moreDetails += "Estilos: " + comboStyles;
	}

	moreDetails += "<div id='legend_panel_" + capa.pk + "'></div>";
	moreDetails += "</div>";
	return moreDetails;
}

//equivalente a addLayer pero no devuelve nada, para evitar redirección en Firefox
function addLayerClick(index, visible) {
	addLayer(index, visible);
}

function addLayer(index, visible) {
	// volver a marcar el boton activo porque se desmarca al hacer click en otro button
	$("#tocAddButton").addClass("ui-btn-active");
	var capa = capas[index];
	var groupIndx = index.split('_')[0];

	$("#layer_" + groupIndx).remove();
	var es_visible = (visible != null ? visible : capa.visible);


	//SelectItem, unselectItem
	//onVisibleClick,toggleZoneToc
	$("#layers").append("<li class='notSelected' id='layer_"+groupIndx+"'  style='color:grey'><div class='name'>"+
			getCheckBoxHtml(groupIndx, es_visible, capa.oneVisLayerGroup) +
			//"<a class='infoBtn' href='javascript:toggleZoneToc(\"#info_" + groupIndx + "\")' data-iconpos='notext' data-icon='info' data-role='button' title='Ver detalles'></a>" +
			"<a class='legendBtn' href='javascript:toggleZoneToc(\"#moreDetails_" + capa.pk + "\")' data-iconpos='notext' data-icon='bullets' data-role='button' title='Ver leyenda'></a>" +

			"<span class='titleLayer' id='title_"+groupIndx+"'>"+ capa.titulo +"</span>"+
			"<span class='titleLayer' id='count_"+groupIndx+"'> (0)</span>"+
			//"<span class='titleLayer' id='count_"+ groupIndx+"'>"+ capa.titulo +"</span>" +
			getMoreDetailsFromLayer(capa) +
			"<div class='toc_info' id='info_"+groupIndx+"' style='display: none;'>"+(capa.info ? capa.info : "No hay información sobre la capa")+ "</div>"+
	"</div></li>");

	if (es_visible && capa.oneVisLayerGroup) {// la capa es excluyente con otras y está marcada como visible
		// ocultar las capas con las que es excluyente
		noVisibleLayer(capa.oneVisLayerGroup);
		openOnly(index,true);
	}
	// TODO: Ver que pasa con las capas del usuario
	var olLayer;
	if (capa.type == 'wms') {
		olLayer = addWMSLayer(index, es_visible);
	} else if (capa.type == 'wmts') {
		olLayer = addWMTSLayer(index, es_visible);
	}


	var opacity = 0.01;
	var vectorSource =   new ol.source.Vector({features: []});

	var wfs_layer;

	if (capa.tablas=="gasolineras"){
				opacity=1;
				wfs_layer = new ol.layer.Vector({
					source: vectorSource,
					style: new ol.style.Style({
						fill: new ol.style.Fill({
							color: 'rgba(0,0,0,'+opacity+')'
						}),
						stroke: new ol.style.Stroke({
							color:'rgba(0,162,232,'+opacity+')',
							width: 2
						}),
						image:new ol.style.Icon({src:'themes/dv/images/gasolinera_verde_borde_negr.png'})
					})
				});
		}else{
			wfs_layer = new ol.layer.Vector({
              source: vectorSource,
              style: new ol.style.Style({
                  fill: new ol.style.Fill({
                      color: 'rgba(0,0,0,'+opacity+')'
                  }),
                  stroke: new ol.style.Stroke({
                      color:'rgba(0,162,232,'+opacity+')',
                      width: 2
                  }),
                  image: new ol.style.Circle({
                      fill: new ol.style.Fill({
                          color: 'rgba(0,0,0,'+opacity+')'
                      }),
                      stroke: new ol.style.Stroke({
                          color:'rgba(0,0,0,'+opacity+')',
                          width: 2
                      }),
                      radius: 5
                  })
              })
          });
		}

   wfs_layer.set('tabla', capa.tablas);
	 wfs_layer.set('url', capa.url);
	 wfs_layer.set('capa', capa.layers);
	 wfs_layer.set('caption', capa.titulo);
	 wfs_layer.set('idx', index);
   wfs_layer.set('pk', capas[index].pk);
   wfs_layer.set('grafico', capas[index].grafico);
	 wfs_layer.setVisible(es_visible);


   var layers2 = toc_group.getLayers();
	  layers2.push(wfs_layer);
	  toc_group.setLayers(layers2);



	addLegend(olLayer);


	//updateTOCList();
	//if ((typeof visible === "undefined") || visible == null) { // no es carga del toc de usuario
	
		$("#layer_" + groupIndx).trigger("create");

	//}
	return olLayer;

}


function getCheckBoxHtml(layerId, visible, groupOneLayer) {
	
	var clickFunction = 'onVisibleClick(event,\''+layerId+'\')';
	if (groupOneLayer != null) {
		clickFunction = 'onVisibleOneClick(event,\''+layerId+'\',\''+groupOneLayer+'\')';
	}
	if (visible) {
		return '<a id="chk_layer'+layerId+'" class="chk_layer" style="visibility:hidden" href="#" title="Pulse para ocultar"><img class="imgToc" src="'+img_path+'/imagesTOC/icon_visible.png" onmousedown="'+clickFunction+';" alt="Pulse para mostrar" border="0" height="16" width="16"></a>';
	} else {
		return '<a id="chk_layer'+layerId+'" class="chk_layer" style="visibility:hidden" href="#" title="Pulse para mostrar"><img class="imgToc" src="'+img_path+'/imagesTOC/icon_hidden.png" onmousedown="'+clickFunction+';" alt="Pulse para ocultar" border="0" height="16" width="16"></a>';
	}
}

function closeTOCWin() {
	$("#tocDialog").dialog("close");

}
function successHandler(transaction) {
	console.log("Correcto");
}
function errorHandler(transaction, error) {
	console.log("Error : " + error.message);
}

function getOLLayer(campo, layerIdx) {
	var layers = toc_group.getLayers();
	for (var i = 0; i < layers.getLength(); i++) {
		var layer = layers.item(i);
		if (layer.get(campo) == layerIdx) {
			return layer;
		}
	}
}

function getOLLayerByTabla(tabla) {
	var group = [];
	var layers = toc_group.getLayers();
	for (var i = 0; i < layers.getLength(); i++) {
		var layer = layers.item(i);
		if (layer.values_.tabla == tabla) {
			//return layer;
			group.push(layer);
		}
	}
	return group;
}

function getOLLayerGroup(campo, groupIdx) {

	var group = [];
	var layers = toc_group.getLayers();
	for (var i = 0; i < layers.getLength(); i++) {
		var layer = layers.item(i);
		if (layer.get(campo) && layer.get(campo).split('_')[0] == groupIdx) {
			//return layer;
			group.push(layer);
		}
	}
	return group;
}

function checkVisibility(){
	var layers = toc_group.getLayers();
  for (var i = 0; i < layers.getLength(); i++) {
    var layer = layers.item(i);

    if((capas[layer.get("pk")].visible)){
      setVisibleLayer(layer, layer.get("idx"), true);
     
    }
  }
}

function onVisibleClick(event,groupIdx, openData, notCheckTimestamp) {

	// en chrome de móvil se llama dos veces seguidas a este evento. Para que no se marque y desmarque seguidamente (y viceversa) controlamos la diferencia de los timestamps de un evento con el anterior
	if (notCheckTimestamp || (event==null || (event.timeStamp - visibleClickTimestamp>100))){
		if (event){
			visibleClickTimestamp=event.timeStamp;
		}
		var layerGroup = getOLLayerGroup("idx", groupIdx);


		for(var i = 0; i<layerGroup.length; i++){
			if (i==0){
				if(layerGroup[i].getVisible()){
					closePopupLayer(layerGroup[i].get("idx"));
				}
				setVisibleLayer(layerGroup[i], layerGroup[i].get("idx"), !layerGroup[i].getVisible());

			}
			else{
				layerGroup[i].setVisible(!layerGroup[i].getVisible());
			}
		}
		if ((layerGroup.length>0) && openData){
			openOnly(layerGroup[0].get("idx"),true);
		}
	}
}

function onVisibleOneClick(event,layerIdx, groupOneVisible) {
	// en chrome de móvil se llama dos veces seguidas a este evento. Para que no se marque y desmarque seguidamente (y viceversa) controlamos la diferencia de los timestamps de un evento con el anterior
	if ((event==null)||(event.timeStamp - visibleClickTimestamp>100)){
	var layer = getOLLayer("idx", layerIdx);
	var visible = ((layer instanceof ol.layer.Group)?layer.get('isVisible'):layer.getVisible());
	var openData=false;
	if (!visible) { // si se va a activar, primero desactivar todas las del grupo excluyente
		noVisibleLayer(groupOneVisible);
		openData=true;
	}
	onVisibleClick(null,layerIdx, openData, true);
	}
}

function noVisibleLayer(group) {
	$("#layers li").each(function(index) {
		var id = $(this)[0].id.replace("layer_","");
		if (capas[id].oneVisLayerGroup && (capas[id].oneVisLayerGroup == group)) {
			var layer = getOLLayer("idx", id);
			if (layer) {// será nula si la capa se ha añadido a la lista pero no al mapa todavía (en ese caso al añadirla al mapa se pondrá visible)
				setVisibleLayer(getOLLayer("idx", id), id, false);
			}
		}
	});
}

function setVisibleLayer(layer, layerIdx, visible) {
	var groupIndex= layerIdx.split('_')[0];

	if (!visible) {
		$("#chk_layer"+groupIndex)[0].title = "Pulse para mostrar";
		$("#chk_layer"+groupIndex+" img")[0].alt = "Pulse para mostrar";

		$("#chk_layer"+groupIndex+" img")[0].src = img_path + "/imagesTOC/icon_hidden.png";

		//layer.setVisible(false);
	} else {
		$("#chk_layer"+groupIndex)[0].title = "Pulse para ocultar";
		$("#chk_layer"+groupIndex+" img")[0].alt = "Pulse para ocultar";
		$("#chk_layer"+groupIndex+" img")[0].src = img_path + "/imagesTOC/icon_visible.png";
		
		//layer.setVisible(true);
	}
	layer.setVisible(visible);
	capas[layerIdx].visible = visible;
	
}

function addQueryableLayer(layer){
	var index = layer.get("idx");
	var escala_info =layer.get("escala_info"); 
	if (escala_info && layer.getVisible() && $("#info_layer_opt_"+index).length==0){
		
			$("#info_layer").append("<option id='info_layer_opt_"+index+"' value='"+index+"'>"+layer.get("caption")+"</option>")
		
	}
}

function getWMTSIdx(wmts_pk){
	for (var i =0; i<serviciosWMTS.length; i++){
		if (serviciosWMTS[i].pk==wmts_pk){
			return i;
		}
	}
	return -1;
}
function addWMTSLayer(index, visible) {

	console.log("WMTS")
	var capa = capas[index];

	if (!wmts_capabilities[capa.wmts]) {
		loadWMTSCapabilities(serviciosWMTS, capa.wmts);
	}

	var capabilities = wmts_capabilities[capa.wmts];
	if (capabilities) {
		var layer;
		var parser = new ol.format.WMTSCapabilities();
		var result = parser.read(capabilities);
		var options = ol.source.WMTS.optionsFromCapabilities(result,
				{layer: capa.layer, matrixSet: capa.gridset, format: capa.format});

		layer = new ol.layer.Tile({
			source: new ol.source.WMTS(options)
		});

		layer.setVisible(visible == null ? capa.visible: visible);
		layer.setOpacity(capa.opacidad);
		layer.set('idx', index);
		layer.set('pk', capa.pk);
		layer.set('caption', capa.titulo);
		layer.set('glg', capa.glg);
		// TODO: Ver de donde se saca esto
		//layer.set('legend', legendURL);
		layer.set('legend', capa.leyenda);
		layer.set('wmts_pk',capa.wmts);
		// TODO: Revisar si esto debe considerarse o no. Creo que no viene nada de esto en getToc.jsp
		//layer.set('minScale', minScale);
		//layer.set('hidden', hidden);

		var layers = toc_group.getLayers();
		layers.push(layer);
		toc_group.setLayers(layers);
		return layer;
	} else {
		console.log("Error incluyendo capabilities de WMTS");
	}
}

function addWMSLayer(index, layer_visible) {
	var caption = capas[index].titulo;
	var tabla = capas[index].tablas;
	var capa = capas[index].layers;
	var url = (capas[index].url.indexOf("http") == 0 ? capas[index].url : server + capas[index].url);
	var layers = capas[index].layers;
	var styles = (capas[index].styles?capas[index].styles:"");
	if (styles != null) {
		lista_styles = styles.split('#');
		styles = lista_styles[0];
	}
	var version = capas[index].version;
	var format = capas[index].format;
	var visible = (layer_visible == null ? capas[index].visible : layer_visible);
	var opacity = capas[index].opacidad;
	var tiled = capas[index].teselada == "t";
	var layer;


	if (tiled) {
		layer = new ol.layer.Tile({
			source: new ol.source.TileWMS({
				params: {'LAYERS': layers, 'VERSION':version, 'FORMAT':format, 'STYLES':styles},
				url: url,
				projection: map_projection
			})
		});
	} else {
		layer = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				params: {'LAYERS': layers, 'VERSION':version, 'FORMAT':format, 'STYLES':styles},
				url: url,
				imageLoadFunction: function (image, src) {

                                  				        var img = image.getImage();
                                  				        if (typeof window.btoa === 'function') {
                                  				          var urlArray = src.split("?");
                                  				          var url = urlArray[0];
                                  				          var params = urlArray[1];

                                  				          var xhr = new XMLHttpRequest();
                                  				          xhr.onload = function (e) {
                                  				            if (this.status === 200) {
                                  				              var uInt8Array = new Uint8Array(this.response);
                                  				              var i = uInt8Array.length;
                                  				              var binaryString = new Array(i);
                                  				              while (i--) {
                                  				                binaryString[i] = String.fromCharCode(uInt8Array[i]);
                                  				              }
                                  				              var data = binaryString.join('');
                                  				              var type = xhr.getResponseHeader('content-type');
                                  				              if (type.indexOf('image') === 0) {
                                  				                img.src = 'data:' + type + ';base64,' + window.btoa(data);
                                  				              }
                                  				            }
                                  				          };
                                  				          xhr.open('POST', url, true);
                                  				          xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                  				          xhr.responseType = 'arraybuffer';
                                  				          xhr.send(params);
                                  				        } else {
                                  				          img.src = src;
                                  				        }
                                  				      },
				projection: map_projection
			})
		});
	}




	layer.setVisible(visible);
	layer.setOpacity(opacity);
	layer.set('idx', index);
	layer.set('pk', capas[index].pk);
	layer.set('caption', caption);
	layer.set('tabla', tabla);
	layer.set('capa', capa);
	layer.set('legend', capas[index].leyenda);
	layer.set('glg', capas[index].glg);
	layer.set('escala_info', capas[index].escala_info);
	layer.set('url', capas[index].url)

	//addQueryableLayer(layer);
	var layers2 = toc_group.getLayers();
	layers2.push(layer);
	toc_group.setLayers(layers2);
	return layer;

}

function setLayerZIndex(id, index) {
	getOLLayer("pk", id).setZIndex(index);
}

function showDelLayerPopup() {
	$("#delLayerPopup").popup("open", {"positionTo":"#tocDelete"});
}
function decTranspTocSlider(slider) {
	var value = Math.max($("#"+slider).val() - 10, 0);
	$("#"+slider).val(value);
	$("#"+slider).slider("refresh");
	updateTocTransp();
}

function incTranspTocSlider(slider) {
	var value = Math.min(parseInt($("#"+slider).val()) + 10, 100);
	$("#"+slider).val(value);
	$("#"+slider).slider("refresh");
	updateTocTransp();
}

function updateTocTransp() {
	var transp = $("#transpTocSlider").val();
	toc_group.setOpacity((100-transp)/100);
}

function showLoadingWMS(show){
	if (show){
		$("#wmsOverlay").show();
	}
	else{
		$("#wmsOverlay").hide();
	}
}
function addAllWMSLayers(){
	showLoadingWMS(true);
	$("#availableWMSLayers .addBtn").trigger("click");
	$("#addAllWMSLayers").hide();
	showLoadingWMS(false);
	closeUserLayerDialog();
}

function closeUserLayerDialog() {
	$('#userLayerDialog').hide();
	openTocDialog();
}
function updateTOCList() {
	if (layerList) {
		
		layerList.reIndex();
		layerList.update();
		layerList.search($("#layersContainer input").val());
	} else {
		layerList = new List('layersContainer', {
			valueNames: ['name']
		});

		//layerList.alphabet = "AaÁáBbCcDdEeÉéFfGgHhIiÍíJjKkLlMmNnÑñOoÓóPpQqRrSsTtUuÚúÜüVvXxYyZz";

	/*	layerList.on('updated', function(list) {
			$("#availableLayers li").trigger("create");
			var value = $('#toc_themes').val();
			if (value == 'all') {
				$(".item_toc").show();
			} else {
				$(".item_toc").hide();
				$("." + value).show();
			}
			if (list.matchingItems.length > 0) {
				var el = $("#availableLayers li").filter(function() {
					return $(this).css('display') == 'list-item';
				});
				if (el.length > 0) {
					$('#layersContainer .no-result').hide();
				} else {
					$('#layersContainer .no-result').show();
				}
			} else {
				$('#addlayersContainer .no-result').show();
			}
		});
		$("#addlayersContainer").css('display', 'inline-block');*/
	}
}