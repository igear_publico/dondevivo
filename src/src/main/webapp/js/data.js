var datosDialogCreated=false;

var selectedPlot= null;
CHART_COLORS=["#a6cee3","#1f78b4","#b2df8a","#33a02c","#fb9a99","#e31a1c","#fdbf6f","#ff7f00","#cab2d6","#6a3d9a","#BDBDBD","#5DBBEA","#08519C","#83D17B","#006D2C","#FCCBBD","#A50F15","#E0AC82","#E28436","#E0E1FC","#7E63A3","#D9D9D9","#969696","#84C3FF","#048DEF","#C7E9C0","#2B7742","#FCBBA1","#C60A03","#FEEDDE","#FC5F16","#B8B8D6","#756BB1"];
CHART_COLORS_ICA=["#00b1a1","#9e00b1","#f3a646","#b30909","#060606","#7E63A3","#048DEF","#B8B8D6"];

function openDatosDialog(notCheckVis) {
	console.log("openDatosDialog");
	if ($('#datosDialog').css("display")!="none"){
		$('#datosDialog').hide();
	}else{

		$("#data_accordion").empty();


		$('#datosDialog').resizable({"alsoResize":"#datosDialog .dialogFullContent"});
		$('#datosDialog').draggable({
			cursor: 'move',
			containment: "#mainPage",
			scroll: false,
			handle: '.ui-header'
		});


		$("#datosDialog").addClass("ui-page-theme-a ui-corner-all");
		$("#datosDialog div[data-role='header']").addClass("ui-header ui-bar-inherit");
		$("#datosDialog div[data-role='button']").button();
		$(".dialogTitle").addClass("ui-title");

		$(".ui-tabs-anchor").addClass("ui-link ui-btn");

		//closeAllDialogs();
		$('.dialogClose').hide();
		if(isSmall){
			$('#tocDialog').hide();
		}

		$('#datosDialog').show();

		$("#closeButtonDatosDialog").bind("vclick", function(event, ui) { event.preventDefault();$('#datosDialog').hide(); });

		var lastCreated;
		var chartsNumber = 0;
		var layers = toc_group.getLayers();
		for (var i = 0; i < layers.getLength(); i++) {
			var layer = layers.item(i);
			if ((layer instanceof ol.layer.Vector) && (layer.get("grafico") == "1") && (layer.get("capa") != "fianzas") && ((selectedPlot==null || selectedPlot==layer.get("idx")))) {
				console.log("showData");
				showData(layer, notCheckVis);
				chartsNumber++;
				lastCreated=layer;
			}
		}
		selectedPlot = null;
		if(chartsNumber > 0){
			$('#d_'+lastCreated.get("capa").replace("/","\\\\\\/")).collapsible("expand");
			$("#no_data").hide();
		}else{
			$("#no_data").show();
		}
		//$('#d_'+layer.get("capa")).collapsible("expand");
		datosDialogCreated=true;

	}

}


function openOnly(capa, notVisClick){
		var layer = getOLLayer("idx", capa);
		var visible = ((layer instanceof ol.layer.Group)?layer.get('isVisible'):layer.getVisible());
		if(!visible && !notVisClick){
			if (capas[capa].oneVisLayerGroup){
				onVisibleOneClick(null,capa, capas[capa].oneVisLayerGroup);
			}
			else{
			onVisibleClick(null,capa);
			}
		}

		$('#datosDialog').hide();
		selectedPlot = capa;
		openDatosDialog(notVisClick);
}

function showICAChart(id_estacion, title){
	$('#datosDialog').hide();
	openDatosDialog();
	$("#no_data").hide();
	var idchart;
	var capa ='ica';
	var collapsed = "<div class='d_"+capa+"'>"+
                                "<div class='chartSITA' id='chart_"+capa+"_"+id_estacion+"' style='width:90%'></div>"+
                                
                            "</div>";
  $("#data_accordion").append("<div id='d_"+capa+"' class='d_"+capa+"' style='margin:0px' data-role='collapsible' ><h3>"+title+"</h3>"+
          collapsed+"</div>");


	$.ajax({
		url: "/BD_GIS/consultaValoresICA.jsp?REQUEST=SERIE&CODMUN="+id_estacion,
		type: 'GET',
		success: function (data) {
			var series = new Object();
			var legend_tooltip=new Array();
			var valores =JSON.parse(data).valores;
			for(var i=0; i<valores.length; i++){
				if (!series[valores[i].magnitud]){
					series[valores[i].magnitud] = new Array();
					legend_tooltip.push(valores[i].magnitud);
				}
				series[valores[i].magnitud].push([valores[i].tempo,parseFloat(valores[i].valor_medido)]);
			}
			var chart_values = new Array();
		
			for (var i=0;i<legend_tooltip.length; i++){
				chart_values.push(series[legend_tooltip[i]]);
			}
			createChartEvolPlotSeries("chart_"+capa+"_"+id_estacion,'\xB5g/m2',chart_values,legend_tooltip,null,true,CHART_COLORS_ICA);
			  $('#d_'+capa).collapsible();
			  bindCollapse("d_"+capa);
			  $('#d_'+capa).collapsible("expand");
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("No se han podido obtener los valores de la estación " + feature.getProperties().id + "(" + textStatus + ")\n " + errorThrown);

		}
	});
	
}
function showFeatureChart(capa,unidades,id,title,valores){
	$('#datosDialog').hide();
	openDatosDialog();
	$("#no_data").hide();
	var idchart;
	var max_data1 = [];
	var min_data1 = [];
	var avg_data1 = [];
	var max_data2 = [];
	var min_data2 = [];
	var avg_data2 = [];
	var datos = eval(valores);
	for (var j=0; j<datos.length;j++){
		if (datos[j].tipo==1){
			max_data1.push([""+datos[j].anyo,datos[j].max]);
			min_data1.push([""+datos[j].anyo,datos[j].min]);
			avg_data1.push([""+datos[j].anyo,datos[j].media]);
		}
		else{
			max_data2.push([""+datos[j].anyo,datos[j].max]);
			min_data2.push([""+datos[j].anyo,datos[j].min]);
			avg_data2.push([""+datos[j].anyo,datos[j].media]);
		}
	}
	var collapsed = "<div class='d_"+capa+"'>"+
                                "<div class='chartSITA' id='chart_"+capa+"_"+id+"_1' style='width:90%'></div>"+
                                "<div class='chartSITA' id='chart_"+capa+"_"+id+"_2' style='width:90%'></div>"+
                            "</div>";
  $("#data_accordion").append("<div id='d_"+capa+"' class='d_"+capa+"' style='margin:0px' data-role='collapsible' ><h3>"+title+"</h3>"+
          collapsed+"</div>");

	if(avg_data1.length>0){
	createChartEvolPlot("chart_"+capa+"_"+id+"_1",unidades,
			[avg_data1,min_data1,max_data1],['Viviendas (media)','Viviendas (mín.)','Viviendas (máx.)']);
	}
	if(avg_data2.length>0){
	createChartEvolPlot("chart_"+capa+"_"+id+"_2",unidades,
			[avg_data2,min_data2,max_data2],['Locales (media)','Locales (mín.)','Locales (máx.)']);
	}

  $('#d_'+capa).collapsible();
  bindCollapse("d_"+capa);
  $('#d_'+capa).collapsible("expand");
}

function bindCollapse(id){
	$( "#" + id+" h3" ).unbind();
	$( "#" + id+" h3" ).bind("click",function(event){
		collapse(event,id);
	});
}
function showData(layer, notCheckVis){

	if (layer.getVisible() || notCheckVis){
		console.log("visible");
		var datos = null;
		var esSalud=false;
		try{
			if (resultadosSITA && resultadosSITA[layer.get("capa")]){
				datos = resultadosSITA[layer.get("capa")];		
			}
			
			else{
				datos = resultadosSalud[0][layer.get("capa")];
				esSalud=true;
			}
			
		}
		catch(err){
			console.log("SITA no se ha cargao todavía");
		}
		if (datos){ // son datos de sita

			var collapsed = "<div class='d_"+layer.get("capa")+" grafico'>"+
                     							"<p><b>Fuente</b>: "+datos.fuente+"</p><p><b>Unidades</b>: "+datos.unidad+
                     									(esSalud && resultadosSalud.length>1 ? "":"</p><p><b>Último dato disponible ("+formatDate(layer.get("lastDate"))+")</b>: "+layer.get("lastValue")+"</p>" )+
                     									"</p><p><b>Escala del dato</b>: "+(esSalud ? 'Zona Básica de Salud':'Municipal')+
                     								"</p>" +
                     							"<div class='chartSITA' id='chart_"+layer.get("capa")+"' style='width:90%'></div>"+
                     					"</div>";


			$("#data_accordion").append("<div id='d_"+layer.get("capa")+"' class='d_"+layer.get("capa")+"' style='margin:0px' data-role='collapsible' ><h3>"+layer.get("caption")+"</h3>"+
              collapsed+"</div>");


			var serie =[]  
			if(esSalud){
				showDataSalud(layer.get("capa"));
			}
			else{
			for (var i=0; i<datos.valores.length;i++){
				serie.push([formatDate(datos.valores[i][0]),datos.valores[i][1]]);
			}
			createChartEvolPlot("chart_"+layer.get("capa"),datos.unidad,[serie]);
			}
		}

		else{  //son datos de SpSS
			var features = layer.getSource().getFeatures();
			if (features.length<=0){
				setTimeout(function(){ showData(layer); }, 1000);
				return;
			}
			var collapsed = "<div class='d_"+layer.get("capa")+" grafico'>"+
                       							"<div class='chartSITA' id='chart_"+layer.get("capa")+"1' style='width:90%'></div>"+
                       							"<div class='chartSITA' id='chart_"+layer.get("capa")+"2' style='width:90%'></div>"+
                       					"</div>";
      $("#data_accordion").append("<div id='d_"+layer.get("capa")+"' class='d_"+layer.get("capa")+"' style='margin:0px' data-role='collapsible' ><h3>"+layer.get("caption")+"</h3>"+
              collapsed+"</div>");


			var max=0;
			var min=999999;
			var sum=0;
			var count=0;
			var serie = new Array();
			for (var i=0;i<features.length;i++){
				var feature = features[i];
				var datos = eval(feature.get("valores"));
				for (var j=0; j<datos.length;j++){
					var idSerie = datos[j].anyo+"_"+datos[j].tipo;
					if (serie[idSerie]){
						serie[idSerie].max = Math.max(datos[j].max,serie[idSerie].max);
						serie[idSerie].min = Math.min(datos[j].min,serie[idSerie].min);
						serie[idSerie].sum = (datos[j].media*datos[j].num)+serie[idSerie].sum;
						serie[idSerie].count = datos[j].num+serie[idSerie].count;

					}
					else{
						serie[idSerie]=new Array();
						serie[idSerie].max = datos[j].max;
						serie[idSerie].min = datos[j].min;
						serie[idSerie].sum = datos[j].media*datos[j].num;
						serie[idSerie].count = datos[j].num;
					}

				}
			}
			var anyos = Object.keys(serie);
			var max_data1 = [];
			var min_data1 = [];
			var avg_data1 = [];
			var max_data2 = [];
			var min_data2 = [];
			var avg_data2 = [];
			for (var i=0; i<anyos.length; i++){
				var fila = serie[anyos[i]];
				var ids = anyos[i].split("_");
				if (ids[1]==1){
					max_data1.push([ids[0],fila.max]);
					min_data1.push([ids[0],fila.min]);
					avg_data1.push([ids[0],fila.sum/fila.count]);
				}
				else{
					max_data2.push([ids[0],fila.max]);
					min_data2.push([ids[0],fila.min]);
					avg_data2.push([ids[0],fila.sum/fila.count]);
				}
			}
			if(avg_data1.length>0){
			createChartEvolPlot("chart_"+layer.get("capa")+"1",'Euros',
					[avg_data1,min_data1,max_data1],['Viviendas (media)','Viviendas (mín.)','Viviendas (máx.)']);
			}

			if(avg_data2.length>0){
			createChartEvolPlot("chart_"+layer.get("capa")+"2",'Euros',
					[avg_data2,min_data2,max_data2],['Locales (media)','Locales (mín.)','Locales (máx.)']);
			}
		}
	}
	else{
		$(".d_"+layer.get("capa")).remove();
	}

  $('#d_'+layer.get("capa").replace("/","\\\\\\/")).collapsible();
  bindCollapse("d_"+layer.get("capa").replace("/","\\\\\\/"));
	//$("#data_accordion").accordion("refresh");
	//$("#data_accordion").accordion("option","active",$("#data_accordion h3").length-1);
}


function collapse(event,id){
	// en chrome de móvil se llama dos veces seguidas a este evento. Para que no se expanda y colapse seguidamente (y viceversa) controlamos la diferencia de los timestamps de un evento con el anterior
	if (event.timeStamp - collapsedTimestamp>100){
		collapsedTimestamp=event.timeStamp;
	cancelBubble(event);
	event.preventDefault();
	if ($("#"+id).collapsible( "option", "collapsed" )){
		$("#"+id).collapsible("expand");
	}
	else{
		$("#"+id).collapsible("collapse");
	}
	}

}

function formatDate(fecha){
	if (fecha.length==4){
		return fecha+"-01-01";
	}
	if (fecha.length==6){
		return fecha.substr(0,4)+"-"+fecha.substr(4,2);
	}
	if (fecha.length==8){
		return fecha.substr(0,4)+"-"+fecha.substr(4,2)+"-"+fecha.substr(6,2);
	}
	return fecha;
}

function createChartEvolPlotSalud(panelId,unidades,series, legend_tooltips, notDate){
	createChartEvolPlotSeries(panelId,unidades,series, legend_tooltips, notDate,null,CHART_COLORS);
}
function createChartEvolPlotSeries(panelId,unidades,series, legend_tooltips, notDate, min0,colors){
	var values = [];

var styles=[];
	for (var i=0; i<series.length; i++){
		if (series[i].length>0){
			values.push(series[i]);
			styles.push({
				color: colors[i],
				label:legend_tooltips[i],
				lineWidth: 4
			});
		}
	}
	plotEvol = $.jqplot(panelId.replace("/","\\/"), values, {
		// The "seriesDefaults" option is an options object that will
		// be applied to all series in the chart.
		seriesDefaults:{
			showLine:true,
			markerOptions: { style:"filledCircle", size:6}
		},
		series:styles,
		//     series:labels,
		highlighter: {
			show: true,
			// tooltipAxes :'y',
			yvalues:2,
			showMarker:false,
			tooltipContentEditor: function (str, seriesIndex, pointIndex) {
				var vals = str.split(",");
				if (vals.length>1){
					return plotEvol.series[seriesIndex].label+":" +vals[1] ;
				}
				else{
					return plotEvol.series[seriesIndex].label+":" +str;
				}
			}
		},


		axes: {
			// Use a category axis on the x axis and use our custom ticks.
			xaxis:{
				 renderer: $.jqplot.CategoryAxisRenderer,
	                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
	                tickRenderer: $.jqplot.CanvasAxisTickRenderer,
	                tickOptions: {
	                    angle: 90, fontSize:'8pt'
	                }
		          },
			// Pad the y axis just a little so bars can get close to, but
			// not touch, the grid boundaries.  1.2 is the default padding.
			yaxis: {
				labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
				label: unidades,
               min:(min0? 0:null)

			}
		},
		cursor:{
			show: true,
			zoom: true
		} ,
		legend: {
			show: (legend_tooltips && legend_tooltips.length>1? true: false),
				placement: 'outside', 
				rendererOptions: {
					numberColumns: 1
				},
				location:'e'
     
		}

	});

}
function createChartEvolPlot(panelId,unidades,series, legend_tooltips){
	var values = [];
	var all_styles = [{
		color: 'rgb(234, 162, 40)',
		label:(legend_tooltips?legend_tooltips[0]:""),
		lineWidth: 4
	},{
		linePattern: 'dashed',
		color: 'rgb(0, 255, 0)',
		label:(legend_tooltips?legend_tooltips[1]:""),
		lineWidth: 2
	},{
		linePattern: 'dashed',
		color: 'rgb(255, 0, 0)',
		label:(legend_tooltips?legend_tooltips[2]:""),
		lineWidth: 2
	}];
var styles=[];
	for (var i=0; i<series.length; i++){
		if (series[i].length>0){
			values.push(series[i]);
			styles.push(all_styles[i]);
		}
	}
	plotEvol = $.jqplot(panelId, values, {
		// The "seriesDefaults" option is an options object that will
		// be applied to all series in the chart.
		seriesDefaults:{
			showLine:true,
			markerOptions: { style:"filledCircle", size:6}
		},
		series:styles,
		//     series:labels,
		highlighter: {
			show: true,
			// tooltipAxes :'y',
			yvalues:2,
			showMarker:false,
			tooltipContentEditor: function (str, seriesIndex, pointIndex) {
				var vals = str.split(",");
				if (vals.length>1){
					return vals[1] ;
				}
				else{
					return str;
				}
			}
		},


		axes: {
			// Use a category axis on the x axis and use our custom ticks.
			xaxis: {
				renderer:$.jqplot.DateAxisRenderer,
				tickOptions:{formatString:'%Y',angle:90, fontSize:'8pt'}
			},
			// Pad the y axis just a little so bars can get close to, but
			// not touch, the grid boundaries.  1.2 is the default padding.
			yaxis: {
				labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
				label: unidades/*,
               min: 0*/

			}
		},
		cursor:{
			show: true,
			zoom: true
		} ,
		legend: {
			show: (legend_tooltips ? true: false)
		}

	});

}


function getPrintCharts() {
	var chartActive = $("#data_accordion").accordion("option","active");
	var xml="<charts>";
	var tituloIdx=0;
	$(".chartSITA").each(function(idx){
		$("#data_accordion").accordion("option","active",idx);
		if (idx>0 && ($(this).parent()[0]==$(".chartSITA")[idx-1].parentNode)){// hay que meter el grafico sin etiqueta
			xml+="<grafico><laImagen>"+$(this).jqplotToImageStr({})+"</laImagen></grafico>";
		}
		else{
			xml+="<grafico><laImagen>"+$(this).jqplotToImageStr({})+"</laImagen><laEtiqueta>"+$("#data_accordion h3")[tituloIdx ].innerText+"</laEtiqueta></grafico>";
			tituloIdx++;
		}
	});
	xml+="</charts>";
	//$("#data_accordion").accordion("option","active",chartActive);
	return xml;
}
