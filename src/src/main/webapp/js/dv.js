var bloques = new Array();
var capasTOC = new Array();
//var toc = new TOC('toc_table');
var resultadosSpSS;
var resultadosGeoJSON;
var resultadosSITA;
var portal;
var listDialog;
var distancias = [1,5,10];
var tokenId = null;
var app = "dondeVivo"
	var results;
var results2;
var d = 1000;
var mun;
var tipoActual;
var munActual;
var zbsActual;

var isSmall = screen.width<=500;

function checkParams(){
	var cmdString = document.location.toString();

	var typename =  getInitialParam(cmdString, "TYPENAME=");
	var objectid =  getInitialParam(cmdString, "OBJECTID=");
	var muni =  getInitialParam(cmdString, "MUNI=");
	portal =   "foo_"+getInitialParam(cmdString, "NUMPORTAL=");
	if (typename && objectid && muni){
		$("#page0").css("display","none");
		$("#page1").css("display","block");

		initMap();
		init();
		initTOC(app);
		initSearch();
		initBaseLayers()

		//var texto = $("#texto0").val();
		//buscar("50002");

		showCortes(typename,objectid,muni);
		setDialogMaxHeight();
	}
}

function init(){

}

function refreshTOCAccordion(){

}


function refreshLegendAccordion(){

}
function inputKeyUp0(e) {
	e.which = e.which || e.keyCode;
	if(e.which == 13) {
		buscar0();
	}
}
/** invocado al buscar desde página de inicio */
function buscar0(){

	$("#page0").css("display","none");
	$("#page1").css("display","block");

	initMap();
	init();
	initTOC(app);
	initSearch();
	initBaseLayers()
	var texto = $("#texto0").val();
	buscar(texto);
	setDialogMaxHeight();

}
/*
 * Invocado cuando el usuario le da a buscar
 */
function buscar(texto){
	
	var tipo = "BusCallUrb";
	try{
		var cp = parseInt(texto);
		if (!isNaN(cp)){
			buscarLugar0(texto,["v111_codigo_postal"]);
			return;
		}
	}
	catch(err){
		// no es código postal
	}
	if (texto.match(".*[0-9]+.*")){
		buscarLugar0(texto,["TroidesV"]);
	}
	else {
		if (texto.indexOf(",")>0){
			buscarLugar0(texto,["BusCallUrb"]);
		}
		else{
			if (APP_NAME=="DVS"){
				buscarLugar0(texto,["v206_zonas_salud","v206_area_salud","instalaciones_sanitarias","BusCallUrb","Localidad" ]);
			}
			else{
				buscarLugar0(texto,["BusCallUrb","Localidad"]);
			}
		}
	}
}

/**
 * realiza la búsqueda con TypedSearchService
 * @param texto
 * @param tipo
 * @param tipo2
 * @param data1
 */
function buscarLugar0(texto,tipos,datos, idx){

	var partes = texto.toLowerCase().split(",");
	if (!idx){
		idx=0;
	}
	var tipo = tipos[ idx ];
	showLoading(true);
	//'/SimpleSearchService/services?texto=' + literal + "&inicio=" + inicio + "&total=" + page_size + maxParam + "&app=" + APP_NAME;
	var url = '/SimpleSearchService/typedSearchService?texto='+encodeURI(partes[0])+(partes.length>1?"&muni="+encodeURI(partes[1]):"")+"&type="+tipo+"&app="+APP_NAME;

	$.ajax({
		url: url,
		type: 'GET',
		//async: false,

		success:  function(data) {
			if (datos){
				datos.push(data);
			}
			else{
				datos=[data];
			}
			if (tipos.length>idx+1){

				buscarLugar0(texto,tipos,datos, (idx ? idx+1 :1));
			}
			else{
				muestraLugar0(tipos,datos);
			}
		},
		error:  function() { alert("error "+url);
		showLoading(false);
		},
		complete: function(xmlHttpRequest) {
		},
		processData: false,
		contentType: "text/xml; charset=\"utf-8\""
	});

}
function getPropertiesGasolineras(estacion){
	return "{\"rotulo\":\""+estacion.getElementsByTagName("rotulo")[0].textContent+
	"\",\"operador\":\""+estacion.getElementsByTagName("operador")[0].textContent+
	"\",\"direccion\":\""+estacion.getElementsByTagName("direccion")[0].textContent+"\"}";
}


function getResultsGasolineras(x,y,radio){
	var distancia = 1000;

	//console.log(x-radio-distancia, y-radio-distancia)
	//console.log(x+radio+distancia, y+radio+distancia)
	var latLon0 = coordTransformProj4s("EPSG:25830", "EPSG:4258", x-radio-distancia, y-radio-distancia);
	var latLon1 = coordTransformProj4s("EPSG:25830", "EPSG:4258", x+radio+distancia, y+radio+distancia);

	$.ajax({
		url: 'consultaGasolineras.jsp?x0='+latLon0.ord[0]+"&y0="+latLon0.ord[1]+"&x1="+latLon1.ord[0]+"&y1="+latLon1.ord[1],
		type: 'GET',

		success:  function(data) {

			var resultados='[{"capa":"gasolineras","distancia":'+distancia+',"featureCollection":{"type":"FeatureCollection","features":['
			var xml = new DOMParser().parseFromString(data, "text/xml");
			var estaciones = xml.getElementsByTagName("estacion");
			for (var i=0; i<estaciones.length; i++){
				if (i>0){
					resultados +=","
				}


				var coordenadaX_dec = estaciones[i].getElementsByTagName("coordenadaX_dec")[0].textContent;
				var coordenadaY_dec = estaciones[i].getElementsByTagName("coordenadaY_dec")[0].textContent;
				var coords = coordTransformProj4s("EPSG:4258", "EPSG:25830",coordenadaX_dec,coordenadaY_dec);
				resultados +='{"type": "Feature", "geometry": {"type": "Point", "coordinates": ['+coords.ord[0]+','+ coords.ord[1]+'] }, "properties":  '+getPropertiesGasolineras(estaciones[i])+' }';
			}
			resultados+="]}}";


			distancia=5000;
			latLon0 = coordTransformProj4s("EPSG:25830", "EPSG:4258", x-radio-distancia, y-radio-distancia);
			latLon1 = coordTransformProj4s("EPSG:25830", "EPSG:4258", x+radio+distancia, y+radio+distancia);
			$.ajax({
				url: 'consultaGasolineras.jsp?x0='+latLon0.ord[0]+"&y0="+latLon0.ord[1]+"&x1="+latLon1.ord[0]+"&y1="+latLon1.ord[1],
				type: 'GET',

				success:  function(data) {
					resultados+=',{"capa":"gasolineras","distancia":'+distancia+',"featureCollection":{"type":"FeatureCollection","features":['
					var xml = new DOMParser().parseFromString(data, "text/xml");
					var estaciones = xml.getElementsByTagName("estacion");
					for (var j=0; j<estaciones.length; j++){
						if (j>0){
							resultados +=","
						}

						var coordenadaX_dec = estaciones[j].getElementsByTagName("coordenadaX_dec")[0].textContent;
						var coordenadaY_dec = estaciones[j].getElementsByTagName("coordenadaY_dec")[0].textContent;
						var coords = coordTransformProj4s("EPSG:4258", "EPSG:25830",coordenadaX_dec,coordenadaY_dec);
						resultados +='{"type": "Feature", "geometry": {"type": "Point", "coordinates": ['+coords.ord[0]+','+ coords.ord[1]+'] }, "properties":  '+getPropertiesGasolineras(estaciones[i])+' }';
					}
					resultados+="]}}";



					distancia=10000;
					latLon0 = coordTransformProj4s("EPSG:25830", "EPSG:4258", x-radio-distancia, y-radio-distancia);
					latLon1 = coordTransformProj4s("EPSG:25830", "EPSG:4258", x+radio+distancia, y+radio+distancia);
					$.ajax({
						url: 'consultaGasolineras.jsp?x0='+latLon0.ord[0]+"&y0="+latLon0.ord[1]+"&x1="+latLon1.ord[0]+"&y1="+latLon1.ord[1],
						type: 'GET',

						success:  function(data) {
							resultados+=',{"capa":"gasolineras","distancia":'+distancia+',"featureCollection":{"type":"FeatureCollection","features":['
							var xml = new DOMParser().parseFromString(data, "text/xml");
							var estaciones = xml.getElementsByTagName("estacion");
							for (var k=0; k<estaciones.length; k++){
								if (k>0){
									resultados +=","
								}
								var coordenadaX_dec= estaciones[k].getElementsByTagName("coordenadaX_dec")[0].textContent;
								var coordenadaY_dec = estaciones[k].getElementsByTagName("coordenadaY_dec")[0].textContent;
								var coords = coordTransformProj4s("EPSG:4258", "EPSG:25830",coordenadaX_dec,coordenadaY_dec);
								resultados +='{"type": "Feature", "geometry": {"type": "Point", "coordinates": ['+coords.ord[0]+','+ coords.ord[1]+'] }, "properties":  '+getPropertiesGasolineras(estaciones[i])+' }';
							}
							resultados+="]}}]";

							resultadosGeoJSON=JSON.parse(resultados);

							showResultGeoJSON();
						},

						error: function(jqXHR, textStatus, errorThrown) {
							//loaded++;
						}


					});
				},

				error: function(jqXHR, textStatus, errorThrown) {
					//loaded++;
				}


			});
		},
		error: function(jqXHR, textStatus, errorThrown) {
			//loaded++;
		}
	});
}




/**
 * parsea los resultados de TypedSearchService
 * @param datos
 * @returns {Array}
 */
function getSearchResults0(datos){

	var json = $.xmlToJSON(datos);
	if((json == null)||(typeof json.Body[0].SearchResponse[0].SearchResult=='undefined')){
		return;
	}
	var results = json.Body[0].SearchResponse[0].SearchResult;
	var respuesta = new Array();
	for ( var j = 0; j < results.length; j++) {
		var busquedaLine = results[j];
		var resultados ="";
		if ((busquedaLine.Count[0].Text!="0")&&(busquedaLine.Count[0].Text!="-1")){

			if(typeof busquedaLine.List != 'undefined'){
				portal = busquedaLine.Type[0].Text;
				resultados = busquedaLine.List[0].Text;
				// resultados de toponimos o de bbdd (no coordenadas)
				var lineas=resultados.split("\n");
				respuesta=respuesta.concat(lineas);
			}
		}
	}
	return respuesta;
}

/**
 * Muestra en el mapa el lugar buscado por el usuario
 * @param tipo
 * @param datos
 * @param datos2
 */
function muestraLugar0(tipos ,datos){
	var results=[];
	for (var i=0; i<tipos.length; i++){
		results.push(getSearchResults0(datos[i]));
	}


	showSearchResults0(tipos,results);
}

function calle(){
	$("#dialog").hide();

	results=results2;
	tipo="BusCallUrb";
	showSearchResults0(tipo,results)
}

function localidad(){
	$("#dialog").hide();
	tipo="Localidad";
	showSearchResults0(tipo,results)
}

function showSearchResults0(tipos,results){
	var num_results=0;
	var idx_result=-1;
	for (var i=0; i<tipos.length; i++){
		num_results=num_results + results[i].length;
		if (results[i].length>0){
			idx_result=i;
		}
	}
	if (num_results==1){

		showResults(tipos[idx_result],results[idx_result][0]);

	}
	else if (num_results>0){

		if ($('#searchDialog').css("display")=="none"){
			openSearchDialog()
		}
		$("#searchResults").show();
		for (var i=0; i<tipos.length; i++){
			if (results[i].length>0){
				printResults(tipos[i], results[i]);
			}
		}

		showLoading(false)
	}
	else{
		alert("No se encontraron resultados para la búsqueda realizada");
		showLoading(false)
	}
}


function printResults(tipo, results){

	for (var i=0; i<results.length;i++){
		var valores = results[i].split("#");
		var onclickFn ='javascript:showCortes(\'' + tipo + '\',' + valores[3] +  ',' + valores[4] +')';
		var icono = '';
		var tooltip = valores[0]+(valores[1].length>0 ? " ("+valores[1]+')':"");
		var _tipo = tipo.toLowerCase();
		if (tipo.indexOf("Coordenadas") != -1){
			_tipo="coordenadas";
		}
		if (tipos[tipo] != null) {
			tooltip += " [" + tipos[tipo] + "]";
		}

		//Podria contener " y dar problemas
		tooltip = tooltip.replace(/"/g, '');

		if (_tipo in iconosBusquedaTipos) {
			if (_tipo == "toponimo") {
				onclickFn ='javascript:showToponimo(\''+valores[2]+'\')';
			}
			else if (_tipo=="coordenadas"){
				onclickFn ='javascript:showToponimo(\''+valores[1]+'\')';
			}
			icono = iconosBusquedaTipos[_tipo];
		} else if (_tipo.indexOf("troidesv_")==0){
			icono = iconosBusquedaTipos["troidesv"];
		} else{
			console.log('Tipo no previsto: ' + _tipo);
			icono = defaultIconBusquedaTipos;
		}

		$("#resultsBusqueda").append('<li class="itemResult"><a href="#" onclick="'+onclickFn+'" title="' + tooltip + '">' + icono +valores[0]+(valores[1].length>0 ? " ("+valores[1]+')':"")+'</a></li>');
		//$("#"+tipo+(conMuni ?"Muni":"")).append('<li class="itemResult"><a href="#" onclick="'+onclickFn+'">' + icono +valores[0]+(valores[1].length>0 ? " ("+valores[1]+')':"")+'</a></li>');

	}
	//$("#"+tipo+(conMuni ?"Muni":"")).listview().listview('refresh');
	$("#resultsBusqueda").listview().listview('refresh');
	try {
		clearInterval(endSearchInterval);
	} catch(err){
	}
}


function showCortes(tipo,objectid, muni){

	showLoading(true);
	if (layerList){
		$("#layersContainer input").val("");
		layerList.search($("#layersContainer input").val());
	}
	tipoActual = tipo;
	munActual = muni;

	objectid = showSearchResult(tipo,objectid);
	if (objectid<0){
		showLoading(false);
		return;
	}
	mun = muni;
	zbsActual=null;
	if (resultadosSalud.length>0){
		for (var t in resultadosSalud[0]){

			var layerGroup = getOLLayerByTabla(t);

			if(layerGroup[0]){
				var groupIndx = layerGroup[0].get('idx').split('_')[0];
				document.getElementById("count_"+groupIndx).innerHTML=" (0)";
			}
		}
	}
	resultadosSalud=[];
	resultadosSaludZBS=[]
	if(tipoActual=='v206_zonas_salud'){
		zbsActual=objectid;
		consultaIndicadoresSalud(zbsActual);
	}
	$.ajax({
		url: '/SpatialSearchService/services?SERVICE='+APP_NAME+'&TYPENAME='+tablas[tipoActual]+'&CQL_FILTER=OBJECTID='+objectid+'&PROPERTYNAME=OBJECTID&TYPENAME_CONN='+(tipoActual=='instalaciones_sanitarias'?'GEOEXT':(tipoActual.indexOf('v206')==0 ?'DVS':'DV')),
		type: 'GET',

		success:  function(data) {
			resultadosSpSS=data.resultados;
			showResultLayers();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("No se pudo obtener la información de lugares de interés cercanos:"+jqXHR.responseText);

		},
		complete: function() {

			showLoading(false)
		}
	});
	if (APP_NAME!='DVS'){
		$.ajax({
			url: 'consultaIndicadores.jsp?C_MUNI_INE='+muni,
			type: 'GET',

			success:  function(data) {

				var cql_filter ='codmun='+muni;
				//$("#toc_table tr").removeClass("sita_enabled");
				var json = JSON.parse(data);
				resultadosSITA = json.indicadores;
				showResultSITA(cql_filter);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$("#SOCIEDADLoading").css("visibility","hidden");
				$("#ECONOMIALoading").css("visibility","hidden");
				alert("No se pudieron obtener los indicadores estadísticos del municipio:"+jqXHR.responseText);

			}
		});

		showSIUa('cod_ine='+muni);
	}
}


function showSIUa(cql_filter){



	var layers = toc_group.getLayers();

	for (var i = 0; i < layers.getLength(); i++) {
		var layer = layers.item(i);
		if ((layer.values_.url == "/SIUa_WMS") && ! (layer instanceof ol.layer.Vector)) {
			//return layer;
			layer.getSource().getParams().CQL_FILTER= cql_filter;

			var groupIndx = layer.get('idx').split('_')[0];

			$("#count_"+groupIndx).html(" (1)");
			$('#layer_'+groupIndx).attr('style','color:black');
			$('#chk_layer'+groupIndx).attr('style','visibility:visible');
		}

	}
}

/**
 * Obtienen los datos del lugar indicado por el usuario con SpatialSearchService y consultaIndicadoresSITA
 * @param tipo
 * @param resultado
 */
function showResults(tipo,resultado){

	try{
		listDialog.dialog("close");

	}
	catch(err){}
	var valores =resultado.split("#");

	var muni = valores[4];
	var objectid = valores[3];

	showCortes(tipo,objectid,muni);

}

function refreshResults(distancia){
	//$(".count").html("(0)");
	//$("#toc_table tr").removeClass("layer_enabled");
	refreshResultLayers(distancia);
	refreshResultGeoJSON(distancia);
}

function refreshResultGeoJSON(distancia){

if (resultadosGeoJSON){
	//Inicializar a 0
	for (var i=0; i<resultadosGeoJSON.length; i++){
		if (resultadosGeoJSON[i].distancia==distancia){
			var layerGroup = getOLLayerByTabla(resultadosGeoJSON[i].capa.toLowerCase());
			if(layerGroup[0]){
				var groupIndx = layerGroup[0].get('idx').split('_')[0];
				document.getElementById("count_"+groupIndx).innerHTML=" (0)";
				$('#layer_'+groupIndx).attr('style','color:grey');
				$('#chk_layer'+groupIndx).attr('style','visibility:hidden');
			}
		}
	}

	for (var i=0; i<resultadosGeoJSON.length; i++){
		if (resultadosGeoJSON[i].distancia==distancia){
			//if (resultadosSpSS[i].featureCollection.totalFeatures>0){

			var layerGroup = getOLLayerByTabla(resultadosGeoJSON[i].capa.toLowerCase());
			if(layerGroup[0]){
				var groupIndx = layerGroup[0].get('idx').split('_')[0];
			}


			for(var n in layerGroup){
				var layer = layerGroup[n];

				if (layer instanceof ol.layer.Vector){

					layer.values_.source.clear();
					if (resultadosGeoJSON[i].featureCollection.features.length>0){

						var count = document.getElementById("count_"+groupIndx).innerHTML.replace("(","").replace(")","");

						try{
							count= parseInt(count);
							if(isNaN(count)){
								count = 0;
							}
						}
						catch (err){
							console.log(err);
							count=0;
						}
						if((count  + resultadosGeoJSON[i].featureCollection.features.length) != 0){
							$('#layer_'+groupIndx).attr('style','color:black');
							$('#chk_layer'+groupIndx).attr('style','visibility:visible');
						}
						document.getElementById("count_"+groupIndx).innerHTML=" ("+(count + resultadosGeoJSON[i].featureCollection.features.length)+")";

						var format = new ol.format.GeoJSON();
						var features = format.readFeatures(resultadosGeoJSON[i].featureCollection);
						for (var j=0; j<features.length; j++){
							layer.values_.source.addFeature(features[j]);
						}

					}
				}

			}
		}
	}
}
}
/**
 * Muestra los resultados de SpatialSearchService
 * @param distancia
 */
function refreshResultLayers(distancia){


	//Inicializar a 0
	for (var i=0; i<resultadosSpSS.length; i++){
		if ( resultadosSpSS[i].capa=='saludplan.v206_zonas_salud'){
		if ((resultadosSalud.length==0) && (zbsActual == null)){
			
				if (resultadosSpSS[i].featureCollection.totalFeatures>0){

					var features = resultadosSpSS[i].featureCollection.features;
					for (var j=0; j<features.length; j++){
						consultaIndicadoresSalud(features[j].properties.objectid);
					}
				}
			}
		}
		else{
			if (resultadosSpSS[i].distancia==distancia){
				var layerGroup = getOLLayerByTabla(resultadosSpSS[i].capa.toLowerCase());
				if(layerGroup[0]){
					var groupIndx = layerGroup[0].get('idx').split('_')[0];
					//console.log( document.getElementById("count_"+groupIndx))
					document.getElementById("count_"+groupIndx).innerHTML=" (0)";
					$('#layer_'+groupIndx).attr('style','color:grey');
					$('#chk_layer'+groupIndx).attr('style','visibility:hidden');
				}
			}
		}
	}

	for (var i=0; i<resultadosSpSS.length; i++){
		if (resultadosSpSS[i].distancia==distancia){
			var layerGroup = getOLLayerByTabla(resultadosSpSS[i].capa.toLowerCase());
			if(layerGroup[0]){
				var groupIndx = layerGroup[0].get('idx').split('_')[0];
			}



			for(var n in layerGroup){
				var layer = layerGroup[n];
				var cql_filter="";
				if (resultadosSpSS[i].featureCollection.totalFeatures>0){

					var features = resultadosSpSS[i].featureCollection.features;
					for (var j=0; j<features.length; j++){
						if (j>0){
							cql_filter+=" OR ";
						}
						cql_filter+="objectid="+features[j].properties.objectid;
					}
				}
				else{
					cql_filter="0=1";
				}

				if (layer instanceof ol.layer.Vector){

					var geoJSON = new ol.format.GeoJSON({defaultDataProjection: map_projection, featureProjection:map_projection});
					var projection = new ol.proj.Projection({code:"EPSG:25830"});
					var vectorSource = new ol.source.Vector({
						format: new ol.format.GeoJSON({dataProjection: projection, featureProjection:projection}) ,

						url:  layer.get("url")+'?service=WFS&' +
						'version=1.0.0&request=GetFeature&typename='+layer.get("capa") +'&CQL_FILTER='+cql_filter+
						'&outputFormat=application/json&srsname=EPSG:25830',
						// cargamos por post por si la URL resulta demasiado larga
						loader: function(extent, resolution, projection) {


							var xhr = new XMLHttpRequest();
							var source = this;
							var urlArray = this.getUrl().split("?");
							var url = urlArray[0];
							var params = urlArray[1];

							var typename = params.split("&")[3].split("=")[1];
							//console.log(typename)
							xhr.open('POST', url, true);
							xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

							var onError = function() {
								console.log("No se ha podido obtener las features de "+layer.get("capa") +'&CQL_FILTER='+cql_filter)
							}
							xhr.onerror = onError;
							xhr.onload = function() {
								if (xhr.status == 200) {
									source.on('addfeature',function(ev){
										ev.feature.set("capa", typename);
										ev.feature.set("atributos",fieldAliasList2[typename]);
									});
									layer.features = source.getFormat().readFeatures(xhr.responseText);
									source.addFeatures(source.getFormat().readFeatures(xhr.responseText));

								} else {
									onError();
								}
							}
							xhr.send(params);
						}
					});

					layer.setSource(vectorSource);
				}
				else{
					var count = document.getElementById("count_"+groupIndx).innerHTML.replace("(","").replace(")","");
					try{
						count= parseInt(count);
						if(isNaN(count)){
							count = 0;
						}
					}
					catch (err){
						console.log(err);
						count=0;
					}
					if((count  + resultadosSpSS[i].featureCollection.totalFeatures) != 0){
						$('#layer_'+groupIndx).attr('style','color:black');
						$('#chk_layer'+groupIndx).attr('style','visibility:visible');
					}

					document.getElementById("count_"+groupIndx).innerHTML=" ("+(count + resultadosSpSS[i].featureCollection.totalFeatures)+")";

					layer.getSource().getParams().CQL_FILTER= cql_filter;
					layer.getSource().changed()
				}
			}

		}
	}
	checkVisibility();
}
function getLastSITAValue(valores){
	if (valores.length==1){
		return valores[0];
	}
	var lastTempo=0;
	var lastValue;
	for (var i=0; i<valores.length; i++){
		if (lastTempo<=valores[i][0]){
			lastValue=valores[i];
			lastTempo = valores[i][0];
		}

	}
	
	return lastValue;
}
function showResultSITA( cql_filter_muni){
	for (var t in resultadosSITA){

		var layerGroup = getOLLayerByTabla(t);

		if(layerGroup[0]){
			var groupIndx = layerGroup[0].get('idx').split('_')[0];
			document.getElementById("count_"+groupIndx).innerHTML=" (0)";
		}
		for(var i in layerGroup){

			var layer = layerGroup[i];

			var cql_filter=cql_filter_muni;
			var lastValue=[];
			var anno=null;
			if (resultadosSITA[t].valores.length==0){
				cql_filter="1=0";
				$("#count_"+groupIndx).html(" (0)");
			}
			else{
				
				
				$("#count_"+groupIndx).html(" (1)");
				$('#layer_'+groupIndx).attr('style','color:black');
				$('#chk_layer'+groupIndx).attr('style','visibility:visible');
				lastValue = getLastSITAValue(resultadosSITA[t].valores);
				
				cql_filter+=" and tempo="+lastValue[0];
				
				
			}

			if (layer instanceof ol.layer.Vector){
				if (lastValue.length>0){
					var projection = new ol.proj.Projection({code:"EPSG:25830"});
					var vectorSource = new ol.source.Vector({
						format: new ol.format.GeoJSON({defaultDataProjection: projection, featureProjection:projection}) ,
						url:  layer.get("url")+'?service=WFS&' +
						'version=1.0.0&request=GetFeature&typename='+layer.get("capa") +'&CQL_FILTER='+cql_filter+
						'&outputFormat=application/json&srsname=EPSG:25830'
					});
					layer.setSource(vectorSource);

					layer.set("lastDate",lastValue[0]);
					layer.set("lastValue",lastValue[1]);
					if ($('#layer_'+groupIndx+" .plotBtn").length<=0){
$("<a  class='plotBtn ui-btn ui-icon-info ui-btn-icon-notext ui-corner-all' onclick='openOnly("+groupIndx+")''></a>").insertAfter('#count_'+groupIndx);
					}
				}
			}
			else{
				layer.getSource().getParams().CQL_FILTER= cql_filter;
			}

			//layer.set("lastDate",lastValue[0]);
			//layer.set("lastValue",lastValue[1]);
		}
	}


}


function showResultLayers(){
	var distancia = getSelectedDistance();
	refreshResultLayers(distancia);
}

function showResultGeoJSON(){
	var distancia = getSelectedDistance();
	refreshResultGeoJSON(distancia);
}