var DOTS_PER_M=2834.3472;
var WMS_SRS="EPSG:25830";
WMS_BBOX_X_MIN=-1034440;
WMS_BBOX_Y_MIN=3560031;
WMS_BBOX_X_MAX=2259829;
WMS_BBOX_Y_MAX=5822607;
//[493707.6026845637, 4413376, 886332.3973154363, 4755088]
var bbox_aragon = [571580, 4400803, 812351,
                   4840039];
//[434305.34375, 4490332.98828125, 949625.65625, 4843082.01171875]

OVERVIEW_BOUND_X_MIN = 530000;
OVERVIEW_BOUND_X_MAX= 855000;
OVERVIEW_BOUND_Y_MIN = 4410000;
OVERVIEW_BOUND_Y_MAX= 4760000;

//https://idearagon.aragon.es/arcgis/services/AragonReferencia/Basico_NEW/MapServer/WMSServer?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fjpeg&TRANSPARENT=true&LAYERS=0&SRS=EPSG%3A25830&STYLES=&WIDTH=1575&HEIGHT=1626&BBOX=675581.901191579%2C4610225.533341243%2C679976.0985602008%2C4614762.019005611
//https://idearagon.aragon.es/arcgis/services/AragonReferencia/Basico_NEW/MapServer/WMSServer?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS=0%2C2%2C4%2C5%2C6%2C8%2C9%2C10%2C11%2C12%2C13%2C14%2C15%2C16%2C17%2C18%2C19
//var SATELITE_WMS_URL=["https://idearagon.aragon.es/arcgis/services/AragonReferencia/Basico_NEW/MapServer/WMSServer"];
//var SATELITE_WMS_LAYERS=["0","2","4","5","6","8","9","10","11","12","13","14","15","16","17","18","19"];
var SATELITE_WMS_URL=["https://idearagon.aragon.es/arcgis/services/AragonReferencia/Basico_NEW/MapServer/WMSServer"];
var SATELITE_WMS_LAYERS=["0,2,4,5,6,8,9,10,11,12,13,14,15,16,17,18,19"];

var popupX;
var popupY;

var popupIndex = 0;

/**
var SATELITE_WMS_URL=["https://wmspro-idearagon.aragon.es/erdas-iws/ogc/wms/AragonFoto"];
var SATELITE_WMS_LAYERS=["modis,spot,landsat,orto_reciente"];
*/
var urlWCTS = "https://idearagon.aragon.es/arcgis/services/AragonReferencia/mde/MapServer/WMSServer";

var graticule;

var map;
var baseLayer, wmtsBaseLayer,bottomBaseLayer;
//Proj4js.defs["EPSG:25830"] = "+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs";
proj4.defs("EPSG:25830","+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs");
proj4.defs("EPSG:25831","+proj=utm +zone=31 +ellps=GRS80 +units=m +no_defs");
proj4.defs('EPSG:4326', '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees');
ol.proj.proj4.register(proj4);

var zoomToPosition=true;
var tracking=false;
var lastPosition;
var map_projection;
var graticule_color= 'rgba(0,82,204,1)';
var overlay, selectControl;
var featurePopup;
var pointerOnPopup=false;
var attributionControl;
var selectInteraction;
var selectedLayer;

var theX = 0;
var theY = 0;

var vectorialFormats=["GML","GPX","GEOJSON","KML","KMZ","OSM","SHP"];

var interactiveLayers=new Array();
function initMap() {
	map_projection = ol.proj.get('EPSG:25830');
	map_projection.setExtent([WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX]);

	// estas 3 líneas son para la cuadrícula
	map_projection.setWorldExtent([-10, 36, 5, 45]);
	map_projection.setGlobal(false);
	map_projection.setGetPointResolution(function (resolution) { return resolution; });

	var options = {
			projection : map_projection,
//			maxExtent : new OpenLayers.Bounds(WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX),
			center : [675533, 4589000],
			zoom:12

	};
	baseLayer =new ol.layer.Image({
		source: new ol.source.ImageWMS({
			params: {'LAYERS': SATELITE_WMS_LAYERS[0],'VERSION':'1.1.1','FORMAT':'image/jpeg'},
			url: SATELITE_WMS_URL[0],
			projection: map_projection
		})
	});
	wmtsBaseLayer=new ol.layer.Tile();
	wmtsBaseLayer.setVisible(false);

	bottomBaseLayer=new ol.layer.Image();
	bottomBaseLayer.setVisible(false);
	//attribution = getFondoLabel()+base_attribution;

	var overviewLayer = new ol.layer.Image(
			{
				source: new ol.source.ImageStatic({url:'/lib/IDEAragon/themes/default/images/mapaOverview.gif',
					imageExtent:[OVERVIEW_BOUND_X_MIN, OVERVIEW_BOUND_Y_MIN, OVERVIEW_BOUND_X_MAX,
					             OVERVIEW_BOUND_Y_MAX],
					             imageSize:[130,140]})
			}
	);
	var controlOptions = {
			projection : map_projection,
			center : [675533, 4589000],
			extent: [675532, 4588999,675534, 4589001],
			maxResolution: (OVERVIEW_BOUND_X_MAX-OVERVIEW_BOUND_X_MIN)/130,
			minResolution:(OVERVIEW_BOUND_X_MAX-OVERVIEW_BOUND_X_MIN)/130/*,
	    			maxZoom:0,
	    			minZoom:0*/

	};
	attributionControl = new ol.control.Attribution({
		tipLabel:"Información capas visibles"	
	});
	map = new ol.Map({
		controls: [new ol.control.ScaleLine(),
		           new ol.control.Zoom(),
			          new ol.control.ZoomToExtent({
				        	
			        		 label:"",
			        			 tipLabel:"Ir a la extensión de Aragón",
			             extent: bbox_aragon
			           }),
		           attributionControl,
		           new ol.control.OverviewMap({
		        	   view:new ol.View(controlOptions),
		        	   layers: [overviewLayer],
		        	   tipLabel: 'Mapa de situación'
		           })],
		           target: $('#map')[0],
		           layers:[bottomBaseLayer,baseLayer,wmtsBaseLayer],
		           view: new ol.View(options)
	});
	map.getView().fit(bbox_aragon,{"size": map.getSize(),"constrainResolution":false});
	
	setTimeout(function(){ 
		//map.getView().setMinZoom(map.getView().getZoom());
		configureMap();
		
	},1000);
	includePositionStuff(map);
	includeNorthControl();
	$( "#mainPage" ).on( "pageshow", function (event) {
		map.updateSize();
	} );
	graticule = new ol.Graticule({
		// the style to use for the lines, optional.
		strokeStyle: new ol.style.Stroke({
			color: graticule_color,
			width: 2,
			lineDash: [0.5, 4]
		}),
		intervals:[90, 45, 30, 20, 10, 5, 2, 1, 0.5,0.25,1/12],
		showLabels: true,
		lonLabelPosition:0.1,
		// necesitamos el formateador de longitud porque quieren que salga O en lugar de W
		lonLabelFormatter: function (decimalValue) {
			var value=decimalValue;
			var direccion="E";
			if (decimalValue <0) {
				direccion = 'O';
				value = 0-value;
			}
			var deegrees = Math.floor(value);
			value = (value -deegrees)*60;
			var mins = Math.floor(value);
			var segs = Math.round((value -mins)*60);
			if (segs==60) {
				segs=0;
				mins = mins+1;
			}
			return deegrees+String.fromCharCode(176)+" "+(mins<10 ?"0":"")+mins+String.fromCharCode(39)+" "+(segs<10 ?"0":"")+segs+String.fromCharCode(34)+" "+direccion;
		}
	});
	$("#btnGraticule").append('<button type="button" title="Ver rejilla" class="ui-icon-graticule"></button>');
	var toolGraticule = new ol.control.Control({element: document.getElementById("btnGraticule")});
	map.addControl(toolGraticule);

	// los añadimos aquí en lugar de en el index.html para que no se apliquen los estilos de jquery mobile
	$("#btnPosition").append('<button onclick="javascript:collapsePosition()" type="button" title="Ver posición" class="ui-icon-location"></button>');
	$("#locationInfo").append('<select id="selectSRS" onchange="changePosSRS()">'+
					'<option selected value="coordsUTM30">UTM30 ETRS89</option>'+
					'<option value="coordsUTM31">UTM31 ETRS89</option>'+
					'<option value="lonLat">Lon/Lat WGS84</option>'+
				'</select>');
	$("#locationInfo").append('<span class="pos" id="coordsUTM30"></span>');
	$("#locationInfo").append('<span class="pos oculto" id="coordsUTM31"></span>');
	$("#locationInfo").append('<span class="pos oculto" id="lonLat"></span>');
	var toolPosition = new ol.control.Control({element: document.getElementById("btnPosition")});
	map.addControl(toolPosition);
	map.addControl(new ol.control.MousePosition({  
		projection: 'EPSG:25830',
		target: document.getElementById('coordsUTM30'),
		coordinateFormat:function(coord){
			return "<b>X: </b>"+coord[0].toFixed(3)+" <b>Y: </b>"+coord[1].toFixed(3);
		}
	})
	);
	map.addControl(new ol.control.MousePosition({  projection: 'EPSG:25831',target: document.getElementById('coordsUTM31'),
		coordinateFormat:function(coord){
			return "<b>X: </b>"+coord[0].toFixed(3)+" <b>Y: </b>"+coord[1].toFixed(3);
		}}));
	map.addControl(new ol.control.MousePosition({  projection: 'EPSG:4326',target: document.getElementById('lonLat'),
		coordinateFormat:function(coord){
			return "<b>Lon: </b>"+coord[0].toFixed(8)+" <b>Lat: </b>"+coord[1].toFixed(8);
		}}));
	
	$("#btnScale").append('<button onclick="javascript:collapseScale()" type="button" title="Cambiar escala"><span id="scaleCollapseIcon">»</span></button>');
	
	$("#scaleInfo").append('<div id="scaleBox"><label>1:</label><input id="scaleTextBox" onkeyup="inputKeyUpScale(event)"></input></div>');
	$("#scaleInfo").append('<div id="changeScale" class="ui-link ui-btn ui-icon-refresh ui-btn-icon-notext ui-shadow ui-corner-all" title="Cambiar escala" onclick="javascript:changeScale()"><div data-icon="refresh" data-iconpos="notext" data-role="button"></div></div>');

	var toolScale = new ol.control.Control({element: document.getElementById("btnScale")});
	map.addControl(toolScale);
	$("#scaleTextBox").val(Math.round(map.getView().getResolution()*DOTS_PER_M));
	
	add3DButton();

	overlay = new ol.Overlay({
		element: document.getElementById('infoTooltip'),
		positioning: 'bottom-left'
	});

	overlay.setMap(map);
	//overlay.getElement().style.display =  'none';
	selectControl =new ol.interaction.Select({
		filter: function (feature, layer) {
			return (feature.getGeometry().getType()!='Point');
		}});
	map.addInteraction(selectControl);

		map.addControl(new ol.control.MousePosition({coordinateFormat:  function(pos){

  		theX = pos[0];
  		theY = pos[1];

  		return "";
  	}
  	}));
	initPopup();
	initSelectInteraction();

	
}

function configureMap(){
	cmdString = document.location.toString();

	var markerx =  getInitialParam(cmdString, "MARKERX=");
	var markery =  getInitialParam(cmdString, "MARKERY=");
	if (markerx && markery){
		
		
		var feature = new ol.Feature({
			geometry: new ol.geom.Point([parseFloat(markerx),parseFloat(markery)])
		});
		feature.setStyle(new ol.style.Style({
			image:new ol.style.Icon({src:img_path+'/chincheta.png'}),
			fill: new ol.style.Fill({
				color: 'rgba(0,0,0,0)'
			}),
			stroke: new ol.style.Stroke({
				color:'rgba(0,162,232,1)',
				width: 3
			})
		}));
		var source = new ol.source.Vector({features:[feature]});
		var markerLayer = new ol.layer.Vector({
			source: source
		});
		//drawingLayer.setMap(map);
		map.addLayer(markerLayer);

		map.getView().fit(ol.extent.buffer( feature.getGeometry().getExtent(),bufferPoints), map.getSize());
	}
	var bbox =  getInitialParam(cmdString, "BOX=");
	if(bbox){
		var extent =bbox.split(":");
		for (var i=0; i<extent.length;i++){
			extent[i]=parseFloat(extent[i]);
		}
		map.getView().fit(extent,{"size": map.getSize(),"constrainResolution":false});
	}
	var activeLayer =  getInitialParam(cmdString, "ACTIVELAYER=");
	var query =  getInitialParam(cmdString, "QUERY=");
	if (activeLayer && query){
		initSearch();
		// hay que poner en minúsuculas los atributos de la condición
		var partes = query.split("=");
		var igual = query.indexOf("=");
		showFeatures(activeLayer, partes[0].toLowerCase()+query.substr(igual),null,(bbox ? true :false));
	}

	
	
}
function collapseMapButton(id,icon){
	if ($("#"+id).hasClass("oculto")){
		$("#"+id).removeClass("oculto");
		if (icon){
			$("#"+icon).text("«");
		}
	}
	else{
		$("#"+id).addClass("oculto");
		if (icon){
			$("#"+icon).text("»");
		}
	}
}
function collapsePosition(){
	collapseMapButton("locationInfo");
}
function collapseScale(){
	collapseMapButton("scaleInfo","scaleCollapseIcon");
}

function changeScale(){
	newValue = document.getElementById('scaleTextBox').value;
	newValue = newValue.replace(/\./ig,"");
	if (! isNaN(newValue)) {
		
		if (newValue >= 0) {
			map.getView().setResolution(newValue/DOTS_PER_M);
		}
	} 
}
function inputKeyUpScale(e) {
    e.which = e.which || e.keyCode;
    if(e.which == 13) {
    	changeScale();
    }
}
function changePosSRS(){
	$(".pos").addClass("oculto");
	$("#"+$("#selectSRS").val()).removeClass("oculto");
}
function onPopup(overPopup) {
	pointerOnPopup = overPopup;
}
function stopDrawControls() {
	var interactions = map.getInteractions();
	for (var i=0; i<interactions.getLength(); i++) {
		if(interactions.item(i) instanceof ol.interaction.Draw) {
			try {
				interactions.item(i).finishDrawing();
			} catch (err) {
			}
			interactions.item(i).setActive(false);
		}
	}
}
function showGraticule() {
	if (graticule.getMap()) {
		graticule.setMap(null);
	} else {
		graticule.setMap(map);
	}

}
//this function will be override if necessary (app version)
function includePositionStuff(map) {
}

function onSuccess(position) {
	creaVentanaAviso('Latitude: '          + position.coords.latitude          + '\n' +
			'Longitude: '         + position.coords.longitude         + '\n' +
			'Altitude: '          + position.coords.altitude          + '\n' +
			'Accuracy: '          + position.coords.accuracy          + '\n' +
			'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
			'Heading: '           + position.coords.heading           + '\n' +
			'Speed: '             + position.coords.speed             + '\n' +
			'Timestamp: '         + position.timestamp                + '\n');
}

//onError Callback receives a PositionError object

function onError(error) {
	creaVentanaAviso('code: '    + error.code    + '\n' +
			'message: ' + error.message + '\n');
}
function setWCTSAltitude(point) {
	$.ajax({
		url: urlWCTS+"?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&LAYERS=11&STYLES=&INFO_FORMAT=application/geojson&CRS=EPSG:25830&BBOX="+ point[0] + ',' + point[1] + ',' + (point[0]+1) + ',' + (point[1]+1)+'&WIDTH=1&HEIGHT=1&QUERY_LAYERS=11&X=1&Y=1',
		type: 'GET',

		success:	function (data) {
			var json = $.parseJSON(data);
			var features = json.features;
			var altitud5;
			for (var i=0; i<features.length;i++) {
				if (features[i].layerName=="11") {
					$("#cota").text(parseInt(features[i].properties.PixelValue));
					break;
				}
			}
		},
		timeout:10000,
		error: 		function (data, textStatus, errorThrown) {
			console.log('Error obteniendo la altitud. State: ' + data.readyState + ", Status:" + data.status);
		}
	});

}

function controlDoubleClickZoom(active) {
	//Find double click interaction
	var interactions = map.getInteractions();
	for (var i = 0; i < interactions.getLength(); i++) {
		var interaction = interactions.item(i);
		if (interaction instanceof ol.interaction.DoubleClickZoom) {
			interaction.setActive(active);
		}
	}
}

function includeNorthControl() {
	var icon = document.createElement('i');
	icon.className = "fa fa-compass";
	icon.id="northIcon";
	var element= document.createElement('span');
	element.appendChild(icon);

	map.addControl(new ol.control.Rotate({"autoHide":false,
		"label":element,
		tipLabel:"Flecha de norte"}));
	setTimeout(function(){$(".ol-rotate-reset").attr("title","Flecha de norte");},200);
}

function getStyle(color){
	var fillColor= 'rgba(0,0,255,';	 
	if (color=="red"){
		fillColor= 'rgba(255,0,0,';	
	}
	else if (color=="green"){
		fillColor= 'rgba(0,255,0,';	
	}
	else if  (color=="yellow"){
		fillColor= 'rgba(255,255,0,';
	}
	
	var fill = new ol.style.Fill({
		color: fillColor+"0.75)"
	});
	var stroke = new ol.style.Stroke({
		color:fillColor+"1)",
		width: 5
	});
	return new ol.style.Style({
			image: new ol.style.Circle({
				fill: fill,
				stroke: new ol.style.Stroke({
					color:fillColor+"1)",
					width: 1
				}),
				radius: 5
			}),

			stroke: stroke,
			fill: fill
		});
}
function addInteractiveLayer(url,layer,campos){
	var projection = new ol.proj.Projection({code:"EPSG:25830"});
	var vectorSource = new ol.source.Vector({
		format: new ol.format.GeoJSON({defaultDataProjection: projection, featureProjection:projection}) ,
		url: function(extent) {
			var serviceURL=url;
			
			return serviceURL+'?service=WFS&' +
			'version=1.0.0&request=GetFeature&typename='+layer +
			'&outputFormat=application/json&srsname=EPSG:25830&bbox=' + extent.join(',') + ',EPSG:25830'
			;
		},
		strategy: ol.loadingstrategy.bbox
	});
	vectorSource.on('addfeature',function(ev){
		ev.feature.set("layer", layer);
		ev.feature.set("atributos",campos);
	});
	var fillColor = 'rgba(255,255,255,0.01)';
	var fill = new ol.style.Fill({
		color: fillColor
	});
	var stroke = new ol.style.Stroke({
		color:fillColor,
		width: 5
	});
	var wfs_layer = new ol.layer.Vector({
		source: vectorSource,
		style: new ol.style.Style({
			image: new ol.style.Circle({
				fill: fill,
				stroke: stroke,
				radius: 5
			}),

			stroke: stroke,
			fill: fill
		}),

	});
	map.addLayer(wfs_layer);
	return wfs_layer;
}



function initSelectInteraction(){

	selectInteraction = new ol.interaction.Select({
    //hitTolerance: 50,
		condition: ol.events.condition.pointerMove,
		style: new ol.style.Style({
			fill: new ol.style.Fill({
				color: 'rgba(0,0,0,0)'
			}),
			stroke: new ol.style.Stroke({

				color:'rgba(0,153,255,1)',
				width: 2
			}),
			 image: new ol.style.Circle({
			       fill: new ol.style.Fill({
						color: 'rgba(0,153,255,0.5)'
					}),
					stroke: new ol.style.Stroke({
						color: 'rgba(0,153,255,1)',
						width: 2
					}),
			       radius: 5
			     })
		}),

		layers:function( layer) {
			if (fieldAliasList2[layer.get("capa")] && !(layer.get("capa")=="Localidad")){
				selectedLayer=layer;
				return true;
			}else{
				return false;
			}
		}
	});
	map.addInteraction(selectInteraction);
	var selected = selectInteraction.getFeatures();
	selected.on('add', onFeatureSelectFuncion);

	map.on("singleclick", function(e) {
			var numFeatures = 0;
      map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {

				//Si se puede, no mostrar localidad
	      if (layer && fieldAliasList2[layer.get("capa")] && !(numFeatures>0 && layer.get("capa")=="Localidad")){
             selectedLayer=layer;
             e.element = feature;
             onFeatureSelectFuncion(e)
             numFeatures++;
	      }

      },{hitTolerance: 5})
  });


 /* 	map.on("pointermove", function(e) {
        if(e.pixel[1] > window.screen.height/2){
          positioning = 'bottom-';
        }else{
          positioning = 'top-';
        }

        if(e.pixel[0] > window.screen.width/2){
          positioning += 'right';
        }else{
          positioning += 'left';
        }
         overlay.setPositioning(positioning)

  	    var close = true;
        map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
  	        close = false;
        },{hitTolerance: 5})
        if(close){
          closePopup()
        }else{
        }
    });*/



}
function onFeatureSelectFuncion(evt) {

	var feature = evt.element;


	var textoHtmlAMostrar = '<div style="max-width:400px;text-align:center;">'+
	'<div class="map-lens"  style="display: block;"><h3 style="margin: 5px">'+selectedLayer.get("caption")+"</h3>";

	var campos = fieldAliasList2[selectedLayer.get("capa")];

	if (campos){
		var camposVis = Object.keys(campos);
		for (var i=0; i<camposVis.length; i++){
			if (feature.get(camposVis[i])){
				if((selectedLayer.get("capa")=="fianzas")&&(camposVis[i]=="valores")){
					var data = eval(feature.get("valores"));
					var encontrado1=false;
					var encontrado2=false;
					for (var j=data.length-1;j>=0;j--){

							var mostrar=false;
							if (!encontrado1 && (data[j].tipo==1)){
								encontrado1=true;
								mostrar=true;
							}
							else if (!encontrado2 && (data[j].tipo==2)){
								encontrado2=true;
								mostrar=true;
							}
							if (mostrar){
							textoHtmlAMostrar +='<div data-ex-content=".label"><span><b>Alquiler '+(data[j].tipo==1?'vivienda':'local')+' ('+data[j].anyo+')'+
							':&nbsp;</b><span>Mín.:'+data[j].min+' &euro;; Media.:'+data[j].media+' &euro;; Máx.:'+data[j].max+' &euro;</span></div></strong>';
							}
							if (encontrado1 && encontrado2){
								break;
							}

					}
					textoHtmlAMostrar +='<div data-ex-content=".label"><span><b>'+campos[camposVis[i]]+':&nbsp;</b><span><a href="javascript:showFeatureChart(\''+
					selectedLayer.get("capa")+"','Euros','"+feature.get("objectid")+"','"+selectedLayer.get("caption")+" "+feature.get("via_loc")+"','"+feature.get("valores").replace(/"/g,"\\'")+'\')" >Ver</a></span></div></strong>';

				}

				else{
				textoHtmlAMostrar +='<div data-ex-content=".label"><span><b>'+campos[camposVis[i]]+':&nbsp;</b><span>'+feature.get(camposVis[i])+'</span></div></strong>';
				}
			}
		}
		if(selectedLayer.get("capa")=="estaciones_ica"){
			textoHtmlAMostrar +='<div data-ex-content=".label"><a href="javascript:showICAChart('+feature.get("id")+',\''+selectedLayer.get("caption")+" "+feature.get("name")+"')\">Ver gráfico</a></span></div></strong>";

		}

		popupIndex = selectedLayer.get("idx")
		textoHtmlAMostrar += '</div>';
		var pos = feature.getGeometry().getClosestPoint([theX,theY]);
		var pixel = map.getPixelFromCoordinate(pos);
		var positioning = ''
		 if(pixel[1] > window.screen.height/2){
	          positioning = 'bottom-';
	        }else{
	          positioning = 'top-';
	        }

	        if(pixel[0] > window.screen.width/2){
	          positioning += 'right';
	        }else{
	          positioning += 'left';
	        }
	         overlay.setPositioning(positioning);
	         console.log(positioning);
		overlay.setPosition(pos);
		document.getElementById('popup-content').innerHTML = textoHtmlAMostrar;
		overlay.getElement().style.display =  '';

		

	}
	else{
		console.log("No hay campos para mostrar de "+selectedLayer.get("capa"));
	}

}

function showPopup(feature,textoHtmlAMostrar){
	popupContent.innerHTML=textoHtmlAMostrar;
	popupOverlay.setPosition(feature.getGeometry().getClosestPoint([theX,theY]));
}


function initPopup(){

/**
	var closer = document.getElementById('popup-closer');
	closer.onclick = function() {
		closePopup();
		return false;
	};
	*/
}

function closePopup() {
	overlay.getElement().style.display =  'none';
}

function closePopupLayer(index){
		if(popupIndex == index){
			closePopup();
		}
}
