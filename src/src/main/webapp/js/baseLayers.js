var fondos_wms;
var colores_rejilla = new Array();
function initBaseLayers(){

	$.ajax({
		url: server+"/BD_GIS/getBaseLayers.jsp",
		type: 'GET',
		success:	function(data) {
			fondos_wms=data.fondosWMS;
			var visible_pk;
			for (var i=0; i<data.grupos.length;i++){
				if(data.grupos[i].titulo == "Callejero"){
					visible_pk=data.grupos[i].pk;
				}

				$("#fondos").append('<li><a class="btnFondo" id="fondo_'+data.grupos[i].pk+'" href="javascript:setBaseLayer(\''+data.grupos[i].pk+'\')"'+
						' data-role="button" data-icon="icon_fondo_'+data.grupos[i].pk+'" data-iconpos="top">'+data.grupos[i].titulo+'</a>'+
						'<div id="fondo_opts_'+data.grupos[i].pk+'" class="fondo_opts"  style="visibility:hidden;"><select onchange="setBaseLayer(\''+data.grupos[i].pk+'\')" data-mini="true"></select>'+
				'</li>');

				$("#fondo_"+data.grupos[i].pk).css("background-image",'url("'+img_path+'/'+data.grupos[i].icono+'")');
				colores_rejilla[data.grupos[i].pk]=data.grupos[i].color_rejilla;
			}
			for (var i=0; i<data.fondosWMS.length;i++){
				$("#fondo_opts_"+data.fondosWMS[i].grupo+" select").append('<option '+(data.fondosWMS[i].visible=="1" ?'selected':'')+' value="'+i+'">'+data.fondosWMS[i].titulo+"</option>");
				if ($("#fondo_opts_"+data.fondosWMS[i].grupo+" option").length>1){
					$("#fondo_opts_"+data.fondosWMS[i].grupo).css("visibility","visible");
				}

			}

			setBaseLayer(visible_pk);
			map.updateSize();
			var cmdString = document.location.toString();
			var fondo =  getInitialParam(cmdString, "FONDO=");
			if (fondo){
				for (var i=0; i<fondos_wms.length;i++){
					if (fondos_wms[i].fondo_param==fondo){
						setBaseLayer(fondos_wms[i].grupo,i);		
						break;
					}
				}
				
			}

		},

		error: 		function(data, textStatus, errorThrown) {
			console.log('Error obteniendo el listado de capas. State: ' + data.readyState + ", Status:" + data.status);
			return false;
		}
	});


}

function getFondoWMS(pk){
	for (var i=0; i<fondos_wms.length;i++){
		if (fondos_wms[i].pk==pk){
			return i;
		}
	}
}

function updateGraticuleColor(fondo){
	var color = colores_rejilla[fondos_wms[fondo].grupo];
	if (graticule.strokeStyle_.getColor()!=color){
		graticule.strokeStyle_.setColor(color);
		if (graticule.getMap()){
			// invocamos 2 veces para refrescar (quitarla y ponerla)
			showGraticule();
			showGraticule();
		}
	}

}
function setBaseLayer(fondo,opcion){
	var index = opcion;
	if (!index){
		index = $("#fondo_opts_"+fondo+" select").val();
	}
	else{
		$("#fondo_opts_"+fondo+" select").val(index);
	}
	setOneBaseLayer(index,baseLayer);
	$('.btnFondo').removeClass("fondoSelected");
	$('#fondo_'+fondo).addClass("fondoSelected");
	if (fondos_wms[index].fondo_base ){
		setOneBaseLayer(getFondoWMS(fondos_wms[index].fondo_base),bottomBaseLayer);
		bottomBaseLayer.setVisible(true);
	}
	else{
		bottomBaseLayer.setVisible(false);
	}
	updateGraticuleColor(index);
}
function setOneBaseLayer(index,baseLayer){
	var url=fondos_wms[index].url;
	var layers= fondos_wms[index].layers;
	var styles=(fondos_wms[index].styles?fondos_wms[index].styles:"");
	var version= fondos_wms[index].version;
	var format=fondos_wms[index].format;
	var tiled=(fondos_wms[index].teselada=="1")||fondos_wms[index].teselada==1;
	var opacity=fondos_wms[index].opacidad;

	attributionControl.setCollapsible(true);
	if (fondos_wms[index].wmts){
		var capa = fondos_wms[index];

		var wmts = {url: capa.url,pk: capa.wmts, wms_url: capa.url_wms};
		if (!wmts_capabilities[capa.wmts]){
			loadWMTSCapabilities([wmts],wmts.pk);
		}
		var capabilities = wmts_capabilities[capa.wmts];
		if (capabilities) {
			var layer;
			var parser = new ol.format.WMTSCapabilities();
			var result = parser.read(capabilities);
			var options = ol.source.WMTS.optionsFromCapabilities(result,
					{layer: capa.layers, matrixSet: capa.gridset, format: capa.format});

			wmtsBaseLayer.setSource(new ol.source.WMTS(options));
			baseLayer.setVisible(false);
			wmtsBaseLayer.setVisible(true);
			wmtsBaseLayer.set('wmts_pk',capa.wmts);
			wmtsBaseLayer.set('wms_url',wmts.wms_url);
		} else {
			console.log("Error incluyendo capabilities de WMTS");
		}
		wmtsBaseLayer.setOpacity(opacity);
		if (fondos_wms[index].etiqueta){
			wmtsBaseLayer.getSource().setAttributions("Fondo seleccionado: "+fondos_wms[index].etiqueta);
		}

	}
	else if (layers=="OSM"){
		wmtsBaseLayer.setSource( new ol.source.XYZ({
				url:url 
		}));
		baseLayer.setVisible(false);
		wmtsBaseLayer.setVisible(true);
		wmtsBaseLayer.getSource().setAttributions("Fondo seleccionado: "+fondos_wms[index].etiqueta);
		//$(".ol-attribution").removeClass("ol-collapsed");
		attributionControl.setCollapsed(false);
		attributionControl.setCollapsible(false);
	}
	else{

		wmtsBaseLayer.setVisible(false);
		baseLayer.setVisible(true);
		if (tiled){
			baseLayer.setSource(new ol.source.TileWMS({
				params: {'LAYERS': layers,'VERSION':version,'FORMAT':format,'STYLES':styles},
				url: url,
				projection: map_projection
			})
			);
		}

		else{
			baseLayer.setSource( new ol.source.ImageWMS({
				params: {'LAYERS': layers,'VERSION':version,'FORMAT':format,'STYLES':styles},
				url: url,
				projection: map_projection
			})
			);
		}
		baseLayer.setOpacity(opacity);
		if (fondos_wms[index].etiqueta){
			baseLayer.getSource().setAttributions("Fondo seleccionado: "+fondos_wms[index].etiqueta);
		}
	}

}

function decTranspFondoSlider(slider){
	var value =Math.max($("#"+slider).val() - 10,  0);
	$("#"+slider).val( value);
	$("#"+slider).slider("refresh");
	updateFondoTransp();
}

function incTranspFondoSlider(slider){
	var value = Math.min(parseInt($("#"+slider).val())  + 10, 100);
	$("#"+slider).val( value);
	$("#"+slider).slider("refresh");
	updateFondoTransp();


}
function updateFondoTransp(){
	var transp = $("#transpFondoSlider").val();
	baseLayer.setOpacity((100-transp)/100);
	bottomBaseLayer.setOpacity((100-transp)/100);
}