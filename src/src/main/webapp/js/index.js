var server = "";
var img_path = "/lib/IDEAragon/themes/default/images";
var urlApp = server+"/visor_3.0";

var showBOA = true;
NOTICIAS_JSP_URL="/BD_GIS/consultaNoticias.jsp";
AVISOS_JSP_URL="/BD_GIS/consultaAvisos.jsp";

var collapsedTimestamp=0;
function initAppByURL() {
	var urlpath = document.location.pathname;
$(".forInstance").hide();
	if (urlpath.toLowerCase().indexOf("salud") > 0) {
		
		APP_NAME = "DVS";
		/*document.getElementById("header_text").src = "themes/visorSIOSE/images/titulo.png";*/
		document.getElementById("header_text").alt = "Salud Dónde Vivo";
		document.title = "IDEAragon > Aplicaciones > Salud Dónde Vivo";
		
		$("#appLabel").text("SALUD DONDE VIVO");
		$("#texto0").attr("placeholder","ZBS o área de salud o instalación sanitaria o calle (con o sin portal) o código postal o localidad. Por ejemplo: Zalfonada / Zaragoza III / Miguel Servet / maria agustin 36, Zaragoza / daroca / 50015")
		$("#weatherTool").hide();
		$("#tocSearchText").show();
	}
	$("#explicacion"+APP_NAME).show();
	$(".ejemploBus"+APP_NAME).show();
	$("#upperToolbar"+APP_NAME).show();
}

$(document).ready(function() {
	initAppByURL();
	initNoticias();
	//setTimeout(function(){ setDialogMaxHeight(); }, 1000);
});

function showLoading(show){
	if (show)
		document.getElementById("loadingOverlay").style.visibility="visible";
	else
		document.getElementById("loadingOverlay").style.visibility="hidden";
}

function closeDialog(id) {
	$("#" + id).dialog("close");
}

function closeMobileDialog(id) {
	if ($("#map").width()<=$("#" + id).width()*2){
		closeDialog(id);
	}
}

function hideMobileDialog(id) {
	if ($("#map").width()<=$("#" + id).width()*2){
		$("#" + id).hide();
	}
}
function closeAllDialogs() {
	$('.dialogWindow').hide();
}

function openDialog(dialog) {
	$.mobile.changePage('#mainPage', {transition: 'pop', role: 'dialog'});
	$('#'+dialog).show();
}

function creaVentanaAviso(msj, esModal,dialogToOpen) {
	closeAllDialogs();
	$("#modalContent").html(msj);
	if (dialogToOpen){
		$("#modalDialog a").prop("href","javascript:openDialog('"+dialogToOpen+"')");
	}
	else{
		$("#modalDialog a").prop("href","#mainPage");
	}
	$.mobile.changePage('#modalDialog', {transition: 'pop', role: 'dialog'});
}


function cancelBubble(event) {
	event.cancelBubble = true;
}

function activateTab(id,btn,tabs,toolbar){
	$(tabs).addClass("oculto");
	$(id).removeClass("oculto");
	$(toolbar+" a").removeClass("tabSelected");
	$(btn).addClass("tabSelected");

}

function bindCollapse(id){
	$( "#" + id+" h3" ).unbind();
	$( "#" + id+" h3" ).bind("click",function(event){
		collapse(event,id);
	});
}
function collapse(event,id){
	// en chrome de móvil se llama dos veces seguidas a este evento. Para que no se expanda y colapse seguidamente (y viceversa) controlamos la diferencia de los timestamps de un evento con el anterior
	if (event.timeStamp - collapsedTimestamp>100){
		collapsedTimestamp=event.timeStamp;
	cancelBubble(event);
	event.preventDefault();
	if ($("#"+id).collapsible( "option", "collapsed" )){
		$("#"+id).collapsible("expand");
	}
	else{
		$("#"+id).collapsible("collapse");
	}
	}
	
}

function setDialogMaxHeight(){
	$(".dialogWindow .dialogFullContent").css("max-height",$("#map").height()*0.90-$(".ui-header").height());


}

proj4.defs('EPSG:4258', '+title=Geograficas ETRS89 +proj=longlat +ellps=GRS80 +no_defs ');
proj4.defs('EPSG:25830', '+title=25830 +proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs ');
ol.proj.proj4.register(proj4);