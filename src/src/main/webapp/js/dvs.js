var resultadosSalud=[];
var resultadosSaludZBS=[];
var pendingSaludZBS=[];
function consultaIndicadoresSalud(zbs_objectid){
	if (pendingSaludZBS.includes(zbs_objectid) || resultadosSaludZBS.includes(zbs_objectid)){
		return;
	}
	showLoading(true);
	pendingSaludZBS.push(zbs_objectid);
	$.ajax({
		url: 'consultaIndicadoresSalud.jsp?zbs='+zbs_objectid,
		type: 'GET',

		success:  function(data) {
			pendingSaludZBS.splice(pendingSaludZBS.indexOf(zbs_objectid),1);
			var cql_filter ='objectid='+zbs_objectid;
			//$("#toc_table tr").removeClass("sita_enabled");
			var json = JSON.parse(data);
			resultadosSalud.push(json.indicadores);
			resultadosSaludZBS.push(zbs_objectid);
			showResultSalud(json.indicadores, cql_filter);
			showLoading(false);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			/*	$("#SOCIEDADLoading").css("visibility","hidden");
  			$("#ECONOMIALoading").css("visibility","hidden");*/
			alert("No se pudieron obtener los indicadores estadísticos de la ZBS:"+jqXHR.responseText);
			showLoading(false);
		}
	});
}

function showResultSalud(resultadosSITA, cql_filter_muni){
	
	for (var t in resultadosSITA){

		var layerGroup = getOLLayerByTabla(t);

		if(layerGroup[0]){
			var groupIndx = layerGroup[0].get('idx').split('_')[0];
			//	document.getElementById("count_"+groupIndx).innerHTML=" (0)";
		}
		var lastValue=[];
		var anno=null;
		if (resultadosSITA[t].valores.length>0){
			lastValue = getLastSITAValue(resultadosSITA[t].valores);
			if (resultadosSITA[t].valores.length>1){
			anno=lastValue[0];
			}
		}
		for(var i in layerGroup){

			var layer = layerGroup[i];
			if (layer instanceof ol.layer.Vector){
				var cql_filter=cql_filter_muni;
		
				
				if (resultadosSITA[t].valores.length==0){
					cql_filter="1=0";
					//$("#count_"+groupIndx).html(" (0)");
				}
				else{
					var count =parseInt($("#count_"+groupIndx).text().trim().replace("(","").replace(")",""));

					$("#count_"+groupIndx).html(" ("+(count ? count+1: 1)+")");
					$('#layer_'+groupIndx).attr('style','color:black');
					$('#chk_layer'+groupIndx).attr('style','visibility:visible');
					
					


				}


				if (lastValue.length>0){
					var projection = new ol.proj.Projection({code:"EPSG:25830"});
					var vectorSource = new ol.source.Vector({
						format: new ol.format.GeoJSON({defaultDataProjection: projection, featureProjection:projection}) ,
						url:  layer.get("url")+'?service=WFS&' +
						'version=1.0.0&request=GetFeature&typename='+layer.get("capa") +(anno ? "_"+anno:"")+'&CQL_FILTER='+cql_filter+
						'&outputFormat=application/json&srsname=EPSG:25830'
					});
					layer.setSource(vectorSource);

					layer.set("lastDate",lastValue[0]);
					layer.set("lastValue",lastValue[1]);
					if ($('#layer_'+groupIndx+" .plotBtn").length<=0){
						$("<a  class='plotBtn ui-btn ui-icon-info ui-btn-icon-notext ui-corner-all' onclick='openOnly("+groupIndx+")''></a>").insertAfter('#count_'+groupIndx);

						
					}
				}
				if (layer.get("visible")){
					 
				    	  openOnly(layer.get("idx"),true);
				     
				}
			}
			else{
				layer.getSource().getParams().LAYERS= layer.get("capa") +(anno ? "_"+anno:"");
				if (anno){
				$('#imgLey_' + layer.get("capa")).attr("id",'imgLey_' + layer.get("capa") + "_"+anno); 
					}
			}

			//layer.set("lastDate",lastValue[0]);
			//layer.set("lastValue",lastValue[1]);
		}
	}


}

function showDataSalud(capa){
	var series=[];
	var nombres=[];
	var noDate=true;
	for (var i=0; i<resultadosSalud.length;i++){
		var datos = resultadosSalud[i][capa];
		var serie=[];
		nombres.push(resultadosSalud[i][capa].zbs);
		for (var j=0; j<datos.valores.length;j++){
			serie.push([formatDate(datos.valores[j][0]),datos.valores[j][1]]);
		}
		series.push(serie);
	}

	createChartEvolPlotSalud("chart_"+capa,datos.unidad,series,nombres, noDate);
}