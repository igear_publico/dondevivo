var maxSearchResults=10;
var defaultBuffer=500;
var bufferPoints=300;
var searchLayer;
var endSearchInterval;
var tiposFinalizados = [];
var _tipos = 0;
var hayResultado = false;
var searching = false;
var ajaxSearchs = [];
var featureBuffer;
var radioLugar;
var ret_objectid;

function initSearch() {
	if (!searchLayer){
		var source = new ol.source.Vector({features: []});
		searchLayer = new ol.layer.Vector({  source: source});
		map.addLayer(searchLayer);
	}
}

function cancelSearch(){
	for (var i=0; i<ajaxSearchs.length;i++){
		ajaxSearchs[i].abort();
	}

	searching=false;

	try{
		clearInterval(endSearchInterval);
	}
	catch(err){
	}

	showSearching(false);
	$("#cancelSearchBtn").hide();
	$("#searchBtn").show();
	$("#searchText").attr('readonly', false);
}


function doSearch() {
	$("#searchHelp").collapsible( "collapse" );
	if (searching){
		return;
	}
	searching=true;
	showSearching(true);
	$("#searchBtn").hide();
	$("#cancelSearchBtn").show();

	$("#searchText").attr('readonly', true);
	$("#noSearchResults").hide();
	ajaxSearchs = [];
	$(".itemResult").remove();
	if (!searchLayer){
		initSearch();
	}
	var texto = $("#searchText").val();
	buscar(texto);

	searching=false;
	showSearching(false);
  $("#cancelSearchBtn").hide();
  $("#searchBtn").show();
  $("#searchText").attr('readonly', false);

}

function changeBufferGeom(distancia){
	if (featureBuffer){
		featureBuffer.getGeometry().setRadius(radioLugar+distancia);
		featureBuffer.getProperties().Distancia=distancia;
		//map.getView().fit(featureBuffer.getGeometry().getExtent(), map.getSize());
	}
}
function refreshMap(){
  	map.getLayers().forEach(function (lyr) {
                               lyr.changed();
                           });
}

function showToponimo(coords,keepExtent) {
	var source = searchLayer.getSource();
	source.clear();
	var coordinates = coords.split(":");
	var point = new ol.Feature({
		  geometry:  new ol.geom.Point([parseFloat(coordinates[0]),parseFloat(coordinates[1])])
		});
	point.setStyle(new ol.style.Style({image:new ol.style.Icon({src:img_path+'/chincheta.png'})}));
	source.addFeature(point);
	source.refresh();
	 if(!keepExtent){
	map.getView().fit( ol.extent.buffer( point.getGeometry().getExtent(),bufferPoints), map.getSize());
	 }
	$("#searchDelete").show();
	$( "#searchDialog" ).hide();
}

function showFeatures(layer,cql_filter,filterCalle,keepExtent, async){
	$.ajax({
		url: server+'/Visor2D?service=WFS&' +
		'version=1.0.0&request=GetFeature&typename='+layer+'&' +
		'outputFormat=application/json&srsname=EPSG:25830&' +
		'CQL_FILTER='+cql_filter,
		type: 'GET',
		async: async,
		
		success:  function(data){
			var format = new ol.format.GeoJSON();
			var features = format.readFeatures(data);
			if (features.length>0){
				var geom = features[0].getGeometry();
				var buffer=defaultBuffer;
				if (geom.getType()=='Point') {
					buffer=bufferPoints;
				}
				/**
				if (!keepExtent){
					map.getView().fit(ol.extent.buffer( features[0].getGeometry().getExtent(),buffer), map.getSize());
				}
				*/
				var source = searchLayer.getSource();
				source.clear(true);
				features[0].setStyle(new ol.style.Style({
					image:new ol.style.Icon({src:img_path+'/chincheta.png'}),
					fill: new ol.style.Fill({
						color: 'rgba(0,0,0,0)'
					}),
					stroke: new ol.style.Stroke({
						color:'rgba(0,162,232,1)',
						width: 3
					})
				}));
				features[0].set("layer",layer);
				features[0].set("cql_filter",cql_filter);
				source.addFeature(features[0]);
				ret_objectid=features[0].getProperties().objectid;

				var Extent = geom.getExtent();
        var ancho =(Extent[2]-Extent[0])/2;
        var alto = (Extent[3]-Extent[1])/2;
        var X = Extent[0] + ancho;
        var Y = Extent[1] + alto;
        radioLugar = Math.max(ancho,alto);
        if (APP_NAME !='DVS'){
        	getResultsGasolineras(X,Y,radioLugar);
        }
        var radioDist=getSelectedDistance();
        featureBuffer = new ol.Feature({
          geometry: new ol.geom.Circle([X,Y],radioLugar+ radioDist),
          Distancia: radioDist
        });

        featureBuffer.setStyle(new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(0,0,0,0)'
          }),
          stroke: new ol.style.Stroke({
            color:'rgba(0,162,232,0.8)',
            width: 5
          })
        }));
        source.addFeature(featureBuffer);

        map.getView().fit(featureBuffer.getGeometry().getExtent(), map.getSize());


				source.refresh();
				$("#searchDelete").show();
				$( "#searchDialog" ).hide();
			}
			else if(layer=="TroidesV"){
				ret_objectid=-1;
				creaVentanaAviso("No se ha encontrado el portal indicado, se mostrará la calle completa");
				tipoActual = "BusCallUrb";
				showFeatures( "BusCallUrb",filterCalle,null,keepExtent, false);
			}
			else{

				console.log("No se ha encontrado el objeto con la condicion "+cql_filter+" en la capa "+layer);
				creaVentanaAviso("No se ha encontrado el lugar indicado");
				ret_objectid=-1;

			}
		},
		error:  function(  jqXHR,  textStatus,  errorThrown) {
			ret_objectid=-1;
			creaVentanaAviso("No se ha podido cargar el objeto en el mapa "+textStatus +"\n "+errorThrown);
		},
		complete: function(xmlHttpRequest) {
			closeAllDialogs();
			openTocDialog();
    }
	});
}
function showSearchResult(idSelectionLayer,objectid) {

	var selectionWhere="objectid="+objectid;
	ret_objectid=objectid;
	var layer = idSelectionLayer;
	var filterCalle;

	var async=true;
	if (idSelectionLayer.indexOf("TroidesV")==0){
		layer="TroidesV";
		async=false;
		selectionWhere="c_mun_via='"+objectid+"' AND numero="+portal.split("_")[1];
		filterCalle="c_mun_via='"+objectid+"'";
	}

	showFeatures(layer,selectionWhere,filterCalle,null,async);
	if (tipos_layer[layer]){
		layer = tipos_layer[layer];
	}

	return ret_objectid;
	
}
function confirmShowLayer(layer) {
	//$.mobile.changePage("#delConfirmationDialog");
	//cambio a dialog

	$("#resultLayer").val(layer);

	$('#showLayerConfirmationDialog').draggable({
		cursor: 'move',
		containment: "#mainPage",
		scroll: false
	});

	$("#showLayerConfirmationDialog").addClass("ui-page-theme-a ui-corner-all");
	$("#showLayerConfirmationDialog div[data-role='header']").addClass("ui-header ui-bar-inherit");
	$(".dialogTitle").addClass("ui-title");

	$("#showLayerConfirmationDialog div[data-role='button']").button();
	//$("#delConfirmationDialog a[data-role='button']").button();
	//$("#drawTypeSel").trigger("create")

	closeAllDialogs();
	$("#showLayerConfirmationDialog").show();
	$("#closeButtonShowLayerConfirmationDialog").bind("vclick", function (event, ui) { event.preventDefault();$('#showLayerConfirmationDialog').hide(); });
}

function showResultLayer() {
	$('#showLayerConfirmationDialog').hide();
	activarCapas($("#resultLayer").val(),true);
	
}
function dontShowResultLayer() {
	$('#showLayerConfirmationDialog').hide();
	
	
}


function showSearching(show){
	if (show){
		$("#searchingOverlay").show();
	}
	else{
		$("#searchText").attr('readonly', false);
		$("#cancelSearchBtn").hide();
		$("#searchBtn").show();
		$("#searchingOverlay").hide();
	}
}

function openSearchDialog() {
	if ($('#searchDialog').css("display")!="none"){
		$('#searchDialog').hide();
	}
	else{
	if ($("#muniCListContainer *").length==0){
		$.get("/BuscadorCajaUnica/municipios_catastrales.html", function(data){
			$("#muniCListContainer").append(data);
		});
	}
	$('#searchDialog').resizable(
			{"alsoResize":"#searchDialog .dialogFullContent"});
	$('#searchDialog').draggable({
        cursor: 'move',
		containment: "#mainPage",
		scroll: false,
		handle: '.ui-header'
    });
	$("#searchDiv").trigger("create");
	$("#searchDialog").addClass("ui-page-theme-a ui-corner-all");
	$("#searchDialog div[data-role='header']").addClass("ui-header ui-bar-inherit");
	$(".dialogTitle").addClass("ui-title");

	$("#searchDialog div[data-role='button']").button();
	$("#searchHelp").collapsible();
	closeAllDialogs();
	$("#searchDialog").show();
	$("#closeButtonSearchDialog").bind("vclick", function(event, ui) { event.preventDefault();$('#searchDialog').hide(); });

	}
}

function inputKeyUpSearch(e) {
    e.which = e.which || e.keyCode;
    if(e.which == 13) {
    	doSearch();
    }
}
