<%@ page language='java' contentType='text/html;charset=UTF-8'%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.net.ssl.*"%>
<%@ page import="javax.xml.parsers.*"%>
<%@ page import="org.w3c.dom.*"%>


<%
	String x0=request.getParameter("x0");
	String y0=request.getParameter("y0");
	String x1=request.getParameter("x1");
	String y1=request.getParameter("y1");
	
	
	String postData ="{\"tipoEstacion\":\"EESS\",\"idProvincia\":null,\"idMunicipio\":null,\"rotulo\":\"\",\"eessEconomicas\":false,\"conPlanesDescuento\":false,\"horarioInicial\":\"\",\"horarioFinal\":\"\",\"calle\":\"\",\"numero\":\"\",\"codPostal\":\"\",\"tipoVenta\":null,\"idOperador\":null,\"nombrePlan\":\"\",\"idTipoDestinatario\":null,\"x0\":"+x0+",\"y0\":"+y0+",\"x1\":"+x1+",\"y1\":"+y1+"}";
	//out.println(postData);


	try{

			// Create a trust manager that does not validate certificate chains
      TrustManager[] trustAllCerts = new TrustManager[]{
          new X509TrustManager() {
              public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                  return null;
              }
              public void checkClientTrusted(
                  java.security.cert.X509Certificate[] certs, String authType) {
              }
              public void checkServerTrusted(
                  java.security.cert.X509Certificate[] certs, String authType) {
              }
          }
      };



			SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());

			URL url = new URL("https://geoportalgasolineras.es/rest/busquedaEstacionesInfo");
			HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
			connection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", 
	           "application/json");

			connection.setDoOutput(true);
			 connection.setDoInput(true);
		     DataOutputStream wr = new DataOutputStream (
	                  connection.getOutputStream ());
	      wr.writeBytes (postData);
	      wr.flush ();
	      wr.close ();


			//read the result from the server
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = rd.readLine()) != null)
			{
				//out.println(line);
				sb.append(line + '\n');
			}
			connection.disconnect();
			
			out.print(sb.toString());
		
			
		
	}
	catch(MalformedURLException ex){
			out.println(ex);
			out.println("Error en la consulta "+postData);
	}
	catch(java.net.SocketTimeoutException ex){
			out.println("Tiempo máximo de respuesta agotado "+postData);
	}
	catch(IOException ex){
				out.println(ex);
		out.println("Error en la consulta "+postData);
	}

	
	%>
