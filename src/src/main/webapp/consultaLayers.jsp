<%@ page language='java' contentType='text/html;charset=UTF-8'%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.xml.parsers.*"%>
<%@ page import="org.w3c.dom.*"%>
<%@ page import = "conexion.*" %>
<jsp:useBean id="dbConn" class="conexion.conexionBD" scope="request"/>
<%
	
Connection conn=null;//Objeto para la conexión a la BD
Statement stmt=null;//Objeto para la ejecucion de sentencias
ResultSet rs=null;//Objeto para guardar los resultados
String service=request.getParameter("SERVICE");

try {
//Nos conectamos a la BD
	conn=dbConn.getConnection("conex"+(service!=null?service:"DV"));
	//Creamos una sentencia a partir de la conexión, que refleje cambios y solo de lectura
	stmt=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	//Seleccionamos la tabla

	String  query = "SELECT * FROM v_dondevivo_capas order by orden";
	rs=stmt.executeQuery(query);
	out.print("{\"capas\":[");
	boolean first=true;	
	while (rs.next()){
		if (!first){
			out.print(",");
		}
		first=false;
		out.print("{\"bloque\":\""+rs.getString("bloque")+"\",");
		out.print("\"titulo\":\""+rs.getString("titulo")+"\",");
		out.print("\"tablas\":\""+rs.getString("tablas")+"\",");
		out.print("\"servicio\":\""+rs.getString("servicio")+"\",");
		out.print("\"grafico\":\""+rs.getString("grafico")+"\",");
		try{
		out.print("\"capas_excluyentes\":"+rs.getBoolean("capas_excluyentes")+",");
		}
		catch(SQLException ex){
		}
		try{
		out.print("\"opacity\":"+rs.getDouble("opacity")+",");
		}
		catch(SQLException ex){
		}
		try{
		out.print("\"info\":\""+rs.getString("info")+"\",");
		}
		catch(SQLException ex){
		}
		out.print("\"capas\":\""+rs.getString("capas")+"\"}");
		
		
	}
	out.print("]}");
	
	} 
catch (Exception e2) {
		out.println("Fallo: " + e2.getMessage());
		
	} 	finally {
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e2) {
			out.println("Fallo al desconectar SQL: " + e2.getMessage());
		}
		try {
			if (stmt != null)
				stmt.close();
		} catch (SQLException e3) {
			out.println("Fallo al cerrar sentencia SQL: "
					+ e3.getMessage());
		}

	}
	
	%>





