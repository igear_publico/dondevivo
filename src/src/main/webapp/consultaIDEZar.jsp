<%@ page language='java' contentType='text/html;charset=UTF-8'%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.xml.parsers.*"%>
<%@ page import="org.w3c.dom.*"%>

<%
	String x=request.getParameter("x");
	String y=request.getParameter("y");
	String layer=request.getParameter("layer");
	Double radio =new Double(request.getParameter("radio"));
	Double[] distancias = new Double[]{1000.0,5000.0,10000.0};
	StringBuilder[] resultados = new StringBuilder[distancias.length];
	String parametrized_url = "http://www.zaragoza.es/sede/servicio/"+layer+"?srsname=etrs89&point="+x+","+y+"&fl=id,title&distance=";
	try{
		
		for (int i=0; i<distancias.length;i++){
			Double distancia = distancias[i]+radio;
			//out.println(parametrized_url+distancia.intValue());
			URL url = new URL(parametrized_url+distancia.intValue());
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.connect();

			//read the result from the server
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				sb.append(line + '\n');
			}
			connection.disconnect();
	
			resultados[i]= sb;
		}
	}
	catch(MalformedURLException ex){
			out.println("Error en la consulta "+parametrized_url);
	}
	catch(java.net.SocketTimeoutException ex){
			out.println("Tiempo máximo de respuesta agotado "+parametrized_url);
	}
	catch(IOException ex){
		out.println("Error en la consulta "+parametrized_url);
	}
	
	out.print("{\"resultados\":[");
	for (int i=0; i<distancias.length;i++){
		if (i>0){
			out.print(",");
		}
		out.print("{\"capa\":\""+layer+"\",\"distancia\":"+distancias[i]+",\"featureCollection\":");
		out.print(resultados[i]);
		out.print("}");
	}
	out.print("]}");
	%>
