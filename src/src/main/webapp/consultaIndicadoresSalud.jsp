<%@ page language='java' contentType='text/html;charset=UTF-8'%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.xml.parsers.*"%>
<%@ page import="org.w3c.dom.*"%>
<%@ page import = "conexion.*" %>
<jsp:useBean id="dbConn" class="conexion.conexionBD" scope="request"/>
<%
	
Connection conn=null;//Objeto para la conexión a la BD
Statement stmt=null;//Objeto para la ejecucion de sentencias
ResultSet rs=null;//Objeto para guardar los resultados
Statement stmt2=null;//Objeto para la ejecucion de sentencias
ResultSet rs2=null;//Objeto para guardar los resultados
String objectid=request.getParameter("zbs");

try {
//Nos conectamos a la BD
	conn=dbConn.getConnection("conexDVS");
	//Creamos una sentencia a partir de la conexión, que refleje cambios y solo de lectura
	stmt=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	//Seleccionamos la tabla
	
	String  query = "SELECT distinct s.sexo,dv.bloque,dv.id,c.capa,s.nombre_indicador_agrupado::text || "+
        "CASE WHEN s.sexo::text = '1'::text THEN ' (hombres)'::text ELSE CASE WHEN s.sexo::text = '2'::text THEN ' (mujeres)'::text ELSE ''::text END END AS titulo,s.unidad_medida_indicador as unidad,'Gobierno de Aragón. Departamento de Sanidad. Dirección General de Salud Pública.' as fuente,'saludp.v_atlas_sp' as tabla FROM saludp.dv_salud_valores dv, saludp.v_indicadores s, saludp.capas_salud c  WHERE  s.codigo_indicador_agrupado::text = dv.id::text AND c.identificador_indicador::text = s.codigo_indicador_agrupado::text and c.sexo::text=s.sexo::text order by bloque";
	rs=stmt.executeQuery(query);
	out.println("{\"indicadores\":{");
	boolean first=true;	
	while (rs.next()){
		if (!first){
			out.print(",");
		}
		first=false;
		out.println("\""+rs.getString("capa")+"\":{");
		out.print("\"unidad\":\""+rs.getString("unidad")+"\",");
		out.print("\"fuente\":\""+rs.getString("fuente")+"\",");
		
		stmt2=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
		rs2=stmt2.executeQuery("SELECT codigo,nombre FROM saludplan.zonas_salud where objectid='"+objectid+"'");
		
		if (rs2.next()){
		out.print("\"zbs\":\""+rs2.getString("nombre")+"\",");
		out.print("\"valores\":[");
		rs2=stmt2.executeQuery("SELECT t.valor_redondeado as valor,t.periodo_de_estudio_indicador as tempo FROM "+rs.getString("tabla")+" t,v_indicadores v where t.identificador_del_indicador=v.identificador_indicador and v.codigo_indicador_agrupado='"+rs.getString("id")+"'  and v.sexo='"+rs.getString("sexo")+"' and codzbs='"+rs2.getString("codigo")+"' order by t.periodo_de_estudio_indicador");
		boolean firstval=true;
		while (rs2.next()){
			if (!firstval){
				out.print(",");
			}
			firstval=false;
			out.print("[\""+rs2.getString("tempo")+"\",");	
			out.print(rs2.getString("valor")+"]");
		
			
		}
		}
		else{
			out.print("\"valores\":[");
			}
		out.println("]}");
		
	}
	out.println("}}");
	
	} 
catch (Exception e2) {
		out.println("Fallo: " + e2.getMessage());
		
	} 	finally {
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e2) {
			out.println("Fallo al desconectar SQL: " + e2.getMessage());
		}
		try {
			if (stmt != null)
				stmt.close();
		} catch (SQLException e3) {
			out.println("Fallo al cerrar sentencia SQL: "
					+ e3.getMessage());
		}
try {
			if (stmt2 != null)
				stmt2.close();
		} catch (SQLException e4) {
			out.println("Fallo al cerrar sentencia SQL: "
					+ e4.getMessage());
		}
	}
	
	%>





