NOTICIAS_JSP_URL="/BD_GIS/consultaNoticias.jsp";
AVISOS_JSP_URL="/BD_GIS/consultaAvisos.jsp";

WMS_PAISAJE_URL = "https://idearagon.aragon.es/arcgis/services/AragonReferencia/MPA/MapServer/WMSServer";
WMS_PAIAJE_LAYERS="48,49"

var url = "/geoserver/VISOR2D/ows";
var defaultBuffer=500;
var bufferPoints=300;
var idearagonWMS = "/Visor2D?SRS=EPSG:25830&version=1.1.1&service=WMS&FORMAT=image/png&TRANSPARENT=TRUE&request=GetMap";

BASE_LAYERS=2;
BASE_WMS_URL_0="https://idearagon.aragon.es/arcgis/services/AragonReferencia/Basico_NEW/MapServer/WMSServer";
BASE_LAYER_0="0,2,4,5,6,8,9,10,11,12,13,14,15,16,17,18,19";
BASE_TITLE_0="Cartograf&iacute;a de referencia";
BASE_LEGEND_0="config/legends/referencia.png";

BASE_WMS_URL_1="https://wmspro-idearagon.aragon.es/erdas-iws/ogc/wms/AragonFotos";
BASE_LAYER_1="2018_pnoa";
BASE_TITLE_1="PNOA 2018";



//CAPAS REFERENCE => se pintan encima del vectorial
REFERENCE_LAYERS=0;
REFERENCE_WMS_URL_0="https://idearagon.aragon.es/arcgis/services/AragonReferencia/Basico/MapServer/WMSServer";
REFERENCE_LAYER_0="0,2,4,5,6,16,17,18,19";
REFERENCE_TITLE_0="Cartograf&iacute;a de referencia";
REFERENCE_LEGEND_0="config/legends/referencia.png";

WMS_SRS="EPSG:25830";
WMS_BBOX_X_MIN=221580;
WMS_BBOX_Y_MIN=3402223;
WMS_BBOX_X_MAX=987351;
WMS_BBOX_Y_MAX=4864969;
	
OVERVIEW_BOUND_X_MIN = 530000;
OVERVIEW_BOUND_X_MAX= 855000;
OVERVIEW_BOUND_Y_MIN = 4410000;
OVERVIEW_BOUND_Y_MAX= 4760000;

APP_NAME="DV";

var tablas=new Array();

var maxSearchResults=30;
tablas["BusCallUrb"]="carto.t111_ejes";
tablas["Localidad"]="carto.t112_nucleos";
tablas["TroidesV"]="carto.t111_troidesvisor";
tablas["v111_codigo_postal"]="carto.v111_codigo_postal";
tablas["v206_zonas_salud"]="saludplan.v206_zonas_salud";
tablas["v206_area_salud"]="saludplan.v206_area_salud";
tablas["v206_centros_salud"]="saludplan.v206_centros_salud";
tablas["v206_mental"]="saludplan.v206_mental";
tablas["v206_hospitales"]="saludplan.v206_hospitales";
tablas["v206_consultorios"]="saludplan.v206_consultorios";
tablas["instalaciones_sanitarias"]="geoext.inst_sanitarias";



server="//" + document.location.host;
var printActionURL = server + "/Mapa/";
var printActionXSL ="/app/jboss/Mapa/templates/informeDondeVivo.xsl";
//var printActionXSL ="c:/Mapa/templates/informeDondeVivo.xsl";

var printWidth = 1200;
var printHeight = 572;
var printMapWidth = 1200;
var printMapHeight = 572;
var printWidthAragon = 325;
var printHeightAragon = 394;
var xminOverview = 530000;
var ymaxOverview = 4760000;
var xmaxOverview = 855000;
var yminOverview = 4410000;
var printWidthOverview=130;
var printHeightOverview=140;
printAppUrl4Legend = server;
var printOverviewURL= server + "/BD_GIS/getOverviewMap.jsp";
var printScaleURL= server + "/BD_GIS/getMapScale.jsp";
var printMarkersURL= server + "/BD_GIS/getMarkersMap.jsp";