﻿

var fieldAliasList2 = new Array();

//Energia Eolica
fieldAliasList2["v_pol_anexo_ii_origen"] = new Array();
fieldAliasList2["v_pol_anexo_ii_origen"]["parque"] = "Parque";
fieldAliasList2["v_pol_anexo_ii_origen"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_anexo_ii_origen"]["titular"] = "Titular";
fieldAliasList2["v_pol_anexo_ii_origen"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_anexo_ii_origen"]["estado"] = "Estado";
fieldAliasList2["v_pol_anexo_ii_origen"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_anexo_iii_origen"] = new Array();
fieldAliasList2["v_pol_anexo_iii_origen"]["parque"] = "Parque";
fieldAliasList2["v_pol_anexo_iii_origen"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_anexo_iii_origen"]["titular"] = "Titular";
fieldAliasList2["v_pol_anexo_iii_origen"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_anexo_iii_origen"]["estado"] = "Estado";
fieldAliasList2["v_pol_anexo_iii_origen"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_pendiente_aa_previa"] = new Array();
fieldAliasList2["v_pol_pendiente_aa_previa"]["parque"] = "Parque";
fieldAliasList2["v_pol_pendiente_aa_previa"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_pendiente_aa_previa"]["titular"] = "Titular";
fieldAliasList2["v_pol_pendiente_aa_previa"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_pendiente_aa_previa"]["estado"] = "Estado";
fieldAliasList2["v_pol_pendiente_aa_previa"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_aut_adm_previa"] = new Array();
fieldAliasList2["v_pol_aut_adm_previa"]["parque"] = "Parque";
fieldAliasList2["v_pol_aut_adm_previa"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_aut_adm_previa"]["titular"] = "Titular";
fieldAliasList2["v_pol_aut_adm_previa"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_aut_adm_previa"]["estado"] = "Estado";
fieldAliasList2["v_pol_aut_adm_previa"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_aut_adm_construccion"] = new Array();
fieldAliasList2["v_pol_aut_adm_construccion"]["parque"] = "Parque";
fieldAliasList2["v_pol_aut_adm_construccion"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_aut_adm_construccion"]["titular"] = "Titular";
fieldAliasList2["v_pol_aut_adm_construccion"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_aut_adm_construccion"]["estado"] = "Estado";
fieldAliasList2["v_pol_aut_adm_construccion"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_perea"] = new Array();
fieldAliasList2["v_pol_perea"]["parque"] = "Parque";
fieldAliasList2["v_pol_perea"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_perea"]["titular"] = "Titular";
fieldAliasList2["v_pol_perea"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_perea"]["estado"] = "Estado";
fieldAliasList2["v_pol_perea"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_anexo_ii_tramit"] = new Array();
fieldAliasList2["v_pol_anexo_ii_tramit"]["parque"] = "Parque";
fieldAliasList2["v_pol_anexo_ii_tramit"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_anexo_ii_tramit"]["titular"] = "Titular";
fieldAliasList2["v_pol_anexo_ii_tramit"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_anexo_ii_tramit"]["estado"] = "Estado";
fieldAliasList2["v_pol_anexo_ii_tramit"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_anexo_iii_tramit"] = new Array();
fieldAliasList2["v_pol_anexo_iii_tramit"]["parque"] = "Parque";
fieldAliasList2["v_pol_anexo_iii_tramit"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_anexo_iii_tramit"]["titular"] = "Titular";
fieldAliasList2["v_pol_anexo_iii_tramit"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_anexo_iii_tramit"]["estado"] = "Estado";
fieldAliasList2["v_pol_anexo_iii_tramit"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_i+d_tramit"] = new Array();
fieldAliasList2["v_pol_i+d_tramit"]["parque"] = "Parque";
fieldAliasList2["v_pol_i+d_tramit"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_i+d_tramit"]["titular"] = "Titular";
fieldAliasList2["v_pol_i+d_tramit"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_i+d_tramit"]["estado"] = "Estado";
fieldAliasList2["v_pol_i+d_tramit"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_reg_general_tramit"] = new Array();
fieldAliasList2["v_pol_reg_general_tramit"]["parque"] = "Parque";
fieldAliasList2["v_pol_reg_general_tramit"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_reg_general_tramit"]["titular"] = "Titular";
fieldAliasList2["v_pol_reg_general_tramit"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_reg_general_tramit"]["estado"] = "Estado";
fieldAliasList2["v_pol_reg_general_tramit"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_protegidas"] = new Array();
fieldAliasList2["v_pol_protegidas"]["parque"] = "Parque";
fieldAliasList2["v_pol_protegidas"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_protegidas"]["titular"] = "Titular";
fieldAliasList2["v_pol_protegidas"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_protegidas"]["estado"] = "Estado";
fieldAliasList2["v_pol_protegidas"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_a_ii_renuncia"] = new Array();
fieldAliasList2["v_pol_a_ii_renuncia"]["parque"] = "Parque";
fieldAliasList2["v_pol_a_ii_renuncia"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_a_ii_renuncia"]["titular"] = "Titular";
fieldAliasList2["v_pol_a_ii_renuncia"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_a_ii_renuncia"]["estado"] = "Estado";
fieldAliasList2["v_pol_a_ii_renuncia"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_a_iii_renuncia"] = new Array();
fieldAliasList2["v_pol_a_iii_renuncia"]["parque"] = "Parque";
fieldAliasList2["v_pol_a_iii_renuncia"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_a_iii_renuncia"]["titular"] = "Titular";
fieldAliasList2["v_pol_a_iii_renuncia"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_a_iii_renuncia"]["estado"] = "Estado";
fieldAliasList2["v_pol_a_iii_renuncia"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_a_ii_sin_admitir"] = new Array();
fieldAliasList2["v_pol_a_ii_sin_admitir"]["parque"] = "Parque";
fieldAliasList2["v_pol_a_ii_sin_admitir"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_a_ii_sin_admitir"]["titular"] = "Titular";
fieldAliasList2["v_pol_a_ii_sin_admitir"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_a_ii_sin_admitir"]["estado"] = "Estado";
fieldAliasList2["v_pol_a_ii_sin_admitir"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_a_iii_sin_admitir"] = new Array();
fieldAliasList2["v_pol_a_iii_sin_admitir"]["parque"] = "Parque";
fieldAliasList2["v_pol_a_iii_sin_admitir"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_a_iii_sin_admitir"]["titular"] = "Titular";
fieldAliasList2["v_pol_a_iii_sin_admitir"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_a_iii_sin_admitir"]["estado"] = "Estado";
fieldAliasList2["v_pol_a_iii_sin_admitir"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_rg_sin_admitir"] = new Array();
fieldAliasList2["v_pol_rg_sin_admitir"]["parque"] = "Parque";
fieldAliasList2["v_pol_rg_sin_admitir"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_rg_sin_admitir"]["titular"] = "Titular";
fieldAliasList2["v_pol_rg_sin_admitir"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_rg_sin_admitir"]["estado"] = "Estado";
fieldAliasList2["v_pol_rg_sin_admitir"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_dt_ii_sin_admitir"] = new Array();
fieldAliasList2["v_pol_dt_ii_sin_admitir"]["parque"] = "Parque";
fieldAliasList2["v_pol_dt_ii_sin_admitir"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_dt_ii_sin_admitir"]["titular"] = "Titular";
fieldAliasList2["v_pol_dt_ii_sin_admitir"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_dt_ii_sin_admitir"]["estado"] = "Estado";
fieldAliasList2["v_pol_dt_ii_sin_admitir"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_proteg_a_ii"] = new Array();
fieldAliasList2["v_pol_proteg_a_ii"]["parque"] = "Parque";
fieldAliasList2["v_pol_proteg_a_ii"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_proteg_a_ii"]["titular"] = "Titular";
fieldAliasList2["v_pol_proteg_a_ii"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_proteg_a_ii"]["estado"] = "Estado";
fieldAliasList2["v_pol_proteg_a_ii"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_proteg_a_iii"] = new Array();
fieldAliasList2["v_pol_proteg_a_iii"]["parque"] = "Parque";
fieldAliasList2["v_pol_proteg_a_iii"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_proteg_a_iii"]["titular"] = "Titular";
fieldAliasList2["v_pol_proteg_a_iii"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_proteg_a_iii"]["estado"] = "Estado";
fieldAliasList2["v_pol_proteg_a_iii"]["origen_proteccion"] = "Origen de la protección";

fieldAliasList2["v_pol_proteg_a_y_c"] = new Array();
fieldAliasList2["v_pol_proteg_a_y_c"]["parque"] = "Parque";
fieldAliasList2["v_pol_proteg_a_y_c"]["potencia"] = "Potencia (MW)";
fieldAliasList2["v_pol_proteg_a_y_c"]["titular"] = "Titular";
fieldAliasList2["v_pol_proteg_a_y_c"]["procedencia"] = "Procedencia";
fieldAliasList2["v_pol_proteg_a_y_c"]["estado"] = "Estado";
fieldAliasList2["v_pol_proteg_a_y_c"]["origen_proteccion"] = "Origen de la protección";


/*// ANEXOIII_DL_2_2016
fieldAliasList2["ANEXOIII_DL_2_2016"] = new Array();
fieldAliasList2["ANEXOIII_DL_2_2016"]["parque"] = "PARQUE";

// A_II_DL_2_2016_V
fieldAliasList2["A_II_DL_2_2016_V"] = new Array();
fieldAliasList2["A_II_DL_2_2016_V"]["vertice"] = "VERTICE";

// A_III_DL_2_2016_V
fieldAliasList2["A_III_DL_2_2016_V"] = new Array();
fieldAliasList2["A_III_DL_2_2016_V"]["vertice"] = "VERTICE";*/

	// Gestores de Residuos //no esta creada
fieldAliasList2["GesRes"] = new Array();
fieldAliasList2["GesRes"]["nima"] = "NIMA";
fieldAliasList2["GesRes"]["nombre_empresa"] = "Nombre del Gestor";
fieldAliasList2["GesRes"]["nombre_centro"] = "Nombre del Centro";
fieldAliasList2["GesRes"]["localidad"] = "Localidad";
fieldAliasList2["GesRes"]["direccion"] = "Dirección";

// EDAR //no esta creada
fieldAliasList2["EDAR"] = new Array();
fieldAliasList2["EDAR"]["codedar"] = "Codigo";
fieldAliasList2["EDAR"]["nombre_eda"] = "Nombre Depuradora";
fieldAliasList2["EDAR"]["fecha_ser"] = "Fecha de Servicio";
fieldAliasList2["EDAR"]["destino_ef"] = "Destino de Vertido";

// Terrenos cinegéticos
fieldAliasList2["VRTCC1"] = new Array();
fieldAliasList2["VRTCC1"]["nregis"] = "Número de Registro";
fieldAliasList2["VRTCC1"]["matricula"] = "Número de Matrícula";
fieldAliasList2["VRTCC1"]["labels"] = "Matrícula";
fieldAliasList2["VRTCC1"]["nombre"] = "Nombre";
fieldAliasList2["VRTCC1"]["titular"] = "Titular";
fieldAliasList2["VRTCC1"]["dtipo"] = "Tipo de Terreno Cinegético";
fieldAliasList2["VRTCC1"]["hsuperf"] = "Superficie Oficial";
fieldAliasList2["VRTCC1"]["daproch"] = "Aprovechamiento cinegético principal";
fieldAliasList2["VRTCC1"]["daprochsec"] = "Aprovechamiento cinegético secundario";

fieldAliasList2["VRTCC1"]["hsuperf"] = "Superficie oficial";

// Cotos de Pesca //no esta creada
fieldAliasList2["COTOSPESCA"] = new Array();
fieldAliasList2["COTOSPESCA"]["cmatcoto"] = "Matrícula";
fieldAliasList2["COTOSPESCA"]["dtiptramo"] = "Tipo de Tramo";
fieldAliasList2["COTOSPESCA"]["dsubtiptramo"] = "Subtipo de Tramo";
fieldAliasList2["COTOSPESCA"]["dmasa"] = "Masa de agua";



// Montes
fieldAliasList2["MONTES"] = new Array();
//fieldAliasList2["MONTES"]["nregistro"] = "Número de Registro";
fieldAliasList2["MONTES"]["labels"] = "Matrícula";
fieldAliasList2["MONTES"]["tipo"] = "Tipo de Monte";
fieldAliasList2["MONTES"]["denominacion"] = "Denominación";
fieldAliasList2["MONTES"]["titular"] = "Titular del Monte";
fieldAliasList2["MONTES"]["origen"] = "Origen";

// Consorcios de Repoblación forestal
fieldAliasList2["CONSREP"] = new Array();
//fieldAliasList2["CONSREP"]["nregistro"] = "Número de Registro";
fieldAliasList2["CONSREP"]["mat_siaf"] = "Matrícula";
//fieldAliasList2["CONSREP"]["tipo"] = "Tipo";
fieldAliasList2["CONSREP"]["dconsor"] = "Denominación";
//fieldAliasList2["CONSREP"]["titular"] = "Titular beneficiario";
fieldAliasList2["CONSREP"]["corigen"] = "Origen";

// OCAS
fieldAliasList2["OCAS"] = new Array();
fieldAliasList2["OCAS"]["denominaci"] = "OCA";

// AMAS
fieldAliasList2["AMAS"] = new Array();
fieldAliasList2["AMAS"]["denominaci"] = "AMA";

//AM.AirQualityManagementZone
fieldAliasList2["AM.AirQualityManagementZone"] = new Array();
fieldAliasList2["AM.AirQualityManagementZone"]["localId"] = "Inspire - LocalId";
fieldAliasList2["AM.AirQualityManagementZone"]["namespace"] = "Inspire - Namespace";
fieldAliasList2["AM.AirQualityManagementZone"]["text"] = "Inspire - Text";
fieldAliasList2["AM.AirQualityManagementZone"]["zoneType"] = "Inspire - ZoneType";
fieldAliasList2["AM.AirQualityManagementZone"]["specializedZoneType"] = "Inspire - SpecialisedZoneType";
fieldAliasList2["AM.AirQualityManagementZone"]["designationPeriodBegin"] = "Inspire - DesignationPeriodBegin";
fieldAliasList2["AM.AirQualityManagementZone"]["designationPeriodEnd"] = "Inspire - DesignationPeriodEnd";
fieldAliasList2["AM.AirQualityManagementZone"]["environmentalDomain"] = "Inspire - EnvironmentalDomain";
fieldAliasList2["AM.AirQualityManagementZone"]["dateEnteredIntoForce"] = "Inspire - DateEnteredIntoForce";
fieldAliasList2["AM.AirQualityManagementZone"]["dateRepealed"] = "Inspire - DateRepealed";
fieldAliasList2["AM.AirQualityManagementZone"]["link"] = "Inspire - Link";

//AM.RestrictedZonesAroundContaminatedSites
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"] = new Array();
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["localId"] = "Inspire - LocalId";
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["namespace"] = "Inspire - Namespace";
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["text"] = "Inspire - Text";
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["zoneType"] = "Inspire - ZoneType";
//fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["specialisedZoneType"] = "Inspire - SpecialisedZoneType";
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["designationPeriodBegin"] = "Inspire - DesignationPeriodBegin";
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["designationPeriodEnd"] = "Inspire - DesignationPeriodEnd";
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["environmentalDomain"] = "Inspire - EnvironmentalDomain";
//fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["dateEnteredIntoForce"] = "Inspire - DateEnteredIntoForce";
//fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["dateRepealed"] = "Inspire - DateRepealed";
fieldAliasList2["AM.RestrictedZonesAroundContaminatedSites"]["link"] = "Inspire - Link";


// Vías pecuarias
fieldAliasList2["VVVPP_R_GEN"] = new Array();
//fieldAliasList2["VVVPP_R_GEN"]["matricula"] = "Matrícula";
//fieldAliasList2["VVVPP_R_GEN"]["nregistro"] = "Número de Registro";
//fieldAliasList2["VVVPP_R_GEN"]["codigo"] = "Código";
fieldAliasList2["VVVPP_R_GEN"]["codclasifi"] = "Códifo de Clasificación";
fieldAliasList2["VVVPP_R_GEN"]["dtipvia"] = "Tipo";
fieldAliasList2["VVVPP_R_GEN"]["nombre_via"] = "Denominación";

// Árboles singulares
fieldAliasList2["ARBOLESSINGULARES_ES24"] = new Array();
fieldAliasList2["ARBOLESSINGULARES_ES24"]["sitecode"] = "Inspire - SiteCode";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["sitename"] = "Inspire - SiteName";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["inspireid"] = "Inspire - InspireId";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["legalfoundationdate"] = "Inspire - LegalFoundationDate";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["legalfoundationdocument"] = "Inspire - LegalFoundationDocument";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["sitedesignation"] = "Inspire - SiteDesignation";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["siteprotectionclassification"] = "Inspire - SiteProtectionClassification";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["designationscheme"] = "Inspire - DesignationScheme";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["designation"] = "Inspire - Designation";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["percentageunderdesignation"] = "Inspire - PercentageUnderDesignation";
fieldAliasList2["ARBOLESSINGULARES_ES24"]["codeara"] = "Inspire - CodeAra";
	
// Reserva de la Biosfera MaB_ES24
fieldAliasList2["MaB_ES24"] = new Array();
fieldAliasList2["MaB_ES24"]["sitecode"] = "Inspire - SiteCode";
fieldAliasList2["MaB_ES24"]["sitename"] = "Inspire - SiteName";
fieldAliasList2["MaB_ES24"]["inspireid"] = "Inspire - InspireId";
fieldAliasList2["MaB_ES24"]["legalfoundationdate"] = "Inspire - LegalFoundationDate";
fieldAliasList2["MaB_ES24"]["legalfoundationdocument"] = "Inspire - LegalFoundationDocument";
fieldAliasList2["MaB_ES24"]["sitedesignation"] = "Inspire - SiteDesignation";
fieldAliasList2["MaB_ES24"]["siteprotectionclassification"] = "Inspire - SiteProtectionClassification";
fieldAliasList2["MaB_ES24"]["designationscheme"] = "Inspire - DesignationScheme";
fieldAliasList2["MaB_ES24"]["designation"] = "Inspire - Designation";
fieldAliasList2["MaB_ES24"]["percentageunderdesignation"] = "Inspire - PercentageUnderDesignation";
fieldAliasList2["MaB_ES24"]["code24"] = "Inspire - Code24";
fieldAliasList2["MaB_ES24"]["codeara"] = "Inspire - CodeAra";

// Lugares de Interés Geológico
fieldAliasList2["LIG_ES24"] = new Array();
fieldAliasList2["LIG_ES24"]["codigo"] = "Matrícula";
fieldAliasList2["LIG_ES24"]["ctipfigura"] = "Tipo de Figura";
fieldAliasList2["LIG_ES24"]["csubtip"] = "Subtipo de Figura";
fieldAliasList2["LIG_ES24"]["descripcio"] = "Denominación";
fieldAliasList2["LIG_ES24"]["sitecode"] = "Inspire - SiteCode";
fieldAliasList2["LIG_ES24"]["sitename"] = "Inspire - SiteName";
fieldAliasList2["LIG_ES24"]["inspireid"] = "Inspire - InspireId";
fieldAliasList2["LIG_ES24"]["legalfoundationdate"] = "Inspire - LegalFoundationDate";
fieldAliasList2["LIG_ES24"]["legalfoundationdocument"] = "Inspire - LegalFoundationDocument";
fieldAliasList2["LIG_ES24"]["sitedesignation"] = "Inspire - SiteDesignation";
fieldAliasList2["LIG_ES24"]["siteprotectionclassification"] = "Inspire - SiteProtectionClassification";
fieldAliasList2["LIG_ES24"]["percentageunderdesignation"] = "Inspire - PercentageUnderDesignation";
fieldAliasList2["LIG_ES24"]["code24"] = "Inspire - Code24";
fieldAliasList2["LIG_ES24"]["codeara"] = "Inspire - CodeAra";

// Fecha Vuelo Aragon Foto
fieldAliasList2["FechaVueloRec"] = new Array();
fieldAliasList2["FechaVueloRec"]["fechavuelo"] = "Fecha de vuelo de Aragón Foto";
fieldAliasList2["FechaVueloRec"]["denomina"] = "Nombre del Plan";
fieldAliasList2["FechaVueloRec"]["tam_pixel"] = "Tamaño de píxel (m.)";

// Fecha Vuelo Series de Ortofotos
fieldAliasList2["FechaVueloSer"] = new Array();
fieldAliasList2["FechaVueloSer"]["fechavuelo"] = "Fecha de vuelo de Aragón Foto";
fieldAliasList2["FechaVueloSer"]["denomina"] = "Nombre del Plan";
fieldAliasList2["FechaVueloSer"]["tam_pixel"] = "Tamaño de píxel (m.)";

// Salud Tipo de Centro | Hospital
fieldAliasList2["Cen_Salud"] = new Array();
fieldAliasList2["Cen_Salud"]["nombre"] = "Nombre";
fieldAliasList2["Cen_Salud"]["nomsector"] = "Sector Sanitario";
fieldAliasList2["Cen_Salud"]["sector"] = "Número de Sector Sanitario";

fieldAliasList2["v206_hospitales"] = new Array();
fieldAliasList2["v206_hospitales"]["nombre"] = "Nombre";
fieldAliasList2["v206_hospitales"]["direccion"] = "Direccion";
fieldAliasList2["v206_hospitales"]["municipio"] = "Municipio";
//fieldAliasList2["v206_hospitales"]["depfun"] = "Titularidad";
//fieldAliasList2["v206_hospitales"]["regcess"] = "Registro";

fieldAliasList2["v206_consultorios"] = new Array();
fieldAliasList2["v206_consultorios"]["nombre_centro"] = "Nombre";
fieldAliasList2["v206_consultorios"]["area_sector"] = "Área";
fieldAliasList2["v206_consultorios"]["municipio"] = "Municipio";
fieldAliasList2["v206_consultorios"]["tipo_via"] = "Tipo de vía";
fieldAliasList2["v206_consultorios"]["nombre_via"] = "Dirección";
fieldAliasList2["v206_consultorios"]["numero_via"] = "Portal";


fieldAliasList2["v206_centros_salud"] = new Array();
fieldAliasList2["v206_centros_salud"]["nombre"] = "Nombre de Centro Sanitario";
fieldAliasList2["v206_centros_salud"]["zona_basic"] = "Zona Básica de Salud";
fieldAliasList2["v206_centros_salud"]["localidad"] = "Localidad";
fieldAliasList2["v206_centros_salud"]["direccion"] = "Dirección";
//fieldAliasList2["v206_centros_salud"]["area"] = "Área de Salud";


fieldAliasList2["zonas_salud"] = new Array();
fieldAliasList2["zonas_salud"]["nombre"] = "Nombre de Zona Básica de Salud";
fieldAliasList2["zonas_salud"]["codigo"] = "Número de Zona Básica de Salud";
fieldAliasList2["zonas_salud"]["sector"] = "Sector Sanitario";
fieldAliasList2["zonas_salud"]["cod_sector"] = "Número de Sector Sanitario";


	// Centro educativo
fieldAliasList2["v206_educa_centros"] = new Array();
fieldAliasList2["v206_educa_centros"]["nombre_cen"] = "Nombre del Centro";
fieldAliasList2["v206_educa_centros"]["tipo_centr"] = "Tipo de Centro";
fieldAliasList2["v206_educa_centros"]["naturaleza"] = "Titularidad del Centro";
fieldAliasList2["v206_educa_centros"]["localidad"] = "Localidad";
fieldAliasList2["v206_educa_centros"]["direccion"] = "Direccion";
fieldAliasList2["v206_educa_centros"]["codpostal"] = "Codigo Postal";
fieldAliasList2["v206_educa_centros"]["telefono"] = "Telefono";
fieldAliasList2["v206_educa_centros"]["mail"] = "Correo electronico";
fieldAliasList2["v206_educa_centros"]["fax"] = "Fax";
fieldAliasList2["v206_educa_centros"]["web"] = "Pagina Web";

	// Zona educativa
fieldAliasList2["Zon_Educa"] = new Array();
fieldAliasList2["Zon_Educa"]["nzona"] = "Número de Zona";
fieldAliasList2["Zon_Educa"]["inf_pri"] = "Zona Infantil y Primaria";
fieldAliasList2["Zon_Educa"]["eso"] = "Zona ESO";
fieldAliasList2["Zon_Educa"]["bach"] = "Zona Bachillerato";

 // Troides
fieldAliasList2["Troides"] = new Array();
fieldAliasList2["Troides"]["denomina"] = "Nombre";
fieldAliasList2["Troides"]["refcat"] = "Referencia Catastral";
fieldAliasList2["Troides"]["x"] = "Coordenada X";
fieldAliasList2["Troides"]["y"] = "Coordenada Y";

	// Localidad
fieldAliasList2["Localidad"] = new Array();
fieldAliasList2["Localidad"]["d_nucleo_i"] = "Nombre";
fieldAliasList2["Localidad"]["c_nucleo_i"] = "Código INE";

	// OCIC
fieldAliasList2["OCIC"] = new Array();
fieldAliasList2["OCIC"]["texto"] = "Nombre de la oficina";
fieldAliasList2["OCIC"]["tipo"] = "Tipo";

 // TroidesV
fieldAliasList2["TroidesV"] = new Array();
fieldAliasList2["TroidesV"]["denomina"] = "Nombre";
fieldAliasList2["TroidesV"]["c_mun_via"] = "Código de vía";
fieldAliasList2["TroidesV"]["numero"] = "Número de vía";

	// BusCallUrb
fieldAliasList2["BUSCALLURB"] = new Array();
fieldAliasList2["BUSCALLURB"]["nomvia"] = "Vía";
fieldAliasList2["BUSCALLURB"]["sigla"] = "Tipo de Vía";
fieldAliasList2["BUSCALLURB"]["buscalo"] = "Nombre de Vía";
fieldAliasList2["BUSCALLURB"]["localidad"] = "Localidad";
fieldAliasList2["BUSCALLURB"]["d_mun_ine"] = "Municipio";
fieldAliasList2["BUSCALLURB"]["mun_via"] = "Código de vía";

//fieldAliasList2["BusCallUrb"] = new Array();
//fieldAliasList2["BusCallUrb"]["nomvia"] = "Vía";
//fieldAliasList2["BusCallUrb"]["sigla"] = "Tipo de Vía";
//fieldAliasList2["BusCallUrb"]["buscalo"] = "Nombre de Vía";
//fieldAliasList2["BusCallUrb"]["localidad"] = "Localidad";
//fieldAliasList2["BusCallUrb"]["d_mun_ine"] = "Municipio";
//fieldAliasList2["BusCallUrb"]["mun_via"] = "Código de vía";

// Toponimo
fieldAliasList2["Toponimo"] = new Array();
fieldAliasList2["Toponimo"]["nombre"] = "Nombre";
fieldAliasList2["Toponimo"]["d_muni_ine"] = "Municipio";
fieldAliasList2["Toponimo"]["referencia"] = "Referencia";
fieldAliasList2["Toponimo"]["fuente"] = "Fuente";
fieldAliasList2["Toponimo"]["escala"] = "Escala";

	// Provincia 
fieldAliasList2["Provincia"] = new Array();
fieldAliasList2["Provincia"]["provincia"] = "Nombre";
fieldAliasList2["Provincia"]["uri_aod"] = "Enlace a Aragón Open Data";

	// Comarca 
fieldAliasList2["Comarca"] = new Array();
fieldAliasList2["Comarca"]["d_comarca"] = "Nombre";
fieldAliasList2["Comarca"]["uri_aod"] = "Enlace a Aragón Open Data";

	// Municipio 
fieldAliasList2["Municipio"] = new Array();
fieldAliasList2["Municipio"]["d_muni_ine"] = "Denominación";
fieldAliasList2["Municipio"]["c_muni_ine"] = "Código INE";
fieldAliasList2["Municipio"]["d_comarca"] = "Comarca a la que pertenece";
fieldAliasList2["Municipio"]["i_iaest"] = "Ficha con datos estadísticos (IAEST)";
//fieldAliasList2["Municipio"]["uri_aod"] = "Enlace a Aragón Open Data";

	// BusMun
fieldAliasList2["BusMun"] = new Array();
fieldAliasList2["BusMun"]["d_muni_ine"] = "Denominación";
fieldAliasList2["BusMun"]["c_muni_ine"] = "Código INE";
fieldAliasList2["BusMun"]["d_comarca"] = "Comarca a la que pertenece";
fieldAliasList2["BusMun"]["i_iaest"] = "Ficha con datos estadísticos (IAEST)";
fieldAliasList2["BusMun"]["uri_aod"] = "Enlace a Aragón Open Data";


// Industria
fieldAliasList2["SIA"] = new Array();
fieldAliasList2["SIA"]["nombre"] = "Nombre";
fieldAliasList2["SIA"]["promotor"] = "Promotor";
fieldAliasList2["SIA"]["suptotm2"] = "Superficie Total en m2";
fieldAliasList2["SIA"]["porocu"] = "Porcentaje de ocupacion";
fieldAliasList2["SIA"]["url"] = "Acceso a la ficha del Suelo Industrial";

	// Minas 
fieldAliasList2["CatMin"] = new Array();
fieldAliasList2["CatMin"]["nombre"] = "Nombre";
fieldAliasList2["CatMin"]["copr"] = "Código de Provincia";
fieldAliasList2["CatMin"]["nreg"] = "Número de registro";
fieldAliasList2["CatMin"]["d_tipo"] = "Tipo de Expediente";
fieldAliasList2["CatMin"]["d_estado"] = "Estado de Tramitación";

	// Red de carreteras 1:300000
fieldAliasList2["Carretera"] = new Array();
fieldAliasList2["Carretera"]["codigo"] = "Denominación";

	// Ferrocarril
fieldAliasList2["Ferrocarril"] = new Array();
fieldAliasList2["Ferrocarril"]["tipo"] = "Tipo";

	// Rios 1:300000
fieldAliasList2["Rio"] = new Array();
fieldAliasList2["Rio"]["rio"] = "Río";

	// Espacios Naturales Protegidos
fieldAliasList2["ENP_ES24"] = new Array();
fieldAliasList2["ENP_ES24"]["codigo"] = "Matrícula";
fieldAliasList2["ENP_ES24"]["ctipfigura"] = "Tipo de Figura";
fieldAliasList2["ENP_ES24"]["csubtip"] = "Subtipo de Figura";
fieldAliasList2["ENP_ES24"]["descripcio"] = "Denominación";
fieldAliasList2["ENP_ES24"]["sitecode"] = "Inspire - SiteCode";
fieldAliasList2["ENP_ES24"]["sitename"] = "Inspire - SiteName";
fieldAliasList2["ENP_ES24"]["inspireid"] = "Inspire - InspireId";
fieldAliasList2["ENP_ES24"]["legalfoundationdate"] = "Inspire - LegalFoundationDate";
fieldAliasList2["ENP_ES24"]["legalfoundationdocument"] = "Inspire - LegalFoundationDocument";
fieldAliasList2["ENP_ES24"]["sitedesignation"] = "Inspire - SiteDesignation";
fieldAliasList2["ENP_ES24"]["siteprotectionclassification"] = "Inspire - SiteProtectionClassification";
fieldAliasList2["ENP_ES24"]["percentageunderdesignation"] = "Inspire - PercentageUnderDesignation";
//fieldAliasList2["ENP_ES24"]["code24"] = "Inspire - Code24";
fieldAliasList2["ENP_ES24"]["codeara"] = "Inspire - CodeAra";

	// Zona Espacios Naturales Protegidos
fieldAliasList2["ZENP"] = new Array();
fieldAliasList2["ZENP"]["enpassociated"] = "Espacio Natural Protegido";
fieldAliasList2["ZENP"]["zonetype"] = "Tipo de Zona";
fieldAliasList2["ZENP"]["zonename"] = "Nombre de la Zona";
fieldAliasList2["ZENP"]["planificationzone"] = "Zona de Planificación";


	// Borderea chouardii
fieldAliasList2["BorCho"] = new Array();
fieldAliasList2["BorCho"]["descripcio"] = "Denominación";

	// Cypripedium calceolus
fieldAliasList2["CypCal"] = fieldAliasList2["BorCho"];
	
	// Falco Naumanni
fieldAliasList2["FalNau"] = fieldAliasList2["BorCho"]; 

	// Gypaetus Barbatus
fieldAliasList2["GypBar"] = fieldAliasList2["BorCho"];
	
	// Vella pseudocytisus
fieldAliasList2["VelPse"] = fieldAliasList2["BorCho"];
	
	// Krascheninnikovia ceratoides
fieldAliasList2["KraCer"] = fieldAliasList2["BorCho"];

  //AusPal
fieldAliasList2["AusPal"] = fieldAliasList2["BorCho"];
 
 //MarAur
fieldAliasList2["MarAur"] = fieldAliasList2["BorCho"];

	// Areas Criticas Proteccion Especies Amenazadas
fieldAliasList2["APPE"] = new Array();
fieldAliasList2["APPE"]["descripcio"] = "Denominación";

	// Areas Criticas Proteccion Especies Amenazadas
fieldAliasList2["ACRIT"] = new Array();
fieldAliasList2["ACRIT"]["dzona"] = "Denominación";

	// Habitats de Interes Comunitario
fieldAliasList2["HIC"] = new Array();
fieldAliasList2["HIC"]["descripcio"] = "Denominación";
fieldAliasList2["HIC"]["multi"] = "Código";
fieldAliasList2["HIC"]["valorgloba"] = "Valor global";

	// Cuadrícula fauna 10km
fieldAliasList2["VFAUNA10"] = new Array();
fieldAliasList2["VFAUNA10"]["cuadricula"] = "Cuadrícula";
fieldAliasList2["VFAUNA10"]["despecie"] = "Especie";
fieldAliasList2["VFAUNA10"]["dvulgar"] = "Nombre vulgar";
//fieldAliasList2["VFAUNA10"]["valorgloba"] = "Valor global";

	// Cuadrícula flora 10km
fieldAliasList2["VFLORA10"] = new Array();
fieldAliasList2["VFLORA10"]["cuadricula"] = "Cuadrícula";
fieldAliasList2["VFLORA10"]["despecie"] = "Especie";
fieldAliasList2["VFLORA10"]["dvulgar"] = "Nombre vulgar";
//fieldAliasList2["VFLORA10"]["valorgloba"] = "Valor global";

	// Cuadrícula flora 1km
fieldAliasList2["VFLORA1"] = new Array();
fieldAliasList2["VFLORA1"]["cuadricula"] = "Cuadrícula";
fieldAliasList2["VFLORA1"]["despecie"] = "Especie";
fieldAliasList2["VFLORA1"]["dvulgar"] = "Nombre vulgar";
//fieldAliasList2["VFLORA1"]["valorgloba"] = "Valor global";

	// HMD
fieldAliasList2["HMD"] = new Array();
fieldAliasList2["HMD"]["codigo"] = "Matrícula";
fieldAliasList2["HMD"]["cod_rn"] = "Código";
fieldAliasList2["HMD"]["descripcio"] = "Denominación";	  
	
	// PORN
fieldAliasList2["PORN_ES24"] = new Array();
fieldAliasList2["PORN_ES24"]["codigo"] = "Matrícula";
fieldAliasList2["PORN_ES24"]["cestado"] = "Estado";
fieldAliasList2["PORN_ES24"]["descripcio"] = "Denominación";

	// ZPORN
fieldAliasList2["ZPORNs"] = new Array();
fieldAliasList2["ZPORNs"]["porn"] = "Nombre de la zona";
fieldAliasList2["ZPORNs"]["ctipfigura"] = "Tipo de Figura";
	
	// ZEPA_ES24
fieldAliasList2["ZEPA_ES24"] = new Array();
fieldAliasList2["ZEPA_ES24"]["sitecode"] = "Inspire - SiteCode";
fieldAliasList2["ZEPA_ES24"]["sitename"] = "Inspire - SiteName";
fieldAliasList2["ZEPA_ES24"]["inspireid"] = "Inspire - InspireId";
fieldAliasList2["ZEPA_ES24"]["legalfoundationdate"] = "Inspire - LegalFoundationDate";
fieldAliasList2["ZEPA_ES24"]["legalfoundationdocument"] = "Inspire - LegalFoundationDocument";
fieldAliasList2["ZEPA_ES24"]["sitedesignation"] = "Inspire - SiteDesignation";
fieldAliasList2["ZEPA_ES24"]["siteprotectionclassification"] = "Inspire - SiteProtectionClassification";
fieldAliasList2["ZEPA_ES24"]["designationscheme"] = "Inspire - DesignationScheme";
fieldAliasList2["ZEPA_ES24"]["designation"] = "Inspire - Designation";
fieldAliasList2["ZEPA_ES24"]["percentageunderdesignation"] = "Inspire - PercentageUnderDesignation";
fieldAliasList2["ZEPA_ES24"]["code24"] = "Inspire - Code24";
fieldAliasList2["ZEPA_ES24"]["codeara"] = "Inspire - CodeAra";
	
	// LIC_ES24
fieldAliasList2["LIC_ES24"] = new Array();
fieldAliasList2["LIC_ES24"]["sitecode"] = "Inspire - SiteCode";
fieldAliasList2["LIC_ES24"]["sitename"] = "Inspire - SiteName";
fieldAliasList2["LIC_ES24"]["inspireid"] = "Inspire - InspireId";
fieldAliasList2["LIC_ES24"]["legalfoundationdate"] = "Inspire - LegalFoundationDate";
fieldAliasList2["LIC_ES24"]["legalfoundationdocument"] = "Inspire - LegalFoundationDocument";
fieldAliasList2["LIC_ES24"]["sitedesignation"] = "Inspire - SiteDesignation";
fieldAliasList2["LIC_ES24"]["siteprotectionclassification"] = "Inspire - SiteProtectionClassification";
fieldAliasList2["LIC_ES24"]["designationscheme"] = "Inspire - DesignationScheme";
fieldAliasList2["LIC_ES24"]["designation"] = "Inspire - Designation";
fieldAliasList2["LIC_ES24"]["percentageunderdesignation"] = "Inspire - PercentageUnderDesignation";
fieldAliasList2["LIC_ES24"]["code24"] = "Inspire - Code24";
fieldAliasList2["LIC_ES24"]["codeara"] = "Inspire - CodeAra";

// Hojas 50K
fieldAliasList2["Cuad50k"] = new Array();
fieldAliasList2["Cuad50k"]["name_hoja"] = "Hoja 1:50.000 Nº";

// Cuadrícula UTM 10K
fieldAliasList2["CuaUTM10k"] = new Array();
fieldAliasList2["CuaUTM10k"]["codigo"] = "Cuadrícula UTM 10 Km. Nº";

// Aluviales
fieldAliasList2["T0212_ALU"] = new Array();
fieldAliasList2["T0212_ALU"]["riesgo_che"] = "Susceptibilidad";

// Colapsos
fieldAliasList2["T0212_COL"] = new Array();
fieldAliasList2["T0212_COL"]["riesgo"] = "Susceptibilidad";

// Deslizamientos
fieldAliasList2["T0212_DES"] = new Array();
fieldAliasList2["T0212_DES"]["peligrosid"] = "Susceptibilidad";

// Inundaciones
fieldAliasList2["T0212_INU"] = new Array();
fieldAliasList2["T0212_INU"]["peligrosid"] = "Susceptibilidad";

// Vientos
fieldAliasList2["T0212_VIE_I"] = new Array();
fieldAliasList2["T0212_VIE_I"]["riesgo"] = "Susceptibilidad";

fieldAliasList2["zar_incendio"] = new Array();
fieldAliasList2["zar_incendio"]["tipo_riesgo"] = "Peligrosidad";

// Metodologia Mapas de Susceptibilidad
fieldAliasList2["MetodologiaSusceptibilidad"] = new Array();
fieldAliasList2["MetodologiaSusceptibilidad"]["url_ms_mt"] = "Metodología de elaboración de Mapas de Susceptibilidad";

// Señaletica SET
fieldAliasList2["V_C_SSTA"] = new Array();
fieldAliasList2["V_C_SSTA"]["tipo"] = "Tipo";
fieldAliasList2["V_C_SSTA"]["nombre"] = "Nombre";
fieldAliasList2["V_C_SSTA"]["etapa"] = "Etapa";
fieldAliasList2["V_C_SSTA"]["num_indicad"] = "Número de indicadores direccionales";
fieldAliasList2["V_C_SSTA"]["x_utm"] = "Coordenada X (ETRS89 H30N)";
fieldAliasList2["V_C_SSTA"]["y_utm"] = "Coordenada Y (ETRS89 H30N)";

// SET
fieldAliasList2["V_C_STA"] = new Array();
fieldAliasList2["V_C_STA"]["tipo"] = "Tipo";
fieldAliasList2["V_C_STA"]["nombre"] = "Nombre";
fieldAliasList2["V_C_STA"]["itinerario"] = "Itinerario";
fieldAliasList2["V_C_STA"]["tramo"] = "Tramo";
fieldAliasList2["V_C_STA"]["ayto"] = "Ayuntamiento";
fieldAliasList2["V_C_STA"]["long_"] = "Longitud";

// Miradores
fieldAliasList2["MA_SET"] = new Array();
fieldAliasList2["MA_SET"]["nombre"] = "Nombre";
fieldAliasList2["MA_SET"]["tipo"] = "Tipo";

// Senderos
fieldAliasList2["SA_SET"] = new Array();
fieldAliasList2["SA_SET"]["nombre"] = "Nombre";
fieldAliasList2["SA_SET"]["tipo"] = "Tipo";
fieldAliasList2["SA_SET"]["itinerario"] = "Itinerario";
fieldAliasList2["SA_SET"]["longitud"] = "Longitud (m.)";

// Corine 1990
fieldAliasList2["CLC1990_N3"] = new Array();
fieldAliasList2["CLC1990_N3"]["uso"] = "";

// Corine 2000
fieldAliasList2["CLC2000_N3"] = new Array();
fieldAliasList2["CLC2000_N3"]["uso"] = "";

// Corine 2006
fieldAliasList2["CLC2006_N3"] = new Array();
fieldAliasList2["CLC2006_N3"]["code_06"] = "";

// Corine 2012
fieldAliasList2["CLC2012_N3"] = new Array();
fieldAliasList2["CLC2012_N3"]["code_12"] = "";

// Figura de Planeamiento
fieldAliasList2["FPM_Info"] = new Array();
fieldAliasList2["FPM_Info"]["d_muni_ine"] = "Municipio";
fieldAliasList2["FPM_Info"]["d_comarca"] = "Comarca";
fieldAliasList2["FPM_Info"]["provincia"] = "Provincia";
fieldAliasList2["FPM_Info"]["figura"] = "Nombre de Figura";
fieldAliasList2["FPM_Info"]["tipo"] = "Figura";
fieldAliasList2["FPM_Info"]["anyo"] = "Año de Figura de Planeamiento";
fieldAliasList2["FPM_Info"]["archivo"] = "Enlace al Archivo SIUa";

// Informacion de Planeamiento
fieldAliasList2["URB_PLU"] = new Array();
fieldAliasList2["URB_PLU"]["cot"] = "Código o Nombre de Expediente";
fieldAliasList2["URB_PLU"]["date_acu"] = "Fecha de Acuerdo";
fieldAliasList2["URB_PLU"]["web_acu"] = "Enlace al BOA";
fieldAliasList2["URB_PLU"]["notepa"] = "Clase de Suelo";
fieldAliasList2["URB_PLU"]["uso_glob"] = "Uso Global o Tipo de Suelo No Urbanizable";

// Cultura
fieldAliasList2["CULTURA_PC"] = new Array();
fieldAliasList2["CULTURA_PC"]["denominaci"] = "Denominacion";

fieldAliasList2["v_bic_bienes_entornos"] = new Array();
fieldAliasList2["v_bic_bienes_entornos"]["denominaci"] = "Nombre";
fieldAliasList2["v_bic_bienes_entornos"]["clase"] = "Clase";
fieldAliasList2["v_bic_bienes_entornos"]["categoria"] = "Categoria";
fieldAliasList2["v_bic_bienes_entornos"]["publicacio"] = "Fecha Publicación BOA";
fieldAliasList2["v_bic_bienes_entornos"]["provincia"] = "Provincia";
fieldAliasList2["v_bic_bienes_entornos"]["comarca"] = "Comarca";
fieldAliasList2["v_bic_bienes_entornos"]["municipio"] = "Municipio";
fieldAliasList2["v_bic_bienes_entornos"]["localidad"] = "Localidad";

fieldAliasList2["v_bic_bienes_limites"] = new Array();
fieldAliasList2["v_bic_bienes_limites"]["denominaci"] = "Nombre";
fieldAliasList2["v_bic_bienes_limites"]["clase"] = "Clase";
fieldAliasList2["v_bic_bienes_limites"]["categoria"] = "Categoria";
fieldAliasList2["v_bic_bienes_limites"]["publicacio"] = "Fecha Publicación BOA";
fieldAliasList2["v_bic_bienes_limites"]["provincia"] = "Provincia";
fieldAliasList2["v_bic_bienes_limites"]["comarca"] = "Comarca";
fieldAliasList2["v_bic_bienes_limites"]["municipio"] = "Municipio";
fieldAliasList2["v_bic_bienes_limites"]["localidad"] = "Localidad";

fieldAliasList2["v_bic_bienes_puntos"] = new Array();
fieldAliasList2["v_bic_bienes_puntos"]["denominaci"] = "Nombre";
fieldAliasList2["v_bic_bienes_puntos"]["clase"] = "Clase";
fieldAliasList2["v_bic_bienes_puntos"]["categoria"] = "Categoria";
fieldAliasList2["v_bic_bienes_puntos"]["publicacio"] = "Fecha Publicación BOA";
fieldAliasList2["v_bic_bienes_puntos"]["provincia"] = "Provincia";
fieldAliasList2["v_bic_bienes_puntos"]["comarca"] = "Comarca";
fieldAliasList2["v_bic_bienes_puntos"]["municipio"] = "Municipio";
fieldAliasList2["v_bic_bienes_puntos"]["localidad"] = "Localidad";

// REGA
fieldAliasList2["REGA_EXPLOTACIONES"] = new Array();
fieldAliasList2["REGA_EXPLOTACIONES"]["explotacion"] = "Explotación";
fieldAliasList2["REGA_EXPLOTACIONES"]["especie"] = "Especie";
fieldAliasList2["REGA_EXPLOTACIONES"]["tipo"] = "Tipo";
fieldAliasList2["REGA_EXPLOTACIONES"]["autoconsumo"] = "Autoconsumo";

fieldAliasList2["REGA_FOCOS"] = new Array();
fieldAliasList2["REGA_FOCOS"]["explotacion"] = "Explotación";
fieldAliasList2["REGA_FOCOS"]["especie"] = "Especie";

fieldAliasList2["REGA_SERVIDUMBRES"] = new Array();
fieldAliasList2["REGA_SERVIDUMBRES"]["explotacion"] = "Explotación";
fieldAliasList2["REGA_SERVIDUMBRES"]["especie"] = "Especie";
fieldAliasList2["REGA_SERVIDUMBRES"]["tipo"] = "Tipo";
fieldAliasList2["REGA_SERVIDUMBRES"]["claszoo"] = "Clasificación zootécnica";
fieldAliasList2["REGA_SERVIDUMBRES"]["c_productiva"] = "Capacidad productiva";
fieldAliasList2["REGA_SERVIDUMBRES"]["capacidad"] = "Capacidad";

//Directrices Parciales Pirineos
fieldAliasList2["OTP"] = new Array();
fieldAliasList2["OTP"]["texto"] = "Directrices Parciales";
fieldAliasList2["OTP"]["f_ent_vig"] = "Fecha de Entrada en Vigor";
fieldAliasList2["OTP"]["url"] = "Enlace BOA";

// Directrices Parciales Matarranya
fieldAliasList2["OTM"] = new Array();
fieldAliasList2["OTM"]["texto"] = "Directrices Parciales";
fieldAliasList2["OTM"]["f_ent_vig"] = "Fecha de Entrada en Vigor";
fieldAliasList2["OTM"]["url"] = "Enlace BOA";

// Grandes Dominios de Paisaje 1:100.000
fieldAliasList2["MP_GDP"] = new Array();
fieldAliasList2["MP_GDP"]["dominios"] = "Gran Dominio de Paisaje";
fieldAliasList2["MP_GDP"]["abstract"] = "Resumen";

// P_CalidadRegional1M
fieldAliasList2["MP_CR1M"] = new Array();
fieldAliasList2["MP_CR1M"]["ic_reclas"] = "Calidad Regional (De Baja = 1 hasta Alta = 10)";

// P_AptitudHomogeneizada
fieldAliasList2["MP_PAH"] = new Array();
fieldAliasList2["MP_PAH"]["aptitud"] = "Aptitud Homogeneizada (De Muy Baja a Muy Alta)";

// P_CalidadHomogeneizada
fieldAliasList2["MP_PCH"] = new Array();
fieldAliasList2["MP_PCH"]["ic_up_final"] = "Calidad Homogeneizada (De Baja = 1 hasta Alta = 10)";

// P_FragilidadHomogeneizada
fieldAliasList2["MP_PFH"] = new Array();
fieldAliasList2["MP_PFH"]["if_upfinal"] = "Fragilidad Homogeneizada (De Baja = 1 hasta Alta = 5)";

// Regiones de Paisaje
fieldAliasList2["MP_TP"] = new Array();
fieldAliasList2["MP_TP"]["region"] = "Región de Paisaje";

// Unidades de Paisaje
fieldAliasList2["MP_UP"] = new Array();
fieldAliasList2["MP_UP"]["id_up"] = "Identificador de Unidad de Paisaje Comarcal";
fieldAliasList2["MP_UP"]["up"] = "Nombre de Unidad de Paisaje";
fieldAliasList2["MP_UP"]["macroup"] = "Macrounidad de Paisaje";
fieldAliasList2["MP_UP"]["mup"] = "Identificador de Macrounidad de Paisaje";
fieldAliasList2["MP_UP"]["calidad"] = "Calidad (De Baja = 1 hasta Alta = 10)";
fieldAliasList2["MP_UP"]["fragilidad"] = "Fragilidad (De Baja = 1 hasta Alta = 5)";
fieldAliasList2["MP_UP"]["region"] = "Region";

// Dominios de Paisaje Comarcal
fieldAliasList2["MP_DP"] = new Array();
fieldAliasList2["MP_DP"]["dominio"] = "Dominio de Paisaje a Escala Comarcal";
fieldAliasList2["MP_DP"]["dominio_lyr"] = "Dominio de Paisaje Homologado a Escala Regional";

// Unidades Fisiogeomorfologicas
fieldAliasList2["MP_UF"] = new Array();
fieldAliasList2["MP_UF"]["u_fisio"] = "Unidad Fisiogeomorfologica de Paisaje a Escala Comarcal";
fieldAliasList2["MP_UF"]["u_fisio_lyr_name"] = "Unidad Fisiogeomorfologica de Paisaje a Escala Regional";

// Vegetación y Usos del Suelo
fieldAliasList2["MP_VU"] = new Array();
fieldAliasList2["MP_VU"]["veg_niv1"] = "Nivel 1 de Vegetación y Usos del Suelo";
fieldAliasList2["MP_VU"]["veg_niv2"] = "Nivel 2 de Vegetación y Usos del Suelo";
fieldAliasList2["MP_VU"]["usos_n3"] = "Nivel 3 de Vegetación y Usos del Suelo";

// P_Recorridos
fieldAliasList2["MP_PR"] = new Array();
fieldAliasList2["MP_PR"]["codigo"] = "Codigo";
fieldAliasList2["MP_PR"]["tipo"] = "Tipo";
fieldAliasList2["MP_PR"]["c_comarca"] = "Codigo de Comarca";
fieldAliasList2["MP_PR"]["d_comarca"] = "Comarca";

// P_Miradores
fieldAliasList2["MP_PM"] = new Array();
fieldAliasList2["MP_PM"]["codigo"] = "Codigo";
fieldAliasList2["MP_PM"]["denominacion"] = "Denominacion";
fieldAliasList2["MP_PM"]["c_comarca"] = "Codigo de Comarca";
fieldAliasList2["MP_PM"]["D_COMARCA"] = "Comarca";;

// CONCEN_ZONAS
fieldAliasList2["CONCEN_ZONAS"] = new Array();
fieldAliasList2["CONCEN_ZONAS"]["codigo"] = "Código";
fieldAliasList2["CONCEN_ZONAS"]["completo"] = "Descripción";
fieldAliasList2["CONCEN_ZONAS"]["fecha_ini"] = "Fecha de inicio";
fieldAliasList2["CONCEN_ZONAS"]["fecha_fin"] = "Fecha de finalización";

//Parcelas
fieldAliasList2["v_parcela_concen"] = new Array();
fieldAliasList2["v_parcela_concen"]["codigo_zona"] = "Zona";
fieldAliasList2["v_parcela_concen"]["nombre_zona"] = "Nombre";
fieldAliasList2["v_parcela_concen"]["poligono"] = "Polígono";
fieldAliasList2["v_parcela_concen"]["masa"] = "Masa";
fieldAliasList2["v_parcela_concen"]["parcela_finca"] = "Parcela";
fieldAliasList2["v_parcela_concen"]["valor_puntos"] = "Valor en puntos";
fieldAliasList2["v_parcela_concen"]["area_m2"] = "Superficie en m2";
fieldAliasList2["v_parcela_concen"]["clase_1"] = "Clase 1";
fieldAliasList2["v_parcela_concen"]["clase_2"] = "Clase 2";
fieldAliasList2["v_parcela_concen"]["clase_3"] = "Clase 3";
fieldAliasList2["v_parcela_concen"]["clase_4"] = "Clase 4";
fieldAliasList2["v_parcela_concen"]["clase_5"] = "Clase 5";
fieldAliasList2["v_parcela_concen"]["clase_6"] = "Clase 6";
fieldAliasList2["v_parcela_concen"]["clase_7"] = "Clase 7";
fieldAliasList2["v_parcela_concen"]["clase_8"] = "Clase 8";
fieldAliasList2["v_parcela_concen"]["clase_9"] = "Clase 9";
fieldAliasList2["v_parcela_concen"]["clase_10"] = "Clase 10";
fieldAliasList2["v_parcela_concen"]["clase_11"] = "Clase 11";
fieldAliasList2["v_parcela_concen"]["clase_12"] = "Clase 12";

//Fincas
fieldAliasList2["v_finca_concen"] = new Array();
fieldAliasList2["v_finca_concen"]["codigo_zona"] = "Zona";
fieldAliasList2["v_finca_concen"]["nombre_zona"] = "Nombre";
fieldAliasList2["v_finca_concen"]["poligono"] = "Polígono";
fieldAliasList2["v_finca_concen"]["masa"] = "Masa";
fieldAliasList2["v_finca_concen"]["finca_finca"] = "Finca";
fieldAliasList2["v_finca_concen"]["valor_puntos"] = "Valor en puntos";
fieldAliasList2["v_finca_concen"]["area_m2"] = "Superficie en m2";
fieldAliasList2["v_finca_concen"]["clase_1"] = "Clase 1";
fieldAliasList2["v_finca_concen"]["clase_2"] = "Clase 2";
fieldAliasList2["v_finca_concen"]["clase_3"] = "Clase 3";
fieldAliasList2["v_finca_concen"]["clase_4"] = "Clase 4";
fieldAliasList2["v_finca_concen"]["clase_5"] = "Clase 5";
fieldAliasList2["v_finca_concen"]["clase_6"] = "Clase 6";
fieldAliasList2["v_finca_concen"]["clase_7"] = "Clase 7";
fieldAliasList2["v_finca_concen"]["clase_8"] = "Clase 8";
fieldAliasList2["v_finca_concen"]["clase_9"] = "Clase 9";
fieldAliasList2["v_finca_concen"]["clase_10"] = "Clase 10";
fieldAliasList2["v_finca_concen"]["clase_11"] = "Clase 11";
fieldAliasList2["v_finca_concen"]["clase_12"] = "Clase 12";

// INAGA EIP
fieldAliasList2["EIP"] = new Array();
fieldAliasList2["EIP"]["tipo_publicacion"] = "Tipo de publicación";
fieldAliasList2["EIP"]["fini"] = "Fecha Inicio";
fieldAliasList2["EIP"]["ffin"] = "Fecha Fin";
fieldAliasList2["EIP"]["idtipologia"] = "Tipología";
fieldAliasList2["EIP"]["csubtipologia"] = "Subtipología";
fieldAliasList2["EIP"]["numexp"] = "Número de Expediente";
fieldAliasList2["EIP"]["denominacion"] = "Denominación";
fieldAliasList2["EIP"]["solicitante"] = "Solicitante";
fieldAliasList2["EIP"]["dorigen"] = "Origen";
fieldAliasList2["EIP"]["url_enlace"] = "Url de acceso";

// INAGA EIP
fieldAliasList2["EIPA"] = new Array();
fieldAliasList2["EIPA"]["tipo_publicacion"] = "Tipo de publicación";
fieldAliasList2["EIPA"]["fini"] = "Fecha Inicio";
fieldAliasList2["EIPA"]["ffin"] = "Fecha Fin";
fieldAliasList2["EIPA"]["idtipologia"] = "Tipología";
fieldAliasList2["EIPA"]["csubtipologia"] = "Subtipología";
fieldAliasList2["EIPA"]["numexp"] = "Número de Expediente";
fieldAliasList2["EIPA"]["denominacion"] = "Denominación";
fieldAliasList2["EIPA"]["solicitante"] = "Solicitante";
fieldAliasList2["EIPA"]["dorigen"] = "Origen";
fieldAliasList2["EIPA"]["url_enlace"] = "Url de acceso";

//SITA
fieldAliasList2["110104_01M"] = new Array();
fieldAliasList2["110104_01M"]["tempo"] = "Fecha";
fieldAliasList2["110104_01M"]["valor"] = "Renta Disponible Bruta (Miles de euros)";

fieldAliasList2["0702_01M"] = new Array();
fieldAliasList2["0702_01M"]["tempo"] = "Fecha";
fieldAliasList2["0702_01M"]["valor"] = "Paro registrado (Nº de personas)";

fieldAliasList2["070301_01M"] = new Array();
fieldAliasList2["070301_01M"]["tempo"] = "Fecha";
fieldAliasList2["070301_01M"]["valor"] = "Afiliaciones en alta a la SS";

fieldAliasList2["070403_01M"] = new Array();
fieldAliasList2["070403_01M"]["tempo"] = "Fecha";
fieldAliasList2["070403_01M"]["valor"] = "Contratos registrados";

fieldAliasList2["0201010301_01M"] = new Array();
fieldAliasList2["0201010301_01M"]["tempo"] = "Fecha";
fieldAliasList2["0201010301_01M"]["valor"] = "Población residente (Nº de personas)";

fieldAliasList2["020202_01M"] = new Array();
fieldAliasList2["020202_01M"]["tempo"] = "Fecha";
fieldAliasList2["020202_01M"]["valor"] = "Densidad de población (Hab/Km2)";

fieldAliasList2["0204_01M"] = new Array();
fieldAliasList2["0204_01M"]["tempo"] = "Fecha";
fieldAliasList2["0204_01M"]["valor"] = "Saldo migratirio (Nº variaciones residenciales)";

fieldAliasList2["0205_01M"] = new Array();
fieldAliasList2["0205_01M"]["tempo"] = "Fecha";
fieldAliasList2["0205_01M"]["valor"] = "Saldo Vegetativo (Nacidos vivos menos defunciones)";

fieldAliasList2["fianzas"] = new Array();
fieldAliasList2["fianzas"]["via_loc"] = "Vía";
fieldAliasList2["fianzas"]["valores"] = "Gráfico evolución";

fieldAliasList2["gasolineras"] = new Array();
fieldAliasList2["gasolineras"]["rotulo"] = "Nombre";
fieldAliasList2["gasolineras"]["operador"] = "Operador";
fieldAliasList2["gasolineras"]["direccion"] = "Dirección";

fieldAliasList2["estaciones_ica"] = new Array();
fieldAliasList2["estaciones_ica"]["name"] = "Nombre";

fieldAliasList2["v_instalaciones_sanitarias_geom_atencion_primaria"] = new Array();
fieldAliasList2["v_instalaciones_sanitarias_geom_atencion_primaria"]["nombre_centro"] = "Nombre";
fieldAliasList2["v_instalaciones_sanitarias_geom_atencion_primaria"]["clase"] = "Clase";
fieldAliasList2["v_instalaciones_sanitarias_geom_atencion_primaria"]["oferta_asistencial"] = "Oferta asistencial";

fieldAliasList2["v_instalaciones_sanitarias_geom_hospital"] = new Array();
fieldAliasList2["v_instalaciones_sanitarias_geom_hospital"]["nombre_centro"] = "Nombre";
fieldAliasList2["v_instalaciones_sanitarias_geom_hospital"]["clase"] = "Clase";
fieldAliasList2["v_instalaciones_sanitarias_geom_hospital"]["oferta_asistencial"] = "Oferta asistencial";

fieldAliasList2["v_instalaciones_sanitarias_geom_centro_sanitario"] = new Array();
fieldAliasList2["v_instalaciones_sanitarias_geom_centro_sanitario"]["nombre_centro"] = "Nombre";
fieldAliasList2["v_instalaciones_sanitarias_geom_centro_sanitario"]["clase"] = "Clase";
fieldAliasList2["v_instalaciones_sanitarias_geom_centro_sanitario"]["oferta_asistencial"] = "Oferta asistencial";

fieldAliasList2["v_instalaciones_sanitarias_geom_otras"] = new Array();
fieldAliasList2["v_instalaciones_sanitarias_geom_otras"]["nombre_centro"] = "Nombre";
fieldAliasList2["v_instalaciones_sanitarias_geom_otras"]["clase"] = "Clase";
fieldAliasList2["v_instalaciones_sanitarias_geom_otras"]["oferta_asistencial"] = "Oferta asistencial";