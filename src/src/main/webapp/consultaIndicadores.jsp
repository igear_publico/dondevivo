<%@ page language='java' contentType='text/html;charset=UTF-8'%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.xml.parsers.*"%>
<%@ page import="org.w3c.dom.*"%>
<%@ page import = "conexion.*" %>
<jsp:useBean id="dbConn" class="conexion.conexionBD" scope="request"/>
<%
	
Connection conn=null;//Objeto para la conexión a la BD
Statement stmt=null;//Objeto para la ejecucion de sentencias
ResultSet rs=null;//Objeto para guardar los resultados
Statement stmt2=null;//Objeto para la ejecucion de sentencias
ResultSet rs2=null;//Objeto para guardar los resultados
String codmun=request.getParameter("C_MUNI_INE");

try {
//Nos conectamos a la BD
	conn=dbConn.getConnection("conexDV");
	//Creamos una sentencia a partir de la conexión, que refleje cambios y solo de lectura
	stmt=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	//Seleccionamos la tabla
	
	String  query = "SELECT dv.bloque,dv.id,s.titulo,s.unidad,s.fuente,s.tabla FROM carto.dondevivo_valores dv, carto.v_mapas_valores s  WHERE s.id = dv.id order by bloque";
	rs=stmt.executeQuery(query);
	out.println("{\"indicadores\":{");
	boolean first=true;	
	while (rs.next()){
		if (!first){
			out.print(",");
		}
		first=false;
		out.println("\""+rs.getString("id")+"\":{");
		out.print("\"unidad\":\""+rs.getString("unidad")+"\",");
		out.print("\"fuente\":\""+rs.getString("fuente")+"\",");
		out.print("\"valores\":[");
		stmt2=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
		//out.println("SELECT valor,tempo FROM carto.proapp1_iaesmap_map_valores where codtema='"+rs.getString("codtema")+"' and codmap='"+rs.getString("codmap")+"' and codmun='"+codmun+"'");
		rs2=stmt2.executeQuery("SELECT valor,tempo FROM "+rs.getString("tabla")+" where id='"+rs.getString("id")+"' and codmun='"+codmun+"' order by tempo");
		boolean firstval=true;
		while (rs2.next()){
			if (!firstval){
				out.print(",");
			}
			firstval=false;
			out.print("[\""+rs2.getString("tempo")+"\",");	
			out.print(rs2.getString("valor")+"]");
		
			
		}
		out.println("]}");
		
	}
	out.println("}}");
	
	} 
catch (Exception e2) {
		out.println("Fallo: " + e2.getMessage());
		
	} 	finally {
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e2) {
			out.println("Fallo al desconectar SQL: " + e2.getMessage());
		}
		try {
			if (stmt != null)
				stmt.close();
		} catch (SQLException e3) {
			out.println("Fallo al cerrar sentencia SQL: "
					+ e3.getMessage());
		}
try {
			if (stmt2 != null)
				stmt2.close();
		} catch (SQLException e4) {
			out.println("Fallo al cerrar sentencia SQL: "
					+ e4.getMessage());
		}
	}
	
	%>





