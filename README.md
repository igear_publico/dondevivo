### Dependencias

Para el funcionamiento de la aplicación Donde Vivo es necesario disponer de:
- una base de datos Postgres + PostGIS
- un WMS/WFS con soporte al parámetro CQL_FILTER y el formato json (e.g.: geoserver)
- el servicio web SpatialSearchService (https://gitlab.com/igear_publico/spatialsearchservice) que permite realizar operaciones espaciales entre una determinada geometría y una serie de capas geográficas almacenadas en PostGIS
- el conjunto de librerías javascript externas y código javascript compartido de IDEARAGON denominado lib (https://gitlab.com/igear_publico/lib)

### Instalación

Para instalar dondeVivo hay que generar el war con los fuentes proporcionados y desplegarlo en el servidor de aplicaciones.

